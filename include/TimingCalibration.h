#ifndef TimingCalibration_h
#define TimingCalibration_h 1

#include "marlin/Processor.h"
#include "lcio.h"

#include "Mapper.hh"
#include "AhcMapper.hh"
#include "Ahc2Mapper.hh"

#include <string>
#include <map>
#include <vector>

#include "TH1I.h"
#include "TH1D.h"
#include "TH2D.h"

#include "TimingManager.h"

using namespace lcio ;
using namespace marlin ;


/**  Example processor for marlin.

 * What it does
 *
 * @param CollectionName Name of the MCParticle collection
 *
 * @author C. Graf, MPP
 * @version $Id: TimingCalibration.h,v 1.4 2005-10-11 12:57:39 gaede Exp $
 */

class TimingCalibration : public Processor {

 public:

    virtual Processor*  newProcessor() { return new TimingCalibration ; }


    TimingCalibration() ;

    /** Called at the begin of the job before anything is read.
    * Use to initialize the processor, e.g. book histograms.
    */
    virtual void init() ;

    void init_histograms();

    void writeCalibration();

    void writeRootFile();

    /** Called for every run.
    */
    virtual void processRunHeader( LCRunHeader* run ) ;

    /** Called for every event - the working horse.
    */
    virtual void processEvent( LCEvent * evt ) ;


    virtual void check( LCEvent * evt ) ;


    /** Called after data processing for clean up.
    */
    virtual void end() ;


 protected:

    // The timing manager reads in the calibration and does the conversion of
    // tdc to ns
    TimingManager* _timingManager;

    int _setOfCalibrationConstants;

    TH1D* h_delta_ns;
    TH1D* h_nHits;

    // map<chipID, histo > depreciated
    // std::map<int, std::vector<TH1I*> > mh_delay_CIDs;
    // std::map<int, std::vector<double> > _m_timing_offset;

    //map<chipID, channel, memCell, bxID even?, histo>
    std::map<int, std::map<int, std::map<int, std::map<bool, TH1I*> > > > _mh_delay;
    std::map<int, std::map<int, std::map<int, std::map<bool, double> > > > _m_timing_offset;
    //both bxIDs
    std::map<int, std::map<int, std::map<int, TH1I*> > > _mh_delay_all;
    std::map<int, std::map<int, std::map<int, double> > > _m_timing_offset_all;

    std::vector<std::pair<int, int> > v_T0;

    // map<chipID, map< memoryCell< vector<tdc_min_even, tdc_max_even, tdc_min_odd, tdc_max_odd> > >
    std::map<int, std::map<int, std::vector<int> > > m_tdcEdges;
    // map<chipID, map< memoryCell < pair<slope_even, slope_odd> > >
    std::map<int, std::map<int, std::pair<double, double> > > m_tdcSlopes;

    std::vector<timing::Event> _v_events;

    std::string _mappingProcessorName;   //name of the MappingProcessor which provides the mapper
    std::string _Ahc2HardwareConnectionName;
    std::map<std::pair<int, int>, int > _HardwareConnnectionContainer;/**<map containing relationship between ChipID and Module/ChipNb*/

    std::string _fn_tdc_calibration; // = "tdc_calibration.txt";
    std::string _fn_timing_calibration; //
    std::string _fn_nonLinearity_correction; //

    bool _doNonLinearityCorrection = false;

    std::vector<int> _broken_chips;

    std::vector<int> _v_chipIDs;
    int _nChips;
    int _nMemoryCells;
    int _nChannels;
    int _nT0s;

    double _bxPeriod; //ns
    double _rampDeadtime; //%

    std::string _colName_HCAL;
    std::string _colName_ECAL;

    std::string _TBCampaign;
    bool _isInitialized = false;

    int _nRun ;
    int _nEvt ;
} ;

#endif
