#ifndef TimingManager_h
#define TimingManager_h 1

#include <algorithm>
#include <map>
#include <vector>

#include "lcio.h"

#include <EVENT/CalorimeterHit.h>
#include <EVENT/LCGenericObject.h>

// #include "Mapper.hh"
// #include "AhcMapper.hh"
#include "Ahc2Mapper.hh"

namespace timing {

    struct Hit {
        int eventID = -1;
        int runID = -1;
        int bxID = -1;
        int chipID = -1;
        int memoryCell = -1;
        int channel = -1;
        int adc = -1;
        int tdc = -1;
        int I = 0;
        int J = 0;
        int K = 0;
        int module = 0;
        int chipNr = 0;

        int hitbit = -1;
        int T0ID = -1;
        double ns = -1;
        double mip = -1.;
        bool isT0 = false;
    };

    struct Event {
        int nT0s = 0;
        std::map<int, Hit> T0s; // easy access of the T0s
        std::vector<Hit> v_hits;
        double T0_time = 0.;
        int nTracks =0;
    };
}

class TimingManager {
    public:
        void initializeGeometry(std::string mappingProcessorName, lcio::LCCollection* Ahc2HardwareConnectionCol);

        // void readTDCCalibration(const char* fn_tdc_calibration);
        void readTDCCalibration_extended(const char* fn_tdc_calibration);

        // void readTimingCalibration(const char* fn_timing_calibration);
        void readTimingCalibration_extended(const char* fn_timing_calibration);

        void readNonLinearityCorrection(const char* fn_nonLinearity);

        timing::Hit fillReconstructedHit(lcio::CalorimeterHit* p);
        timing::Hit fillRawHit(lcio::LCGenericObject* p);

        timing::Hit fillHit(lcio::LCGenericObject* p);
        timing::Hit fillHit(lcio::CalorimeterHit* p);

        void readRootFile(const char* fn_root_hits, std::vector<timing::Event>& v_events);
        // void processEvents(std::vector<timing::Event>& v_events)


        double tdcToNs(int chipID, int memoryCell, int BxID, int tdc);
        double tdcToNs(int chipID, int channel, int memoryCell, int BxID, int tdc);
        double tdcToNs_T0(int chipID, int channel, int memoryCell, int BxID, int tdc);
        double tdcToNs_nonLin(int chipID, int channel, int memoryCell, int BxID, int tdc, double T0_time);
        double tdcToNs_nonLin(int chipID, int channel, int memoryCell, int BxID, int tdc, double nl_offset, double nl_slope);

        // double getOffset(int chipID, int channel);
        double getOffset(int chipID, int channel, int memCell, int bxID);


        // set flag so that T0 Linearization is performed. Is done via flag in the file
        // void inline enableT0Linearization(int nChips) { _isT0Linearization = true; };

        void inline setNChips(int nChips) { _nChips = nChips; };
        void inline setNChannels(int nChannels) { _nChannels = nChannels; };
        void inline setNMemoryCells(int nMemoryCells) { _nMemoryCells = nMemoryCells; };

        void inline setBxPeriod(double bxPeriod) { _bxPeriod = bxPeriod; };
        void inline setRampDeadtime(double rampDeadtime) { _rampDeadtime = rampDeadtime; };

        void inline setT0s(std::vector<std::pair<int, int> > v_T0) { _v_T0 = v_T0; };
        bool inline isT0(std::pair<int, int> p_chn) {
            return (std::find(_v_T0.begin(), _v_T0.end(), p_chn) != _v_T0.end());
        };

        double inline getBxPeriod() { return _bxPeriod; };
        double inline getRampDeadtime() { return _rampDeadtime; };
        int inline getNChannels() { return _nChannels; };

        // int inline getModuleFromCellID(int cellID) { return _ahcMapper->getModuleFromCellID(cellID); };
        // int inline getChipFromCellID(int cellID) { return _ahcMapper->getChipFromCellID(cellID); };
        // int inline getChanFromCellID(int cellID) { return _ahcMapper->getChanFromCellID(cellID); };

        int inline getNMemoryCells() { return _nMemoryCells; };
        std::map<int, std::vector<std::pair<int, int> > > inline * getKIJContainer() { return &_KIJContainer; };

        int inline getChipIDFromModuleChipNr(int module, int chipNr) {
            return _HardwareConnnectionContainer[std::make_pair(module, chipNr)];
        }

        std::vector<int> inline getChipIDs() { return _v_chipIDs; };

        double getTdcPedestal(int chipID, int channel, int memoryCell, int bxID, int setOfCalibrationConstants);
        double getTdcMaxEdge(int chipID, int channel, int memoryCell, int bxID, int setOfCalibrationConstants);
        double getTdcSlope(int chipID, int channel, int memoryCell, int bxID, int setOfCalibrationConstants);

        std::pair<double, double> inline getTdcEdges(int chipID, int channel, int memoryCell, int bxID) {
            return std::make_pair(getTdcPedestal(chipID, channel, memoryCell, bxID, 9), getTdcMaxEdge(chipID, channel, memoryCell, bxID, 9));
        };



    private:

        // Fills the mapping for module number and chip number to cellID
        void fillChipIDMap(lcio::LCCollection* Ahc2HardwareConnectionCol);

        int _setOfCalibrationConstants = 0;

        //TODO: How to do this properly
        double _bxPeriod = 4000;
        double _rampDeadtime = 0.02;

        int _nChips = 0;
        int _nChannels = 36;
        int _nMemoryCells = 16;

        std::vector<int> _v_chipIDs;

        std::vector<std::pair<int, int> > _v_T0; // T0 channels

        bool _hasAhcMapper = true;
        bool _hasInitializedGeometry = false;

        bool _hasReadTDCCalibration = false;
        bool _hasReadTimingCalibration = false;
        bool _hasReadNonLinearityCorrection = false;

        const CALICE::Ahc2Mapper* _ahcMapper; //this is the AHCal specific mapper;

        /**<map containing relationship between ChipID and Module/ChipNr*/
        std::map<std::pair<int, int>, int > _HardwareConnnectionContainer;
        // map<K, pair<I, J> >
        std::map<int, std::vector<std::pair<int, int> > > _KIJContainer;




        // map<chipID, channel, memoryCell, bxID%2==0, value>
        std::map<int, std::map<int, std::map<int, std::map<bool, double> > > > _m_tdcPedestal_extended;
        std::map<int, std::map<int, std::map<int, std::map<bool, double> > > > _m_tdcMaxEdge_extended;
        std::map<int, std::map<int, std::map<int, std::map<bool, double> > > > _m_tdcSlope_extended;
        // map<chipID, channel, memoryCell, value> holding the results for both bxIDs
        std::map<int, std::map<int, std::map<int, double> > > _m_tdcPedestal_extended_all;
        std::map<int, std::map<int, std::map<int, double> > > _m_tdcMaxEdge_extended_all;
        std::map<int, std::map<int, std::map<int, double> > > _m_tdcSlope_extended_all;

        // map<chipID, map< channelID, offset> >
        // std::map<int, std::map<int, double > > _m_timing_offset;
        // map<chipID, channel, memCell, bxID%2==0, offset>
        std::map<int, std::map<int, std::map<int, std::map<bool, double > > > > _m_timing_offset_extended;
        // for both bxIDs
        std::map<int, std::map<int, std::map<int, double > > > _m_timing_offset_extended_all;

        // map<chipID, map<bxID%2==0, <nl_offset, nl_slope> > >
        std::map<int, std::map<bool, std::pair<double, double> > > _m_T0_nonLinearity;

        //map<chipID, map< bxID%2==0, vetor< a, b, c > > >
        //a+b*t+c*t^2
        std::map<int, std::map<bool, std::vector<double> > > _m_nonLinearity;

        bool _isT0Linearization = false;

};


#endif
