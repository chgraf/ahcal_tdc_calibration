#ifndef TDC_analysis_h
#define TDC_analysis_h 1

#include "marlin/Processor.h"
#include "lcio.h"

#include <string>
#include <map>
#include <vector>

#include "TH1I.h"
#include "TH1D.h"
#include "TH2D.h"

#include "Mapper.hh"
#include "AhcMapper.hh"
#include "Ahc2Mapper.hh"

#include "HitMap.h"
#include "TimingManager.h"

using namespace lcio ;
using namespace marlin ;


/**  Example processor for marlin.

 * What it does:
 * Processor for analysing TDC Testbeam data. It reads calibration files,
 * converts from tdc to ns, uses the offset correction and produces plots
 * with these information
 *
 * @param CollectionName Name of the MCParticle collection
 *
 * @author C. Graf, MPP
 */


class TDC_analysis : public Processor {

 public:

  enum DataSet {MUON, PION70_TRACKS, PION70};

  virtual Processor*  newProcessor() { return new TDC_analysis ; }


  TDC_analysis() ;

  virtual void init();

  // initilaizes histograms
  void init_histograms();


  virtual void processRunHeader( LCRunHeader* run ) ;

  void processHit(timing::Hit& hit);
  void reProcessEvent(timing::Event event);

  virtual void processEvent(LCEvent * evt);

  virtual void check(LCEvent * evt);

  void fit_calibration();

  void writeEventMaps(std::vector< std::vector< std::tuple<timing::Hit, double> > > v_eventsForHitMap);

  virtual void end() ;


 protected:

    // The timing manager reads in the calibration and does the conversion of
    // tdc to ns
    TimingManager* _timingManager;

    std::vector<std::pair<int, int> > _broken_channels;
    std::vector<int> _broken_chips;

    // Mapper and hardware connection
    std::string _mappingProcessorName;   //name of the MappingProcessor which provides the mapper
    std::string _Ahc2HardwareConnectionName;

    DataSet _dataSet;
    int p_DataSet;

    // Holding File names
    // TDC calibration constants: edges of the TDC spectra
    std::string _fn_tdc_calibration; // = "tdc_calibration.txt";
    // Timing calibration constants: Offset per chip and channel
    std::string _fn_timing_calibration; // = "tdc_calibration.txt";
    std::string _fn_nonLinearity_correction; // = "tdc_non_linearity.txt";
    // output root file
    std::string _fn_root_out; // = "analysis.root";
    std::string _fn_root_in; // = "analysis.root";

    bool _isRootInput = false;

    // input collection names
    std::string _colName_HCAL_Raw;
    std::string _colName_HCAL_Reco;
    std::string _colName_HCAL_Tracks;

    // from which TB campaign is the data? Used to adjust T0s and other things
    // that might have changed
    std::string _TBCampaign;
    // true, if the input data is reconstructed: needed to specify the right collection
    bool _isReconstructedData;
    bool _isTrackData;

    // Holding all events
    std::vector<timing::Event> _v_events;

    // Holds all available chipIDs
    std::vector<int> _v_chipIDs;

    int _nChannels;
    int _nMemoryCells;

    // vector holding the T0s, first part of the pair is the chipID, the second
    // one is the channel
    std::vector<std::pair<int, int> > v_T0;
    // number of T0s
    int _nT0s = 0;

    //Histograms
    TH1D* h_tdcSlopes;
    TH1D* h_pedestal;
    TH1D* h_maxEdge;
    TH1D* h_delta_ns;

    TH1D* h_T0_memCell;

    TH1D* h_nonLin;

    TH1D* h_memCell_pedestal;
    TH1D* h_memCell_slope;
    TH1D* h_memCell_maxEdge;

    std::vector<TH1I*> vh_T0_diff;

    std::vector<TH2D*> vh2_T0_vs_mT0s;

    std::map<int, std::map<int, TH2D*> > mh2_T0_vs_each_other;

    std::map<int, std::map<int, TH2D*> > mh2_T0_vs_each_other_tdc;

    // Support for the T0 plots
    std::vector<TH1I*> vh_TDCs_T0;
    std::vector<TH1I*> vh_TDCs_T0_odd;
    std::vector<TH1I*> vh_ADCs_T0;

    std::vector<std::vector<double> > v_ns_T0;

    // std::vector<double> v_ns_T0_12;
    // std::vector<double> v_ns_T0_02;
    // std::vector<double> v_ns_T0_01;

    HitMap* _hitMap_calibrated;
    HitMap* _hitMap_timeResolution;
    HitMap* _hitMap_timeDelay;
    HitMap* _hitMap;

    std::vector<int> tmp_count_T0s;

    // can be deleted if no longer needed
    // map<chipID, vector<memoryCell, histo> >
    std::map<int, TH1I* > mh_TDCs_CIDs_even;
    std::map<int, TH1I* > mh_TDCs_CIDs_odd;

    std::map<int, TH1I* > mh_TDCs_CIDs_corrected_even;
    std::map<int, TH1I* > mh_TDCs_CIDs_corrected_odd;

    // std::map<int, std::pair<double, double> > _m_nonLinParameters_even;
    // std::map<int, std::pair<double, double> > _m_nonLinParameters_odd;

    // std::map<int, std::map<bool, std::vector<double> > > _mv_quad_fit;

    std::map<int, std::map<int, std::map<int, std::map<int, std::tuple<double, double, double> > > > > _m_calibration_values;


    // Flag needed for initilization
    bool _isInitialized = false;
    bool _hasInitializedRun = false;

    int _nRun ;
    int _nEvt ;

} ;

#endif
