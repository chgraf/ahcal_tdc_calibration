#include <map>
#include <memory>
#include <string>
#include <vector>

#include "TH2D.h"
#include "TFile.h"
#include "TCanvas.h"

    class HitMap {
        public:

            // HitMap();
            HitMap(std::map<int, std::vector<std::pair<int, int> > >* KIJContainer, std::string pTitle);
            void initHitMap(std::map<int, int> nChannelsX, std::map<int, int> nChannelsY, std::string pTitle);

            void fill(int i, int j, int k);
            void set(int i, int j, int k, double value);

            void commonZScale(double zMin, double zMax);
            inline void setLogZ() { _logZ = true; };

            void write();
            void write(TFile* outputRootFile);

            // virtual ~HitMap() {
                // _vh2_layers.clear();
            // }

        private:
            int _nLayers;

            bool _logZ = false;


            std::vector<int> _vLayers;
            std::map<int, int> _nChannelsX;
            std::map<int, int> _nChannelsY;
            std::map<int, TH2D*> _mh2_layers;

    };
