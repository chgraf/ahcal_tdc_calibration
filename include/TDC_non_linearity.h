#ifndef TDC_non_linearity_h
#define TDC_non_linearity_h 1

#include "marlin/Processor.h"
#include "lcio.h"

#include <string>
#include <map>
#include <vector>

#include "TH1I.h"
#include "TH1D.h"
#include "TH2D.h"

#include "Mapper.hh"
#include "AhcMapper.hh"
#include "Ahc2Mapper.hh"

#include "HitMap.h"
#include "TimingManager.h"

using namespace lcio ;
using namespace marlin ;


/**  Example processor for marlin.

 * What it does:
 * Processor for analysing TDC Testbeam data. It reads calibration files,
 * converts from tdc to ns, uses the offset correction and produces plots
 * with these information
 *
 * @param CollectionName Name of the MCParticle collection
 *
 * @author C. Graf, MPP
 */


class TDC_non_linearity : public Processor {

 public:


  virtual Processor*  newProcessor() { return new TDC_non_linearity ; }


  TDC_non_linearity() ;

  virtual void init();

  // initilaizes histograms
  void init_histograms();


  virtual void processRunHeader( LCRunHeader* run ) ;

  virtual void processEvent( LCEvent * evt ) ;

  virtual void check( LCEvent * evt ) ;

  virtual void end() ;


 protected:

  void prepare_quadratic_nonLinearity_correction(
      std::map<int, std::map<bool, std::vector<double> > > &mv_T0,
      std::map<int, std::map<bool, std::vector<double> > > &mv_ns_T0
  );

  void histogram_nonLinearity_correction(bool isEven, TFile* o_file);

  void quadratic_nonLinearity_correction(
      std::map<int, std::map<bool, std::vector<double> > > &mv_T0,
      std::map<int, std::map<bool, std::vector<double> > > &mv_ns_T0,
      bool isEven,
      TFile* o_file
  );

  void writeNonLinearityCorrection();

    // The timing manager reads in the calibration and does the conversion of
    // tdc to ns
    TimingManager* _timingManager;

    // Mapper and hardware connection
    std::string _mappingProcessorName;   //name of the MappingProcessor which provides the mapper
    std::string _Ahc2HardwareConnectionName;

    // Holding File names
    // TDC calibration constants: edges of the TDC spectra
    std::string _fn_tdc_calibration; // = "tdc_calibration.txt";
    // Timing calibration constants: Offset per chip and channel
    std::string _fn_timing_calibration; // = "tdc_calibration.txt";
    // Output file for the non linearity correction constants
    std::string _fn_non_linearity_out; // = "tdc_non_linearity.txt";
    // output root file
    std::string _fn_root_out; // = "non_linearity.root";

    // input collection names
    std::string _colName_HCAL_Raw;
    std::string _colName_HCAL_Reco;
    std::string _colName_HCAL_Tracks;

    // from which TB campaign is the data? Used to adjust T0s and other things
    // that might have changed
    std::string _TBCampaign;
    // true, if the input data is reconstructed: needed to specify the right collection
    bool _isReconstructedData;
    bool _isTrackData;

    //parameter is set at steering file
    bool _enableT0Linearization;

    // Holding all events
    std::vector<timing::Event> _v_events;

    // Holds all available chipIDs
    std::vector<int> _v_chipIDs;

    int _nChannels = 0;

    // vector holding the T0s, first part of the pair is the chipID, the second
    // one is the channel
    std::vector<std::pair<int, int> > v_T0;
    // number of T0s
    int _nT0s = 0;

    //Histograms

    TH1D* h_nonLin;

    // can be deleted if no longer needed
    // map<chipID, vector<memoryCell, histo> >
    std::map<int, TH1I* > mh_TDCs_CIDs_even;
    std::map<int, TH1I* > mh_TDCs_CIDs_odd;

    std::map<int, TH1I* > mh_TDCs_CIDs_corrected_even;
    std::map<int, TH1I* > mh_TDCs_CIDs_corrected_odd;

    std::map<int, std::pair<double, double> > _m_nonLinParameters_even;
    std::map<int, std::pair<double, double> > _m_nonLinParameters_odd;

    std::map<int, std::map<bool, std::vector<double> > > _mv_quad_fit;



    // Flag needed for initilization
    bool _isInitialized = false;
    bool _hasInitializedRun = false;

    int _nRun ;
    int _nEvt ;

} ;

#endif
