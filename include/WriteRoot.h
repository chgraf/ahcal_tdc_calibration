#ifndef WriteRoot_h
#define WriteRoot_h 1

#include "marlin/Processor.h"
#include "lcio.h"

#include "TimingManager.h"

using namespace lcio ;
using namespace marlin ;


/**  Class that writes out a root file

 * What it does:
 * Writing the hits to a root file
 *
 * @param CollectionName Name of the MCParticle collection
 *
 * @author C. Graf, MPP
 */


class WriteRoot : public Processor {

 public:

  virtual Processor*  newProcessor() { return new WriteRoot ; }

  WriteRoot();

  virtual void init();

  virtual void processRunHeader(LCRunHeader* run);

  virtual void initRun(LCEvent* evt);

  virtual void processEvent(LCEvent* evt);

  virtual void check(LCEvent* evt);

  virtual void end();


 protected:

    // The timing manager reads in the calibration and does the conversion of
    // tdc to ns
    TimingManager* _timingManager;

    // Mapper and hardware connection;
    std::string _mappingProcessorName;   //name of the MappingProcessor which provides the mapper
    std::string _Ahc2HardwareConnectionName;

    std::string _fn_root_out; // = "analysis.root";

    // input collection names
    std::string _colName_HCAL_Raw;
    std::string _colName_HCAL_Reco;
    std::string _colName_HCAL_Tracks;

    // from which TB campaign is the data? Used to adjust T0s and other things
    // that might have changed
    std::string _TBCampaign;
    // true, if the input data is reconstructed: needed to specify the right collection
    bool _isReconstructedData;
    bool _isTrackData;

    // Holding all events
    std::vector<timing::Event> _v_events;

    // Holds all available chipIDs
    std::vector<int> _v_chipIDs;

    int _nChannels;

    // vector holding the T0s, first part of the pair is the chipID, the second
    // one is the channel
    std::vector<std::pair<int, int> > v_T0;
    // number of T0s
    int _nT0s = 0;

    // Flag needed for initilization
    bool _isInitialized = false;
    bool _hasInitializedRun = false;

    int _nRun = 0;
    int _nEvt = 0;

};

#endif
