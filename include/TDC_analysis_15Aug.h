#ifndef TDC_analysis_15Aug_h
#define TDC_analysis_15Aug_h 1

#include "marlin/Processor.h"
#include "lcio.h"

#include <string>
#include <map>
#include <vector>

#include "TH1I.h"
#include "TH1D.h"
#include "TH2D.h"

using namespace lcio ;
using namespace marlin ;


/**  Example processor for marlin.

 * What it does
 *
 * @param CollectionName Name of the MCParticle collection
 *
 * @author C. Graf, MPP
 * @version $Id: TDC_analysis.h,v 1.4 2005-10-11 12:57:39 gaede Exp $
 */

struct Hit {
    int bxID;
    int chipID;
    int memoryCell;
    int channel;
    int adc;
    int tdc;
    int ns;
    int hitbit;
    bool isT0;
    int T0ID = -1;

};

struct Event {
    int nT0s = 0;
    std::map<int, Hit> T0s; // easy access of the T0s
    std::vector<Hit> v_hits;
};

class TDC_analysis_15Aug : public Processor {

 public:

  virtual Processor*  newProcessor() { return new TDC_analysis_15Aug ; }


  TDC_analysis_15Aug() ;

  /** Called at the begin of the job before anything is read.
   * Use to initialize the processor, e.g. book histograms.
   */
  virtual void init() ;

  double calculateTDCMedian(TH1I* histo);
  std::pair<double, double> calculateEdges(TH1I* histo);
  void writeCalibration();
  void readCalibration();
  int tdcToNs(int chipID, int memoryCell, int BxID, int tdc);

  /** Called for every run.
   */
  virtual void processRunHeader( LCRunHeader* run ) ;

  /** Called for every event - the working horse.
   */
  virtual void processEvent( LCEvent * evt ) ;


  virtual void check( LCEvent * evt ) ;


  /** Called after data processing for clean up.
   */
  virtual void end() ;


 protected:

  bool isCalibrationMode = false;

  bool isCalibrationRead = false;

     std::vector<int> tmp_count_T0s;

  TH1D* h_tdcSlopes;
  TH1D* h_delta_ns;

  TH1D* h_memCell_pedestal;
  TH1D* h_memCell_slope;

  TH1D* h_T0_diff_0;
  TH1D* h_T0_diff_1;
  TH1D* h_T0_diff_2;

  TH2D* h2_T0_vs_mT0s_0;
  TH2D* h2_T0_vs_mT0s_1;
  TH2D* h2_T0_vs_mT0s_2;
  TH2D* h2_T0_vs_mT0s_3;

  TH2D* h2_T00_vs_T01;
  TH2D* h2_T00_vs_T02;
  TH2D* h2_T01_vs_T02;

  TH2D* h2_T00_vs_T01_tdc;
  TH2D* h2_T00_vs_T02_tdc;
  TH2D* h2_T01_vs_T02_tdc;

  TH2D* h2_debug_T0_adcs;

  // map<chipID, histo >
  std::map<int, TH1I*> mh_TDCs_CIDs;

  // map<chipID, vector<memoryCell, histo> >
  std::map<int, std::vector<TH1I*> > mh_TDCs_CIDs_even;
  std::map<int, std::vector<TH1I*> > mh_TDCs_CIDs_odd;

  std::vector<std::pair<int, int> > v_T0;

  std::vector<TH1I*> vh_TDCs_T0;
  std::vector<TH1I*> vh_ADCs_T0;

  // needed for TGraphs
  std::vector<double> v_ns_T0_0;
  std::vector<double> v_ns_T0_1;
  std::vector<double> v_ns_T0_2;

  std::vector<double> v_ns_T0_12;
  std::vector<double> v_ns_T0_02;
  std::vector<double> v_ns_T0_01;
  // std::vector<double> v_ns_T0_012;
  // map<chipID, map< memoryCell< vector<tdc_min_even, tdc_max_even, tdc_min_odd, tdc_max_odd> > >
  std::map<int, std::map<int, std::vector<int> > > m_tdcEdges;
  // map<chipID, map< memoryCell < pair<slope_even, slope_odd> > >
  std::map<int, std::map<int, std::pair<double, double> > > m_tdcSlopes;

  /** Input collection name.
   */

  std::vector<Event> _v_events;


  std::string _colName_HCAL;
  std::string _colName_ECAL;

  const char* fn_tdc_calibration = "tdc_calibration.txt";

  int _first_chip;
  int _nChips;
  int _nMemoryCells = 16;

  int _nRun ;
  int _nEvt ;
} ;

#endif
