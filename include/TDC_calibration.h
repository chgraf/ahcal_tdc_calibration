#ifndef TDC_calibration_h
#define TDC_calibration_h 1

#include "marlin/Processor.h"
#include "lcio.h"

#include "Mapper.hh"
#include "AhcMapper.hh"
#include "Ahc2Mapper.hh"

#include <string>
#include <map>
#include <vector>

#include "TH1I.h"
#include "TH1D.h"
#include "TH2D.h"

#include "TimingManager.h"

using namespace lcio ;
using namespace marlin ;


/**  Example processor for marlin.

 * What it does:
 * Writes a .txt file with calibration constants for the TDC. These are the
 * minimum and the maximum edge of the TDC spectra for each chip, memory Cell
 * and for even and odd bunch crossing IDs (bcIDs).
 * It also produces two root output files. One with all the TDC spectra
 * and one with output for analysis that may help to see how accurate the calibration
 * is right now.
 * Using the min and max edges of a TDC, one can convert from tdc to ns with:
 * ns = (tdc - min_edge) * slope
 * where
 * slope = (clock_period - deadtime) / (max_edge - min_edge)
 * clock_period = 4000ns (in Testbeam mode) / 200ns (in ILC mode)
 * deadtime = 0.98*clock_period for the current setup in TB mode
 *
 * Input: raw or reconstructed slcio files
 * (set the isReconstructed parameter to distinguish between the two)
 * or it may also use proper root files with the TDC spectra.
 * (set useRootFile from "false" to "<rootFilename>") Which is much faster.
 * this can be used for optimizing the calibration without reading in all the
 * data every time.
 *
 * @param
 *
 * @author C. Graf, MPP
 * @version $Id: TDC_calibration.h,v 0.1 2016-08-19$
 */


class TDC_calibration : public Processor {

 public:

  virtual Processor*  newProcessor() { return new TDC_calibration ; }


  TDC_calibration() ;

  /** Called at the begin of the job before anything is read.
   * Use to initialize the processor, e.g. book histograms.
   */
  virtual void init() ;

  void init_histograms();

  double calculateTDCMedian(TH1I* histo);
  std::pair<double, double> calculateEdges(TH1I* histo);
  // std::pair<double, double> fitPlateau(TH1I* histo, double min_edge, double max_edge);
  void writeCalibration();

  void readRootFile();
  void writeRootFile();
  // int tdcToNs(int chipID, int memoryCell, int BxID, int tdc);

  /** Called for every run.
   */
  virtual void processRunHeader( LCRunHeader* run ) ;

  /** Called for every event - the working horse.
   */
  virtual void processEvent( LCEvent * evt ) ;


  virtual void check( LCEvent * evt ) ;


  /** Called after data processing for clean up.
   */
  virtual void end() ;


 protected:

    int _tmp_counter = 0;

    // The timing manager reads in the calibration and does the conversion of
    // tdc to ns
    TimingManager* _timingManager;

    TH1D* h_channels;

    TH1D* h_tdcSlopes;
    TH1D* h_pedestal;
    TH1D* h_maxEdge;

    TH1D* h_memCell_pedestal;
    TH1D* h_memCell_slope;
    TH1D* h_memCell_maxEdge;

    TH1D* h_channel_pedestal;
    TH1D* h_channel_slope;
    TH1D* h_channel_maxEdge;

    // map<chipID, vector<channel, vector<memoryCell, histo> >
    // CCM: ChipChannelMemory
    std::map<int, std::vector< std::vector<TH1I*> > > mh_TDCs_ChiChaMem_even;
    std::map<int, std::vector< std::vector<TH1I*> > > mh_TDCs_ChiChaMem_odd;
    std::map<int, std::vector< std::vector<TH1I*> > > mh_TDCs_ChiChaMem_all;


    // CM: ChipMemory
    std::map<int, std::vector<TH1I*> > mh_TDCs_ChiMem_even; // delete if not needed
    std::map<int, std::vector<TH1I*> > mh_TDCs_ChiMem_odd; //delete if not needed
    // chips
    std::map<int, TH1I*> mh_TDCs_Chips_even; //delete if not needed
    std::map<int, TH1I*> mh_TDCs_Chips_odd; //delete if not needed

    std::vector<std::pair<int, int> > v_T0;

    // map<chipID, map< memoryCell< vector<tdc_min_even, tdc_max_even, tdc_min_odd, tdc_max_odd> > >
    std::map<int, std::map<int, std::vector<int> > > m_tdcEdges;
    // map<chipID, map< memoryCell < pair<slope_even, slope_odd> > >
    std::map<int, std::map<int, std::pair<double, double> > > m_tdcSlopes;

    std::map<int, double> _m_id_pedestal;
    std::map<int, double> _m_id_maxEdge;
    std::map<int, double> _m_id_slope;

    std::map<int, double> _m_id_sumMemCell_pedestal;
    std::map<int, double> _m_id_sumMemCell_maxEdge;
    std::map<int, double> _m_id_sumMemCell_slope;

    std::vector<timing::Event> _v_events;

    std::string _mappingProcessorName;   //name of the MappingProcessor which provides the mapper
    std::string _Ahc2HardwareConnectionName;

    std::string _fn_tdc_calibration; // = "tdc_calibration.txt";
    std::string _fn_root_spectra; //
    std::string _fn_root_analysis; //

    std::vector<int> _v_chipIDs;

    int _count_working_fits;
    int _count_thr_edges;

    int _nChips;
    int _nChannels;
    int _nMemoryCells;

    bool _isInitialized = false;
    bool _hasInitializedRun = false;

    double _bxPeriod; //ns
    double _rampDeadtime; //%

    std::string _colName_HCAL_Raw;
    std::string _colName_HCAL_Reco;

    std::string _TBCampaign;
    bool _isReconstructedData = false;
    bool _useRootFile;
    bool _fOnlyCalibrateT0s;
    std::string _fn_RootInput;

    int _nRun ;
    int _nEvt ;
} ;

#endif
