#include "TimingCalibration.h"
#include <algorithm> //std::sort #include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <EVENT/LCCollection.h>
#include <EVENT/LCGenericObject.h>
#include <EVENT/CalorimeterHit.h>

#include "MappingProcessor.hh"
#include "Ahc2HardwareConnection.hh"

// ----- include for verbosity dependend logging ---------
#include "marlin/VerbosityLevels.h"
#include "marlin/Exceptions.h"
#include "TFile.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TCanvas.h"

using namespace lcio;
using namespace marlin;
using namespace CALICE;


TimingCalibration aTimingCalibration;


TimingCalibration::TimingCalibration() : Processor("TimingCalibration") {

    // modify processor description
    _description = "TimingCalibration has a first look on the TDC data ..." ;


    // register steering parameters: name, description, class-variable, default value
    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCAL" ,
            "Name of the HCAL input collection"  ,
            _colName_HCAL ,
            std::string("EUDAQDataHCAL")
    );

    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameECAL" ,
            "Name of the ECAL input collection"  ,
            _colName_ECAL ,
           std::string("EUDAQDataECAL")
    );

    registerProcessorParameter(
        "TestbeamCampaign",
        "TestbeamCampaign: Aug15, Jul15",
        _TBCampaign,
        std::string("Aug15")
    );

    registerProcessorParameter(
        "TDCCalibrationFilename",
        "name of the TDC calibration file",
        _fn_tdc_calibration,
        std::string("tdc_calibration.txt")
    );

    registerProcessorParameter(
        "NonLinearityCorrectionFilename",
        "name of the nonLinearity correction file",
        _fn_nonLinearity_correction,
        std::string("tdc_non_linearity.txt")
    );

    registerProcessorParameter(
        "OutputTimingCalibrationFilename",
        "name of the output timing calibration file",
        _fn_timing_calibration,
        std::string("timing_calibration_extended.txt")
    );

    registerProcessorParameter(
        "MappingProcessorName" ,
        "Name of the MappingProcessor instance that provides the geometry of the detector." ,
        _mappingProcessorName,
        std::string("MyMappingProcessor")
    );

    registerProcessorParameter(
        "HardwareConnectionCollection" ,
        "Name of the Ahc2HardwareConnection Collection",
        _Ahc2HardwareConnectionName,
		std::string("Ahc2HardwareConnection")
    );

    registerProcessorParameter(
        "SetOfCalibrationConstants" ,
        "Which calibration constants to use",
        _setOfCalibrationConstants,
		0
    );

    registerProcessorParameter(
        "DoNonLinearityCorrection" ,
        "Should the nonLinearity Corretion be done?",
        _doNonLinearityCorrection,
		false
    );
}


void TimingCalibration::init() {
    std::cout << "Init..." << std::endl;
    // ---------- config ----------


    _timingManager = new TimingManager();

    _timingManager->setNChannels(36);
    _timingManager->setNMemoryCells(16);
    _timingManager->setBxPeriod(4000);
    _timingManager->setRampDeadtime(0.02);

    _nChannels = _timingManager->getNChannels();
    _nMemoryCells = _timingManager->getNMemoryCells();

    if(_TBCampaign == "Aug15") {
        // v_T0.push_back(std::pair<int, int>(185,29)); //noisy
        v_T0.push_back(std::pair<int, int>(201,29));
        v_T0.push_back(std::pair<int, int>(217,23));
    } else if(_TBCampaign == "Jul15") {
        v_T0.push_back(std::pair<int, int>(169,29));
        v_T0.push_back(std::pair<int, int>(177,23));
        v_T0.push_back(std::pair<int, int>(185,29));
        v_T0.push_back(std::pair<int, int>(217,23));
    }

    _nT0s = v_T0.size();

    _timingManager->setT0s(v_T0);

    // 19 broken channels. But some of them may be ok
    // _broken_chips.push_back(141);
    // _broken_chips.push_back(147);
    // _broken_chips.push_back(169);
    // _broken_chips.push_back(174);
    // _broken_chips.push_back(175);
    // _broken_chips.push_back(177);
    // _broken_chips.push_back(178);
    // _broken_chips.push_back(181);
    // _broken_chips.push_back(182);
    // _broken_chips.push_back(186);
    // _broken_chips.push_back(190);
    // _broken_chips.push_back(195);
    // _broken_chips.push_back(198);
    // _broken_chips.push_back(207);
    // _broken_chips.push_back(215);
    // _broken_chips.push_back(222);
    // _broken_chips.push_back(227);
    // _broken_chips.push_back(234);
    // _broken_chips.push_back(235);

    _HardwareConnnectionContainer.clear();

    // usually a good idea to
    printParameters() ;

    std::cout << "Reading TDC Calibration... " << std::endl;
    _timingManager->readTDCCalibration_extended(_fn_tdc_calibration.c_str());

    if(_doNonLinearityCorrection) {
        std::cout << "Reading nonLinearity Correction... " << std::endl;
        // _timingManager->readTimingCalibration_extended(_fn_input_timing_calibration.c_str());
        _timingManager->readNonLinearityCorrection(_fn_nonLinearity_correction.c_str());
    }

    _nRun = 0 ;
    _nEvt = 0 ;

}

/*  Adapted from Eldwans Ahc2CalibrateProcessor
    Fills a container with a mapping of ModuleNumber and ChipNumber to ChipID*/

void TimingCalibration::init_histograms() {
    // ---------- Init Histos ----------

    h_delta_ns = new TH1D("h_delta_ns", "difference of t to mean time of event", 20000, -10000, 10000);
    h_delta_ns->GetXaxis()->SetTitle("#Delta t [ns]");
    h_delta_ns->GetYaxis()->SetTitle("#");

    h_nHits = new TH1D("h_nHits", "number of Hits", 200, 0, 200);

    for(auto& chipID : _v_chipIDs) {
        std::map<int, std::map<int, std::map<bool, TH1I*> > > mm_bx;
        std::map<int, std::map<int, TH1I*> > mm_all;
        for(int iChannel=0; iChannel < _nChannels + 1; iChannel++ ) {
            std::map<int, std::map<bool, TH1I*> > m_bx;
            std::map<int, TH1I*> m_all;
            for(int iMemCell=0; iMemCell < _nMemoryCells; iMemCell++ ) {
                for(int k=0; k<3; k++ ) {
                    // std::cout << chipID << " " << iChannel << " " << iMemCell << " " << k << std::endl;
                    std::string bx;
                    if(k==0) bx = "even";
                    else if(k==1) bx = "odd";
                    else if(k==2) bx = "all";

                    std::stringstream hist_name;
                    std::stringstream hist_title;

                    hist_name.str(std::string());
                    hist_name << "h_delay_" << chipID << "_" << iChannel << "_" << iMemCell << "_" << bx;
                    hist_title.str(std::string());
                    hist_title << "Difference between T0 signal and Channel Response, Chip " << chipID;
                    hist_title << ", Ch: " << iChannel << ", MC: " << iMemCell << ", " << bx;

                    TH1I* h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 2000, -2000, 2000);
                    h_tmp->GetXaxis()->SetTitle("ns");
                    h_tmp->GetYaxis()->SetTitle("#");

                    if(k==0) m_bx[iMemCell][true] = h_tmp;
                    else if(k==1) m_bx[iMemCell][false] = h_tmp;
                    else if(k==2) m_all[iMemCell] = h_tmp;
                }
            }
            mm_bx[iChannel] = m_bx;
            mm_all[iChannel] = m_all;
        }
        _mh_delay[chipID] = mm_bx;
        _mh_delay_all[chipID] = mm_all;
    }
    std::cout << "initialized Histograms" << std::endl;
}

void TimingCalibration::processRunHeader( LCRunHeader* run) {

    _nRun++ ;
}


void TimingCalibration::processEvent( LCEvent * evt ) {

    if(not _isInitialized) {
        _timingManager->initializeGeometry(_mappingProcessorName, evt->getCollection(_Ahc2HardwareConnectionName) );
        _v_chipIDs = _timingManager->getChipIDs();
        // _nChips = _v_chipIDs.size();

        init_histograms();
        _isInitialized = true;

        _nChips = _v_chipIDs.size();
    }

    LCCollection* col = NULL;

    try {
        col = evt->getCollection( _colName_HCAL ) ;
    } catch (const std::exception& e) {

    }

    // LCCollection* col = evt->getCollection( _colName_HCAL ) ;

    // this will only be entered if the collection is available
    if( col != NULL ){

        int nHits = col->getNumberOfElements();

        // stores all the hits in the event
        timing::Event event;


        for(int i=0; i< nHits ; i++){

            // load values into hit struct
            timing::Hit hit;

            // get the current hit
            CalorimeterHit* p = dynamic_cast<CalorimeterHit*>( col->getElementAt( i ) ) ;

            hit = _timingManager->fillHit(p);

            // BxID has to be set explicitly using the event
            hit.bxID = evt->getParameters().getIntVal("BXID");

            if(hit.bxID > 4096) {
                std::cout << "Watch out! Corrupted readout cycle!";
                std::cout << " ... just use data that is converted from the raw .txt files";
                std::cout << " using the newest version of LabviewConverter2.cc";
            }

            hit.ns = _timingManager->tdcToNs(hit.chipID, hit.channel, hit.memoryCell, hit.bxID, hit.tdc);

            if(hit.ns < 0) continue;

            hit.isT0 = false;
            // is it a T0 hit?
            if(std::find(v_T0.begin(), v_T0.end(), std::make_pair(hit.chipID, hit.channel)) != v_T0.end()) {
                //full calibration for T0s
                hit.ns = _timingManager->tdcToNs_T0(hit.chipID, hit.channel, hit.memoryCell, hit.bxID, hit.tdc);

                //assign each T0 with an ID TODO: check if this is needed here.
                for(int k=0; k < _nT0s; k++) {
                    if(std::pair<int, int>(hit.chipID, hit.channel) == v_T0[k]) {
                        hit.T0ID = k;
                        break;
                    }
                }

                event.T0s[hit.T0ID] = hit;

                event.nT0s++;
                hit.isT0 = true;
            }

            if(std::find(_broken_chips.begin(), _broken_chips.end(), hit.chipID) == _broken_chips.end()) {
                // save hit in event
                event.v_hits.push_back(hit);
            }
        }



        // only take events where all T0 channels reported a hit
        if(event.nT0s == _nT0s) {

            double mean_T0_ns = 0.;
            for(int i=0; i < event.nT0s; i++) mean_T0_ns += event.T0s[i].ns;
            mean_T0_ns = mean_T0_ns / (double)event.nT0s;


            // if(event.v_hits.size() < 20) {
            for(auto& hit : event.v_hits) {
                if(hit.isT0) continue;

                if(_doNonLinearityCorrection) {
                    hit.ns = _timingManager->tdcToNs_nonLin(hit.chipID, hit.channel, hit.memoryCell, hit.bxID, hit.tdc, mean_T0_ns);
                }

                h_delta_ns->Fill(hit.ns - mean_T0_ns);
                _mh_delay[hit.chipID][hit.channel][hit.memoryCell][hit.bxID%2==0]->Fill(hit.ns - mean_T0_ns);
                _mh_delay[hit.chipID][hit.channel][0][hit.bxID%2==0]->Fill(hit.ns - mean_T0_ns);
                _mh_delay[hit.chipID][_nChannels][hit.memoryCell][hit.bxID%2==0]->Fill(hit.ns - mean_T0_ns);
                _mh_delay[hit.chipID][_nChannels][0][hit.bxID%2==0]->Fill(hit.ns - mean_T0_ns);

                _mh_delay_all[hit.chipID][hit.channel][hit.memoryCell]->Fill(hit.ns - mean_T0_ns);
                _mh_delay_all[hit.chipID][hit.channel][0]->Fill(hit.ns - mean_T0_ns);
                _mh_delay_all[hit.chipID][_nChannels][hit.memoryCell]->Fill(hit.ns - mean_T0_ns);
                _mh_delay_all[hit.chipID][_nChannels][0]->Fill(hit.ns - mean_T0_ns);
                // mh_delay_CIDs[hit.chipID][hit.channel]->Fill(hit.ns - mean_T0_ns);
            }
            // }

            if(abs(event.T0s[0].ns - event.T0s[1].ns) < 12) {
                _v_events.push_back(event);
            }


        } // end if nT0s == 3

        h_nHits->Fill(event.v_hits.size());

    } // end if col != 0


    //-- note: this will not be printed if compiled w/o MARLINDEBUG=1 !

    streamlog_out(DEBUG) << "   processing event: " << evt->getEventNumber()
        << "   in run:  " << evt->getRunNumber() << std::endl ;

    _nEvt ++ ;
}



void TimingCalibration::check( LCEvent * evt ) {
    // nothing to check here - could be used to fill checkplots in reconstruction processor
}

/* determines edges of tdc by using FindFirstBinAbove() and writes them to file */
void TimingCalibration::writeCalibration() {
    ofstream out_file;
    out_file.open(_fn_timing_calibration.c_str());
    out_file << "Set_of_calibration_constants:\t" << _setOfCalibrationConstants << "\n";
    out_file << "ChipID\tChannel\tMemoryCell\toffset_even\toffset_odd\toffset_all\n";

    for(auto& mm_offset : _m_timing_offset) {
        int chipID = mm_offset.first;
        for(auto& m_offset: mm_offset.second) {
            int channel = m_offset.first;
            for(auto& memCell_offset: m_offset.second) {
                int memCell = memCell_offset.first;

                out_file << chipID << "\t";
                out_file << channel << "\t";
                out_file << memCell << "\t";
                out_file << memCell_offset.second[true] << "\t";
                out_file << memCell_offset.second[false] << "\t";
                out_file << _m_timing_offset_all[chipID][channel][memCell] << "\t";

                out_file << "\n";
            }
        }
    }

    out_file.close();
}

void TimingCalibration::writeRootFile() {
    TFile* o_file = new TFile("timingCalibration.root", "recreate");

    if (o_file->IsOpen()) {

        std::vector<double> v_chip;
        std::vector<double> v_delay_mean;
        std::vector<double> v_delay_stdev;
        std::vector<double> v_error_x;
        std::vector<double> v_error_y;

        h_nHits->Write();

        TH1D* h_chip_resolutions = new TH1D("h_chip_resolutions", "Time Resolution per chip", 100, 0, 50);

        std::vector<TH1I*> vh_channel_offset;
        for(int i=0; i < _nChannels; i++) {
                std::stringstream hist_name;
                hist_name.str(std::string());
                hist_name << "h_channel_offset_" << i;

                std::stringstream hist_title;
                hist_title.str(std::string());
                hist_title << "Offset compared to Chip mean for Channel " << i;

                TH1I* h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 2000, -100, 100);
                h_tmp->GetXaxis()->SetTitle("#Delta t");
                h_tmp->GetYaxis()->SetTitle("#");
                vh_channel_offset.push_back(h_tmp);
        }


        int nTotalHits = 0;
        int nUsedHits = 0;

        // for(auto& p_histo : mh_delay_CID) {
        for(auto& mm_histo: _mh_delay) {
            int chipID = mm_histo.first;
            for(auto& m_histo: mm_histo.second) {
                int channel = m_histo.first;
                for(auto& memCell_histo: m_histo.second) {
                    int memCell = memCell_histo.first;
                    for(int k=0; k<3; k++) {
                        TH1I* histo;
                        if(k == 0) histo = memCell_histo.second[true];
                        else if(k == 1) histo = memCell_histo.second[false];
                        else if(k == 2) histo = _mh_delay_all[chipID][channel][memCell];

                        nTotalHits += histo->GetEntries();

                        // we need some statistics
                        if(histo->GetEntries() > 100) {
                            // std::cout << "chipID: " << chipID << "\tChannel: " << i << std::endl;
                            // double max_val = histo->GetMaximum();
                            int maxBin = histo->GetMaximumBin();

                            // int fit_x_min = histo->FindFirstBinAbove(0.4*max_val);
                            // int fit_x_max = histo->FindLastBinAbove(0.4*max_val);

                            int fit_x_min = maxBin - 10;
                            int fit_x_max = maxBin + 10;
                            // std::cout << histo->GetBinCenter(fit_x_min) << " " << histo->GetBinCenter(fit_x_max) << std::endl;
                            TF1 *f1 = new TF1("f1","gaus",-1000,1000);
                            histo->Fit(f1,"QL","",histo->GetBinCenter(fit_x_min), histo->GetBinCenter(fit_x_max) );
                            histo->Draw();

                            //FIXME: Be aware that i am cutting on this
                            if(f1->GetParameter(2) > 20) {
                                // v_delay_mean.push_back(0);
                                // _m_timing_offset[chipID].push_back(0);
                                //v_delay_stdev.push_back(f1->GetParameter(2));
                                // v_error_x.push_back(0);
                                // v_error_y.push_back(f1->GetParError(1));
                                // v_error_y.push_back(0);
                                // v_chip.push_back(chipID*100+i);

                                double offset = 0;
                                if(k==0) _m_timing_offset[chipID][channel][memCell][true] = offset;
                                else if(k==1) _m_timing_offset[chipID][channel][memCell][false] = offset;
                                else if(k==2) _m_timing_offset_all[chipID][channel][memCell] = offset;

                                h_chip_resolutions->Fill(f1->GetParameter(2));

                                std::cout << "bad fit cutting on 20ns resolution: " << chipID << "\t" << channel << std::endl;
                                continue;
                            }

                            // XXX!!! this changes how everything works!
                            double offset = f1->GetParameter(1);
                            // double offset = histo->GetBinCenter(maxBin);

                            // v_delay_mean.push_back(offset);
                            if(k==0) _m_timing_offset[chipID][channel][memCell][true] = offset;
                            else if(k==1) _m_timing_offset[chipID][channel][memCell][false] = offset;
                            else if(k==2) _m_timing_offset_all[chipID][channel][memCell] = offset;
                            //v_delay_stdev.push_back(f1->GetParameter(2));
                            // v_error_x.push_back(0);
                            // v_error_y.push_back(f1->GetParError(1));
                            // v_error_y.push_back(0);
                            // v_chip.push_back(chipID*100+i);

                            h_chip_resolutions->Fill(f1->GetParameter(2));

                            nUsedHits += histo->GetEntries();

                            // mean_offset += offset;
                        } else {
                            // v_delay_mean.push_back(0);
                            double offset = 0;
                            if(k==0) _m_timing_offset[chipID][channel][memCell][true] = offset;
                            else if(k==1) _m_timing_offset[chipID][channel][memCell][false] = offset;
                            else if(k==2) _m_timing_offset_all[chipID][channel][memCell] = offset;
                            //v_delay_stdev.push_back(f1->GetParameter(2));
                            // v_error_x.push_back(0);
                            // v_error_y.push_back(f1->GetParError(1));
                            // v_error_y.push_back(0);
                            // v_chip.push_back(chipID*100+i);

                            h_chip_resolutions->Fill(99);
                        }

                        if(memCell == 1 && histo->GetEntries() > 50) {
                            histo->Write();
                        }
                    }
                }
            }
        }

            // Calculate fit range. Two Options:
            // 1. Get bin with maximum content -> choose a certain range around this
            // 2. Get maximum value -> FindFirstBinAbove(0.5*Max) / FindLastBinAbove(0.5*Max)
            // => Second Option may be more robust to changes of binning in the histograms
            //      as long as there are no second peaks, which does not seem to be the case

        //     int count = 0;
        //     double mean_offset = 0.;
        //     for(unsigned int i=0; i < p_histo.second.size(); i++) {
        //
        //         TH1I* histo = p_histo.second[i];
        //
        //     }
        //
        //     mean_offset = mean_offset / (double)count;
        //
        //     int c = 0;
        //     for(auto& o : _m_timing_offset[chipID]) {
        //         if(o != 0) {
        //             vh_channel_offset[c]->Fill(o - mean_offset);
        //         }
        //         c++;
        //     }
        //     // std::cout << chipID << ": " << mean_offset / (double)p_histo.second.size() << std::endl;
        // }

        std::cout << "Using " << nUsedHits << " / " << nTotalHits << std::endl;

        for(auto& histo : vh_channel_offset) {
            histo->Write();
        }

        TH1D* h_delta_ns_corrected = new TH1D("h_delta_ns_corrected", "difference of t to T0 mean of event; offset corrected", 2000, -100, 100);
        TH1D* h_delta_ns_corrected_many_hits = new TH1D("h_delta_ns_corrected_many_hits", "difference of t to T0 mean of event; many hits", 2000, -100, 100);

        for(auto& event: _v_events) {
            double T0_time = 0.;
            for(auto& T0_hit: event.T0s) {
                T0_time += T0_hit.second.ns;
            }
            T0_time = T0_time / (double)event.nT0s;

            for(auto& hit: event.v_hits) {
                double offset = _m_timing_offset[hit.chipID][hit.channel][hit.memoryCell][hit.bxID%2==0];
                if(offset != 0) {
                    h_delta_ns_corrected->Fill(hit.ns - T0_time - offset);
                        // h_delta_ns_corrected_many_hits->Fill(hit.ns - T0_time - offset);
                }
            }
        }
        h_delta_ns_corrected->Write();
        h_delta_ns_corrected_many_hits->Write();

        h_chip_resolutions->Write();

        // TGraphErrors* gr = new TGraphErrors(v_delay_mean.size(), &(v_chip[0]), &(v_delay_mean[0]), &(v_error_x[0]), &(v_error_y[0]) );
        // gr->Write();

    } else {
        std::cout << "!!! Could not open root file. Histograms are not saved!" << std::endl;
    }

    o_file->Close();
    delete o_file;

}

void TimingCalibration::end() {

    writeRootFile();
    writeCalibration();

    std::cout << "TimingCalibration::end()  " << name()
 	      << " processed " << _nEvt << " events in " << _nRun << " runs "
     	      << std::endl ;
}
