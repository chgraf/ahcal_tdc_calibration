#include "WriteRoot.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <EVENT/LCCollection.h>
#include <EVENT/LCGenericObject.h>
#include <EVENT/CalorimeterHit.h>

#include "MappingProcessor.hh"
#include "Ahc2HardwareConnection.hh" // ----- include for verbosity dependend logging ---------
#include "marlin/VerbosityLevels.h"
#include "marlin/Exceptions.h"

#include "TFile.h"
#include "TF1.h"
#include "TTree.h"

using namespace lcio;
using namespace marlin;
using namespace CALICE;


WriteRoot aWriteRoot;


WriteRoot::WriteRoot() : Processor("WriteRoot") {

    // modify processor description
    _description = "WriteRoot writes out root Tree ..." ;


    // register steering parameters: name, description, class-variable, default value

    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCALRaw",
            "Name of the HCAL input collection for raw data",
            _colName_HCAL_Raw ,
            std::string("EUDAQDataHCAL")
    );

    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCALReco",
            "Name of the HCAL input collection for reco data",
            _colName_HCAL_Reco,
            std::string("Ahc2Calorimeter_Hits")
    );

    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCALTracks",
            "Name of the HCAL input collection for tracked data",
            _colName_HCAL_Tracks,
            std::string("Ahc2Calorimeter_Tracks")
    );

    registerProcessorParameter(
        "isTrackData",
        "Use the track collection",
        _isTrackData,
        false
    );

    registerProcessorParameter(
        "TestbeamCampaign",
        "TestbeamCampaign: Aug15, Jul15",
        _TBCampaign,
        std::string("Aug15")
    );

    registerProcessorParameter(
        "OutputRootFilename",
        "name of the Root output file",
        _fn_root_out,
        std::string("hits.root")
    );

    registerProcessorParameter(
        "MappingProcessorName" ,
        "Name of the MappingProcessor instance that provides the geometry of the detector." ,
        _mappingProcessorName,
        std::string("MyMappingProcessor")
    );

    registerProcessorParameter(
        "HardwareConnectionCollection" ,
        "Name of the Ahc2HardwareConnection Collection",
        _Ahc2HardwareConnectionName,
		std::string("Ahc2HardwareConnection")
    );

}

void WriteRoot::init() {
    std::cout << "Init..." << std::endl;

    _timingManager = new TimingManager();

    // ---------- config ----------
    _timingManager->setNChannels(36);
    _timingManager->setNMemoryCells(16);
    _timingManager->setBxPeriod(4000);
    _timingManager->setRampDeadtime(0.02);

    _nChannels = _timingManager->getNChannels();

    if(_TBCampaign == "Aug15") {
        // v_T0.push_back(std::pair<int, int>(185,29));
        // v_T0.push_back(std::pair<int, int>(177,29));
        v_T0.push_back(std::pair<int, int>(201,29));
        v_T0.push_back(std::pair<int, int>(217,23));
    } else if(_TBCampaign == "Jul15") {
        v_T0.push_back(std::pair<int, int>(169,29));
        v_T0.push_back(std::pair<int, int>(177,23));
        v_T0.push_back(std::pair<int, int>(185,29));
        v_T0.push_back(std::pair<int, int>(217,23));
    }

    _nT0s = v_T0.size();

    streamlog_out(DEBUG) << "   init called  " << std::endl ;

    // usually a good idea to
    printParameters() ;

    _timingManager->setT0s(v_T0);

    _isInitialized = false;

    _nRun = 0 ;
    _nEvt = 0 ;

}

void WriteRoot::processRunHeader( LCRunHeader* run) {

    _hasInitializedRun = false;
    _nRun++ ;
}

void WriteRoot::initRun(LCEvent * evt) {
    const std::vector<std::string>* colNames =  evt->getCollectionNames();
    if(std::find(colNames->begin(), colNames->end(), _colName_HCAL_Reco) != colNames->end()) {
        std::cout << "Using reconstructed data from collection: " << _colName_HCAL_Reco << std::endl;
        std::cout << "Collection names stored in the event: " << std::endl;
        for(auto& n : *colNames) {
            std::cout << n << ", ";
        }
        std::cout << std::endl;
        _isReconstructedData = true;
    } else if(std::find(colNames->begin(), colNames->end(), _colName_HCAL_Raw) != colNames->end()) {
        std::cout << "Using raw data from collection: " << _colName_HCAL_Raw << std::endl;
        std::cout << "Collection names stored in the event: " << std::endl;
        for(auto& n : *colNames) {
            std::cout << n << ", ";
        }
        std::cout << std::endl;
        _isReconstructedData = false;
    } else {
        std::cout << "Could not find any of the specified collecitons..." << std::endl;
        std::cout << "Input raw data collection: " << _colName_HCAL_Raw << std::endl;
        std::cout << "Input reconstructed data collection: " << _colName_HCAL_Reco << std::endl;
        std::cout << "Collection names stored in the event: " << std::endl;
        for(auto& n : *colNames) {
            std::cout << n << ", ";
        }
        std::cout << std::endl;
    }

    _hasInitializedRun = true;
}

void WriteRoot::processEvent( LCEvent * evt ) {

        // std::cout << evt->getCollectionNames()->size() << std::endl;
        // std::cout << _colName_HCAL << "\t" << evt->getCollectionNames()->at(4) << std::endl;
    LCCollection* col = NULL;

    // this has to be done once, but an event is needed for it.
    if(not _isInitialized) {
        _timingManager->initializeGeometry(_mappingProcessorName, evt->getCollection(_Ahc2HardwareConnectionName) );
        _v_chipIDs = _timingManager->getChipIDs();

        _isInitialized = true;
    }

    // checks which collections to use for the run
    if(not _hasInitializedRun) {
        initRun(evt);
    }

    // choose the correct collections
    try {
        if(_isTrackData) {
            col = evt->getCollection( _colName_HCAL_Tracks) ;
        } else if(_isReconstructedData) {
            col = evt->getCollection( _colName_HCAL_Reco) ;
        } else {
            col = evt->getCollection( _colName_HCAL_Raw) ;
        }
    } catch (const std::exception& e) { }




    // this will only be entered if the collection is available
    if( col != NULL ) {

        int nHits = col->getNumberOfElements();

        // stores all the hits in the event
        timing::Event event;

        if(_isTrackData) {
            int nTracks = evt->getParameters().getIntVal("nTracks");
            event.nTracks = nTracks;
        }

        for(int i=0; i< nHits ; i++){
            // load values into hit struct
            timing::Hit hit;

            if(_isReconstructedData) {
                // get the current hit
                CalorimeterHit* p = dynamic_cast<CalorimeterHit*>( col->getElementAt( i ) ) ;

                // hit = _timingManager->FillReconstructedHit(p);
                hit = _timingManager->fillHit(p);

                // BxID has to be set explicitly using the event
                hit.bxID = evt->getParameters().getIntVal("BXID");


            } else {
                // get the current hit
                LCGenericObject* p = dynamic_cast<LCGenericObject*>( col->getElementAt( i ) ) ;

                hit = _timingManager->fillHit(p);

                // throw away memoryCell 0, its broken
                if(hit.memoryCell == 0) continue;
                // just use hitbit == 1
                if(hit.hitbit == 0) continue;
            }

            hit.eventID = _nEvt;
            hit.runID = _nRun;

            if(hit.bxID > 4096) {
                std::cout << "Watch out! Corrupted readout cycle!";
                std::cout << " ... just use data that is converted from the raw .txt files";
                std::cout << " using the newest version of LabviewConverter2.cc";
            }

            // save hit in event
            event.v_hits.push_back(hit);
        }

        _v_events.push_back(event);
    } // end if col != 0

    _nEvt++;
}



void WriteRoot::check( LCEvent * evt ) {
    // nothing to check here - could be used to fill checkplots in reconstruction processor
}



void WriteRoot::end() {

    TFile* o_file = new TFile(_fn_root_out.c_str(), "recreate");

    timing::Hit hit_t;
    timing::Event event_t;

    if(o_file->IsOpen()) {
        TTree* outTree = new TTree("T", "outTree");

        outTree->Branch("eventID", &hit_t.eventID, "eventID/I");
        outTree->Branch("runID", &hit_t.runID, "runID/I");
        outTree->Branch("bxID", &hit_t.bxID, "bxID/I");
        outTree->Branch("chipID", &hit_t.chipID, "chipID/I");
        outTree->Branch("memoryCell", &hit_t.memoryCell, "memoryCell/I");
        outTree->Branch("channel", &hit_t.channel, "channel/I");
        outTree->Branch("nTracks", &event_t.nTracks, "channel/I");
        outTree->Branch("tdc", &hit_t.tdc, "tdc/I");
        outTree->Branch("adc", &hit_t.adc, "adc/I");
        outTree->Branch("mip", &hit_t.mip, "mip/D");
        outTree->Branch("I", &hit_t.I, "I/I");
        outTree->Branch("J", &hit_t.J, "J/I");
        outTree->Branch("K", &hit_t.K, "K/I");

        std::cout << _v_events.size() << std::endl;

        for(auto& event: _v_events) {
            event_t = event;
            // std::cout << "I am in events" << std::endl;
            for(auto& hit: event.v_hits) {
                // std::cout << "I am in" << std::endl;
                hit_t = hit;
                outTree->Fill();
            }
        }

        outTree->Write();
        outTree->Print();

    } else {
        std::cout << "!!! Could not open root file. Histograms are not saved!" << std::endl;
    }

    o_file->Close();
    delete o_file;

    std::cout << "WriteRoot::end()  " << name()
 	      << " processed " << _nEvt << " events in " << _nRun << " runs "
     	      << std::endl ;

}
