#include "TDC_non_linearity.h"

#include <algorithm> //std::sort
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <EVENT/LCCollection.h>
#include <EVENT/LCGenericObject.h>
#include <EVENT/CalorimeterHit.h>

#include "MappingProcessor.hh"
#include "Ahc2HardwareConnection.hh" // ----- include for verbosity dependend logging ---------
#include "marlin/VerbosityLevels.h"
#include "marlin/Exceptions.h"

#include "TFile.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TCanvas.h"


using namespace lcio;
using namespace marlin;
using namespace CALICE;


TDC_non_linearity aTDC_non_linearity;


TDC_non_linearity::TDC_non_linearity() : Processor("TDC_non_linearity") {

    // modify processor description
    _description = "TDC_non_linearity has a first look on the TDC data ..." ;


    // register steering parameters: name, description, class-variable, default value

    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCALRaw",
            "Name of the HCAL input collection for raw data",
            _colName_HCAL_Raw ,
            std::string("EUDAQDataHCAL")
    );

    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCALReco",
            "Name of the HCAL input collection for reco data",
            _colName_HCAL_Reco,
            std::string("Ahc2Calorimeter_Hits")
    );

    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCALTracks",
            "Name of the HCAL input collection for tracked data",
            _colName_HCAL_Tracks,
            std::string("Ahc2Calorimeter_Tracks")
    );

    registerProcessorParameter(
        "isTrackData",
        "Use the track collection",
        _isTrackData,
        false
    );

    registerProcessorParameter(
        "enableT0Linearization",
        "Should the T0 linearization be enabled using the Histogram method?",
        _enableT0Linearization,
        false
    );

    registerProcessorParameter(
        "TestbeamCampaign",
        "TestbeamCampaign: Aug15, Jul15",
        _TBCampaign,
        std::string("Aug15")
    );

    registerProcessorParameter(
        "NonLinearityOutputFile",
        "Name of the file that stores the non-linearity constants",
        _fn_non_linearity_out,
        std::string("tdc_non_linearity.txt")
    );

    registerProcessorParameter(
        "TDCCalibrationFilename",
        "name of the TDC calibration file",
        _fn_tdc_calibration,
        std::string("tdc_calibration.txt")
    );

    registerProcessorParameter(
        "TimingCalibrationFilename",
        "name of the Timing calibration file",
        _fn_timing_calibration,
        std::string("timing_calibration.txt")
    );

    registerProcessorParameter(
        "OutputRootFilename",
        "name of the Root output file",
        _fn_root_out,
        std::string("non_linearity.root")
    );

    registerProcessorParameter(
        "MappingProcessorName" ,
        "Name of the MappingProcessor instance that provides the geometry of the detector." ,
        _mappingProcessorName,
        std::string("MyMappingProcessor")
    );

    registerProcessorParameter(
        "HardwareConnectionCollection" ,
        "Name of the Ahc2HardwareConnection Collection",
        _Ahc2HardwareConnectionName,
		std::string("Ahc2HardwareConnection")
    );

}

void TDC_non_linearity::init() {
    std::cout << "Init..." << std::endl;

    _timingManager = new TimingManager();

    // ---------- config ----------
    _timingManager->setNChannels(36);
    _timingManager->setNMemoryCells(16);
    _timingManager->setBxPeriod(4000);
    _timingManager->setRampDeadtime(0.02);

    _nChannels = _timingManager->getNChannels();


    if(_TBCampaign == "Aug15") {
        // v_T0.push_back(std::pair<int, int>(185,29));
        // v_T0.push_back(std::pair<int, int>(177,29));
        v_T0.push_back(std::pair<int, int>(201,29));
        v_T0.push_back(std::pair<int, int>(217,23));
    } else if(_TBCampaign == "Jul15") {
        v_T0.push_back(std::pair<int, int>(169,29));
        v_T0.push_back(std::pair<int, int>(177,23));
        v_T0.push_back(std::pair<int, int>(185,29));
        v_T0.push_back(std::pair<int, int>(217,23));
    }

    _nT0s = v_T0.size();

    _timingManager->setT0s(v_T0);

    streamlog_out(DEBUG) << "   init called  " << std::endl ;

    // usually a good idea to
    printParameters() ;

    std::cout << "Reading TDC Calibration... " << std::endl;
    _timingManager->readTDCCalibration_extended(_fn_tdc_calibration.c_str());

    std::cout << "Reading Timing Calibration... " << std::endl;
    _timingManager->readTimingCalibration_extended(_fn_timing_calibration.c_str());

    _isInitialized = false;

    _nRun = 0 ;
    _nEvt = 0 ;

}

void TDC_non_linearity::init_histograms() {

    // ---------- Init Histos ----------
    h_nonLin = new TH1D("h_nonLin", "significance of fit slope", 2000, -100, 100);
    h_nonLin->GetXaxis()->SetTitle("sigma");
    h_nonLin->GetYaxis()->SetTitle("#");

    //ToDo: Shouldnt this go into the analysis processor? At least the corrected ones
    for(auto& chipID : _v_chipIDs) {
        TH1I* h_even;
        TH1I* h_odd;
        TH1I* h_corrected_even;
        TH1I* h_corrected_odd;
        // for(int memoryCell=0; memoryCell < 16; memoryCell++) { //run through all memory cells, except the first one (its broken)
            // BXID even
        std::stringstream hist_name;
        hist_name.str(std::string());
        hist_name << "h_TDCs_CID_" << chipID << "_even";

        std::stringstream hist_title;
        hist_title.str(std::string());
        hist_title << "TDCs for hitbit == 1, Channel " << chipID << ", BXID even";

        TH1I* h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 250, 1, 5001);
        h_tmp->GetXaxis()->SetTitle("tdc");
        h_tmp->GetYaxis()->SetTitle("#");
        h_even = h_tmp;

        // BXID odd
        hist_name.str(std::string());
        hist_name << "h_TDCs_CID_" << chipID << "_odd";

        hist_title.str(std::string());
        hist_title << "TDCs for hitbit == 1, Channel " << chipID << ", BXID odd";

        h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 250, 1, 5001);
        h_tmp->GetXaxis()->SetTitle("tdc");
        h_tmp->GetYaxis()->SetTitle("#");
        h_odd = h_tmp;

        mh_TDCs_CIDs_even.insert(std::pair<int, TH1I* >(chipID, h_even) );
        mh_TDCs_CIDs_odd.insert(std::pair<int, TH1I* >(chipID, h_odd) );

        // ----- Corrected Histograms -----

        hist_name.str(std::string());
        hist_name << "h_TDCs_CID_" << chipID << "_corrected_even";

        hist_title.str(std::string());
        hist_title << "TDCs for hitbit == 1, Channel " << chipID << ", BXID even, Corrected";

        h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 250, 1, 5001);
        h_tmp->GetXaxis()->SetTitle("tdc");
        h_tmp->GetYaxis()->SetTitle("#");
        h_corrected_even = h_tmp;

        // BXID odd
        hist_name.str(std::string());
        hist_name << "h_TDCs_CID_" << chipID << "_corrected_odd";

        hist_title.str(std::string());
        hist_title << "TDCs for hitbit == 1, Channel " << chipID << ", BXID odd, Corrected";

        h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 250, 1, 5001);
        h_tmp->GetXaxis()->SetTitle("tdc");
        h_tmp->GetYaxis()->SetTitle("#");
        h_corrected_odd = h_tmp;

        mh_TDCs_CIDs_corrected_even.insert(std::pair<int, TH1I* >(chipID, h_corrected_even) );
        mh_TDCs_CIDs_corrected_odd.insert(std::pair<int, TH1I* >(chipID, h_corrected_odd) );

    }
}


void TDC_non_linearity::processRunHeader( LCRunHeader* run) {

    _hasInitializedRun = false;
    _nRun++ ;
}



void TDC_non_linearity::processEvent( LCEvent * evt ) {

    LCCollection* col = NULL;

    // Do we have reconstructed or raw data?
    if(not _hasInitializedRun) {
        const std::vector<std::string>* colNames =  evt->getCollectionNames();
        if(std::find(colNames->begin(), colNames->end(), _colName_HCAL_Reco) != colNames->end()) {
            _isReconstructedData = true;
        } else if(std::find(colNames->begin(), colNames->end(), _colName_HCAL_Raw) != colNames->end()) {
            _isReconstructedData = false;
        } else {
            std::cout << "Could not find any of the specified collecitons..." << std::endl;
            std::cout << "Input raw data collection: " << _colName_HCAL_Raw << std::endl;
            std::cout << "Input reconstructed data collection: " << _colName_HCAL_Reco << std::endl;
            std::cout << "Collection names stored in the event: " << std::endl;
            for(auto& n : *colNames) {
                std::cout << n << ", ";
            }
            std::cout << std::endl;
        }
        _hasInitializedRun = true;
    }

    // Fills the hardwareConnection Container
    // and the histograms // This has only be done // Cannot be done in init() because we need one Event for the hardwareConnection
    // Or is there another way?
    if(not _isInitialized) {
        _timingManager->initializeGeometry(_mappingProcessorName, evt->getCollection(_Ahc2HardwareConnectionName) );
        _v_chipIDs = _timingManager->getChipIDs();
        // _nChips = _v_chipIDs.size();

        init_histograms();
        _isInitialized = true;
    }

    // Get the correct collections. Note that to use Tracks the flag _isTrackData
    // has to be set as a Marlin variable to true
    try {
        if(_isTrackData) {
            col = evt->getCollection( _colName_HCAL_Tracks) ;
        } else if(_isReconstructedData) {
            col = evt->getCollection( _colName_HCAL_Reco) ;
        } else {
            col = evt->getCollection( _colName_HCAL_Raw) ;
        }
    } catch (const std::exception& e) { }


    // this will only be entered if the collection is available
    if( col != NULL ) {

        int nHits = col->getNumberOfElements();

        // stores all the hits in the event
        timing::Event event;

        for(int i=0; i< nHits ; i++){
            // std::cout << ".";

            // load values into hit struct
            timing::Hit  hit;

            if(_isReconstructedData) {
                // get the current hit
                // std::cout << "\tbefore hit assignment" << std::endl;
                // std::cout << "Encoding: " << _ahcMapper->getDecoder()->getCellIDEncoding() << std::endl;
                CalorimeterHit* p = dynamic_cast<CalorimeterHit*>( col->getElementAt( i ) ) ;

                // hit = _timingManager->FillReconstructedHit(p);
                hit = _timingManager->fillHit(p);

                // BxID has to be set explicitly using the event
                hit.bxID = evt->getParameters().getIntVal("BXID");


            } else {
                // get the current hit
                LCGenericObject* p = dynamic_cast<LCGenericObject*>( col->getElementAt( i ) ) ;

                hit = _timingManager->fillHit(p);

                // throw away memoryCell 0, its broken
                if(hit.memoryCell == 0) continue;
                // just use hitbit == 1
                if(hit.hitbit == 0) continue;
            }

            if(hit.bxID > 4096) {
                std::cout << "Watch out! Corrupted readout cycle!";
                std::cout << " ... just use data that is converted from the raw .txt files";
                std::cout << " using the newest version of LabviewConverter2.cc";
            }

            // convert tdc to ns
            hit.ns = _timingManager->tdcToNs(hit.chipID, hit.channel, hit.memoryCell, hit.bxID, hit.tdc);

            // if(not _isReconstructedData) {
                // count the number of triggered T0s
            hit.isT0 = false;
            if(std::find(v_T0.begin(), v_T0.end(), std::pair<int, int>(hit.chipID, hit.channel)) != v_T0.end()) {
                int adc_T0_thr = 0;
                //Cutting on the T0 adc spectrum to cut away noise hits
                if(_TBCampaign == "Jul15") {
                    adc_T0_thr = 500;
                } else if(_TBCampaign == "Aug15") {
                    adc_T0_thr = 1100;
                }

                hit.ns = _timingManager->tdcToNs_T0(hit.chipID, hit.channel, hit.memoryCell, hit.bxID, hit.tdc);


                //cutting the second peak in the T0 adc spectrum
                if(hit.ns > 0 && (hit.adc > adc_T0_thr || _isReconstructedData)) {

                    for(int k=0; k < _nT0s; k++) {
                        if(std::pair<int, int>(hit.chipID, hit.channel) == v_T0[k]) {
                            // std::cout << "hit at T0ID: " << k << std::endl;
                            hit.T0ID = k;
                            break;
                        }
                    }

                    event.T0s[hit.T0ID] = hit;

                    event.nT0s++;
                    hit.isT0 = true;
                }
            }

            // save hit in event
            event.v_hits.push_back(hit);
        }

        // only take events where all T0 channels reported a hit
        if(event.nT0s == _nT0s) {
            // FIXME: how to handle this case? When the T0s differ too much
            // QuickFix: just throw away
            if(abs(event.T0s[0].ns - event.T0s[1].ns) < 12) {
                for(auto& hit: event.v_hits) {
                    if(hit.ns < 0) continue; // then the calibration has not worked.

                    if(hit.bxID % 2 == 0) {
                        mh_TDCs_CIDs_even[hit.chipID]->Fill(hit.tdc);
                    } else {
                        mh_TDCs_CIDs_odd[hit.chipID]->Fill(hit.tdc);
                    }
                }
                // std::cout << event.T0s[0]->adc << " " << event.T0s[1]->adc << std::endl;
                _v_events.push_back(event);
            }

        } // end if nT0s == 3
    } // end if col != 0


    //-- note: this will not be printed if compiled w/o MARLINDEBUG=1 !

    streamlog_out(DEBUG) << "   processing event: " << evt->getEventNumber()
        << "   in run:  " << evt->getRunNumber() << std::endl ;

    _nEvt++;
}


void TDC_non_linearity::check( LCEvent * evt ) {
    // nothing to check here - could be used to fill checkplots in reconstruction processor
}

void TDC_non_linearity::writeNonLinearityCorrection() {
    std::cout << "Writing non-linearity constants to " << _fn_non_linearity_out << std::endl;

    ofstream out_file;
    out_file.open(_fn_non_linearity_out.c_str());
    // //FIXME: Bring this back to the old way!!!!!
    out_file << "T0 linearization is enabled\t" << _enableT0Linearization << std::endl;
    out_file << "T0 linearization constants:" << std::endl;
    out_file << "ChipID\tChannel\tnl_offset_even\tnl_slope_even\tnl_offset_odd\tnl_slope_odd" << std::endl;

    for(auto& T0: v_T0){
        int T0_chip = T0.first;
        out_file << T0.first << "\t" << T0.second << "\t";
        out_file << _m_nonLinParameters_even[T0_chip].first << "\t" << _m_nonLinParameters_even[T0_chip].second << "\t";
        out_file << _m_nonLinParameters_odd[T0_chip].first << "\t" << _m_nonLinParameters_odd[T0_chip].second << std::endl;
    }

    out_file << "Quadratic correction function for each chip: a + b*t + c*t^2" << std::endl;
    out_file << "ChipID\ta_even\tb_even\tc_even\ta_odd\tb_odd\tc_odd" << std::endl;

    for(auto& chipID: _v_chipIDs) {
        out_file << chipID << "\t";
        out_file << _mv_quad_fit[chipID][true][0] << "\t";
        out_file << _mv_quad_fit[chipID][true][1] << "\t";
        out_file << _mv_quad_fit[chipID][true][2] << "\t";
        out_file << _mv_quad_fit[chipID][false][0] << "\t";
        out_file << _mv_quad_fit[chipID][false][1] << "\t";
        out_file << _mv_quad_fit[chipID][false][2] << std::endl;
    }

    out_file.close();
}

        //fill vectors for the quadratic nonLinearity correction
void TDC_non_linearity::prepare_quadratic_nonLinearity_correction(
      std::map<int, std::map<bool, std::vector<double> > > &mv_T0,
      std::map<int, std::map<bool, std::vector<double> > > &mv_ns_T0
) {
    for(auto& event: _v_events) {
        double T0_time = 0.;

        //calculate T0 time of the event
        for(auto& T0_hit: event.T0s) {
            timing::Hit h = T0_hit.second;
            double ns = h.ns; // use the conventional not linearized time

            if(_enableT0Linearization) { // if T0 linearization is enabled: use the nl correction
                double nl_offset = h.bxID%2==0 ? _m_nonLinParameters_even[h.chipID].first : _m_nonLinParameters_odd[h.chipID].first;
                double nl_slope = h.bxID%2==0 ? _m_nonLinParameters_even[h.chipID].second : _m_nonLinParameters_odd[h.chipID].second;
                if(nl_offset != 0 && nl_slope != 0) {
                        ns = _timingManager->tdcToNs_nonLin(h.chipID, h.memoryCell, h.bxID, h.tdc, nl_offset, nl_slope);
                }
            }
            T0_time += ns;
        }

        //mean T0 time
        T0_time = T0_time / (double)event.nT0s;
        event.T0_time = T0_time;

        //go through all hits
        for(auto& hit: event.v_hits) {
            //get the delay for this channel from the timingManager
            double offset = _timingManager->getOffset(hit.chipID, hit.channel, hit.memoryCell, hit.bxID);

            if(offset != 0 && hit.ns > 0) {
                mv_ns_T0[hit.chipID][hit.bxID%2==0].push_back(hit.ns - T0_time - offset);
                mv_T0[hit.chipID][hit.bxID%2==0].push_back(T0_time);
            }
        }
    }
}

// make linear fit in all histograms
void TDC_non_linearity::histogram_nonLinearity_correction(bool isEven, TFile* o_file) {

    if(o_file->IsOpen()) {
        o_file->Cd("");
    }

    //Linear fit
    TF1 *f_lin = new TF1("f_lin","[0] + [1]*x", 0, 4000);

    for(auto& chipID : _v_chipIDs) {
        TH1I* histo = (isEven ? mh_TDCs_CIDs_even[chipID]: mh_TDCs_CIDs_odd[chipID]);

        double min_edge = _timingManager->getTdcPedestal(chipID, _nChannels, 1, isEven, 9) + 200;
        double max_edge = _timingManager->getTdcMaxEdge(chipID, _nChannels, 1, isEven, 9) - 500;

        f_lin->SetParameter(0,histo->GetEntries()/(double)histo->GetNbinsX());
        f_lin->SetParameter(1,-0.01);
        histo->Fit("f_lin","L","",min_edge, max_edge);
        histo->Write();

        if(histo->GetEntries() > 1000) {
            double nl_offset = f_lin->GetParameter(0);
            double nl_slope = f_lin->GetParameter(1);

            h_nonLin->Fill(nl_slope / f_lin->GetParError(1));

            if(isEven) _m_nonLinParameters_even[chipID] = std::make_pair(nl_offset, nl_slope);
            else _m_nonLinParameters_odd[chipID] = std::make_pair(nl_offset, nl_slope);
        } else {
            if(isEven) _m_nonLinParameters_even[chipID] = std::make_pair(0.,0.);
            else _m_nonLinParameters_odd[chipID] = std::make_pair(0., 0.);
        }
    }
}

//produce non linearity graph for each channel and perform quadratic fit.

void TDC_non_linearity::quadratic_nonLinearity_correction(
      std::map<int, std::map<bool, std::vector<double> > > &mv_T0,
      std::map<int, std::map<bool, std::vector<double> > > &mv_ns_T0,
      bool isEven,
      TFile* o_file
) {

    if(o_file->IsOpen()) o_file->Cd("");

    for(auto& chipID : _v_chipIDs) {
        //the boolean key is the bxID even (true), or odd (false)
        TGraph* gr_nonLin = new TGraph(mv_T0[chipID][isEven].size(), &(mv_T0[chipID][isEven][0]), &(mv_ns_T0[chipID][isEven][0]));
        TF1* f_pol2 = new TF1("f_pol2", "pol2", 0,4000);

        std::stringstream gr_name;
        gr_name.str(std::string());
        gr_name << "gr_nl_" << chipID << (isEven ? "_even" : "_odd");

        std::stringstream gr_title;
        gr_title.str(std::string());
        gr_title << "Non-Linearity Correction " << chipID << ", BXID " << (isEven ? "even": "odd");

        gr_nonLin->SetName(gr_name.str().c_str());
        gr_nonLin->SetTitle(gr_title.str().c_str());
        gr_nonLin->GetYaxis()->SetTitle("ns - T0");
        gr_nonLin->GetXaxis()->SetTitle("T0");

        f_pol2->SetParameter(0,0.);
        f_pol2->SetParameter(1,-0.1);
        f_pol2->SetParameter(2,1.0e-5);
        gr_nonLin->Fit("f_pol2", "ROB=0.75", "", 300, 3400);
        _mv_quad_fit[chipID][isEven].push_back(f_pol2->GetParameter(0));
        _mv_quad_fit[chipID][isEven].push_back(f_pol2->GetParameter(1));
        _mv_quad_fit[chipID][isEven].push_back(f_pol2->GetParameter(2));
        // std::cout << f_pol2->GetParameter(1) / (double)f_pol2->GetParameter(2) << "\t should be ~4000" << std::endl;

        if(o_file->IsOpen()) gr_nonLin->Write();

    }
}


void TDC_non_linearity::end() {

    TFile* o_file = new TFile("non_linearity.root", "recreate");

    if(o_file->IsOpen()) {

        // TH1D* h_delta_ns_corrected = new TH1D("h_delta_ns_corrected", "difference of t to T0 mean of event, offset corrected", 3000, -500, 1000);
        // TH1D* h_delta_ns_nonLin = new TH1D("h_delta_ns_nonLin", "difference of t to T0 mean of event, non-Linearity corrected", 3000, -500, 1000);
        // TH1D* h_delta_ns_T0_nonLin = new TH1D("h_delta_ns_T0_nonLin", "difference of t to T0 mean of event; T0s are non-Linearity corrected", 3000, -500, 1000);
        // TH1D* h_T0_diff = new TH1D("h_T0_diff", "Difference between T0s; T0s are non-Linearity corrected", 3000, -500, 1000);
        // TH1D* h_nonLin_diff = new TH1D("h_nonLin_diff", "difference between calibrated ns with and without nonLinear Correction", 3000, -100, 200);

        TH1D* h_nonLin_corrected_diff = new TH1D("h_nonLin_corrected_diff", "difference between calibrated ns with and without nonLinear Correction", 2000, -100, 500);
        TH1D* h_delta_nonLin_corrected = new TH1D("h_delta_nonLin_corrected", "difference of t to T0 mean of event, full non-Linearity corrected", 3000, -500, 1000);
        // TH1D* h_delta_nonLin_corrected_2 = new TH1D("h_delta_nonLin_corrected_2", "Second method", 3000, -500, 1000);;

        // TH1D* h_nHits = new TH1D("h_nHits", "number of hits per event", 200, 0, 200);

        // perform the nonLinearity correction with the histogram method.
        // mainly for linearizing the T0s.
        // may be turned off by the following if:
        // if(_enableT0Linearization) {
        histogram_nonLinearity_correction(true, o_file);
        histogram_nonLinearity_correction(false, o_file);
        // }

        h_nonLin->Write();

        std::map<int, std::map<bool, std::vector<double> > > mv_ns_T0;
        std::map<int, std::map<bool, std::vector<double> > > mv_T0;

        //perform fits in graphs to correct for the nonlinearity of all channels
        //with respect to the T0s. Do this for even and odd bunchcrossing IDs.
        prepare_quadratic_nonLinearity_correction(mv_T0, mv_ns_T0);
        quadratic_nonLinearity_correction(mv_T0, mv_ns_T0, true, o_file);
        quadratic_nonLinearity_correction(mv_T0, mv_ns_T0, false, o_file);

        writeNonLinearityCorrection();

        // FIXME: This may be deleted and may actually more belong in the analysis Processor

        _timingManager->readNonLinearityCorrection(_fn_non_linearity_out.c_str());

        // Again go through all events and hits and do the final non-Linearity correction
        for(auto& event: _v_events) {
            double T0_time = event.T0_time;

            for(auto& hit: event.v_hits) {
                bool isEven = hit.bxID%2==0;
                double offset = _timingManager->getOffset(hit.chipID, hit.channel, hit.memoryCell, hit.bxID);
                // double nl_offset = isEven ? _m_nonLinParameters_even[hit.chipID].first : _m_nonLinParameters_odd[hit.chipID].first;
                // double nl_slope = isEven ? _m_nonLinParameters_even[hit.chipID].second : _m_nonLinParameters_odd[hit.chipID].second;

                // double ns = _timingManager->tdcToNs(hit.chipID, hit.memoryCell, hit.bxID, hit.tdc);
                double ns = _timingManager->tdcToNs_nonLin(hit.chipID, hit.channel, hit.memoryCell, hit.bxID, hit.tdc, T0_time);
                // double ns = hit.ns;

                // ns -= (_mv_quad_fit[hit.chipID][isEven][0] + T0_time*_mv_quad_fit[hit.chipID][isEven][1] + T0_time*T0_time*_mv_quad_fit[hit.chipID][isEven][2]);
                // ns -= (ns*_mv_quad_fit[hit.chipID][isEven][1] + ns*ns*_mv_quad_fit[hit.chipID][isEven][2]);
                // double ns_2 = _timingManager->tdcToNs_nonLin(hit.chipID, hit.memoryCell, hit.bxID, hit.tdc, nl_offset, nl_slope);

                if(isEven) mh_TDCs_CIDs_corrected_even[hit.chipID]->Fill(ns);
                else mh_TDCs_CIDs_corrected_odd[hit.chipID]->Fill(ns);

                if(offset != 0 && ns > 500 && ns < 3500) {
                    h_delta_nonLin_corrected->Fill(ns - T0_time - offset);
                    // h_delta_nonLin_corrected_2->Fill(ns_2 - T0_time - offset);
                    h_nonLin_corrected_diff->Fill(ns - hit.ns);
                }
            }
        }


        h_nonLin->Reset();
        //Write histograms again after non-Linearity correction
        for(auto& chipID : _v_chipIDs) {

            TF1 *f_lin = new TF1("f_lin","[0] + [1]*x", 0, 4000);
            //Even bxID
            auto fit_histos = [&](TH1I* histo) {
                double min_edge = 300;
                double max_edge = 3250;
                std::cout << min_edge << "\t" << max_edge << std::endl;
                histo->Fit("f_lin","L","",min_edge, max_edge);
                histo->Write();
                if(histo->GetEntries() > 1000) {
                    double nl_offset = f_lin->GetParameter(0);
                    double nl_slope = f_lin->GetParameter(1);

                    h_nonLin->Fill(nl_slope / f_lin->GetParError(1));

                    return std::make_pair(nl_offset, nl_slope);
                } else {
                    return std::make_pair(0.,0.);
                }
            };

            fit_histos(mh_TDCs_CIDs_corrected_even[chipID]);
            fit_histos(mh_TDCs_CIDs_corrected_odd[chipID]);
        }

        h_nonLin->Write();

        // h_nonLin_diff->Write();
        // h_delta_ns_nonLin->Write();
        // h_delta_ns_T0_nonLin->Write();
        // h_T0_diff->Write();

        h_delta_nonLin_corrected->Write();
        h_nonLin_corrected_diff->Write();
        // h_delta_nonLin_corrected_2->Write();

    } else {
        std::cout << "!!! Could not open root file. Histograms are not saved!" << std::endl;
    }


    o_file->Close();
    delete o_file;

    std::cout << "TDC_non_linearity::end()  " << name()
 	      << " processed " << _nEvt << " events in " << _nRun << " runs "
     	      << std::endl ;

}
