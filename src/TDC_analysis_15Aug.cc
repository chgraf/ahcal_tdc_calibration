#include "TDC_analysis_15Aug.h"
#include <algorithm> //std::sort
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <EVENT/LCCollection.h>
#include <EVENT/LCGenericObject.h>

// ----- include for verbosity dependend logging ---------
#include "marlin/VerbosityLevels.h"
#include "TFile.h"
#include "TGraph.h"
#include "TCanvas.h"

using namespace lcio ;
using namespace marlin ;


TDC_analysis_15Aug aTDC_analysis_15Aug ;


TDC_analysis_15Aug::TDC_analysis_15Aug() : Processor("TDC_analysis_15Aug") {

    // modify processor description
    _description = "TDC_analysis_15Aug has a first look on the TDC data ..." ;


    // register steering parameters: name, description, class-variable, default value
    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCAL" ,
            "Name of the HCAL input collection"  ,
            _colName_HCAL ,
            std::string("EUDAQDataHCAL")
    );

    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameECAL" ,
            "Name of the ECAL input collection"  ,
            _colName_ECAL ,
            std::string("EUDAQDataECAL")
    );
}



void TDC_analysis_15Aug::init() {
    // ---------- config ----------
    _first_chip = 140;
    _nChips = 120;

    // ---------- Init Histos ----------
    h_tdcSlopes = new TH1D("h_tdcSlopes", "TDC Slopes", 500, -5, 5);
    h_delta_ns = new TH1D("h_delta_ns", "difference of t to mean time of event", 20000, -10000, 10000);

    h_memCell_pedestal = new TH1D("h_memCell_pedestal", "Differences in pedestal of all the memCells in one chip to the mean in tdc", 800, -200, 200);
    h_memCell_slope = new TH1D("h_memCell_slope", "Differences in slope of all the memCells in one chip to the mean in tdc", 800, -2, 2);

    h_T0_diff_0 = new TH1D("h_T0_diff_0", "Differences of T0 0 to the mean T0 of this event", 2000, -1000, 1000);
    h_T0_diff_1 = new TH1D("h_T0_diff_1", "Differences of T0 1 to the mean T0 of this event", 2000, -1000, 1000);
    h_T0_diff_2 = new TH1D("h_T0_diff_2", "Differences of T0 2 to the mean T0 of this event", 2000, -1000, 1000);

    h2_T0_vs_mT0s_0 = new TH2D("h2_T0_vs_mT0s_0", "T0 0 vs mean of all other T0s", 1500, -1000, 5000, 1500, -1000, 5000);
    h2_T0_vs_mT0s_1 = new TH2D("h2_T0_vs_mT0s_1", "T0 1 vs mean of all other T0s", 1500, -1000, 5000, 1500, -1000, 5000);
    h2_T0_vs_mT0s_2 = new TH2D("h2_T0_vs_mT0s_2", "T0 2 vs mean of all other T0s", 1500, -1000, 5000, 1500, -1000, 5000);
    h2_T0_vs_mT0s_0->SetOption("COLZ");
    h2_T0_vs_mT0s_1->SetOption("COLZ");
    h2_T0_vs_mT0s_2->SetOption("COLZ");

    h2_T00_vs_T01 = new TH2D("h2_T00_vs_T01", "T0 0 vs T0 1 ns", 1500, -1000, 5000, 1500, -1000, 5000);
    h2_T00_vs_T02 = new TH2D("h2_T00_vs_T02", "T0 0 vs T0 2 ns", 1500, -1000, 5000, 1500, -1000, 5000);
    h2_T01_vs_T02 = new TH2D("h2_T01_vs_T02", "T0 1 vs T0 2 ns", 1500, -1000, 5000, 1500, -1000, 5000);
    h2_T00_vs_T01->SetOption("COLZ");
    h2_T00_vs_T02->SetOption("COLZ");
    h2_T01_vs_T02->SetOption("COLZ");

    h2_T00_vs_T01_tdc = new TH2D("h2_T00_vs_T01_tdc", "T0 0 vs T0 1 tdc", 1500, -1000, 5000, 1500, -1000, 5000);
    h2_T00_vs_T02_tdc = new TH2D("h2_T00_vs_T02_tdc", "T0 0 vs T0 2 tdc", 1500, -1000, 5000, 1500, -1000, 5000);
    h2_T01_vs_T02_tdc = new TH2D("h2_T01_vs_T02_tdc", "T0 1 vs T0 2 tdc", 1500, -1000, 5000, 1500, -1000, 5000);
    h2_T00_vs_T01_tdc->SetOption("COLZ");
    h2_T00_vs_T02_tdc->SetOption("COLZ");
    h2_T01_vs_T02_tdc->SetOption("COLZ");

    h2_debug_T0_adcs = new TH2D("h2_debug_T0_adcs", "T0 1 vs T0 2 adc", 500, 0, 4000, 500, 0, 4000);

    //v_T0.push_back(std::pair<int, int>(169,29));
    // v_T0.push_back(std::pair<int, int>(177,23));
    v_T0.push_back(std::pair<int, int>(185,29));
    v_T0.push_back(std::pair<int, int>(201,29)); // noisy
    //v_T0.push_back(std::pair<int, int>(211,6)); // broken
    v_T0.push_back(std::pair<int, int>(217,23));

    for(int i=0; i<_nChips; i++) {
        int chipID = _first_chip+i;

        tmp_count_T0s.push_back(0); // 0
        tmp_count_T0s.push_back(0); // 1
        tmp_count_T0s.push_back(0); // 2
        tmp_count_T0s.push_back(0); // 3
        tmp_count_T0s.push_back(0); // 4

        std::stringstream hist_name;
        hist_name << "h_TDCs_CID_" << chipID << "_";

        std::stringstream hist_title;
        hist_title << "TDCs for hitbit == 1, Channel " << chipID;

        TH1I* h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 500, 1, 10000);
        mh_TDCs_CIDs.insert( std::pair<int, TH1I*>(chipID, h_tmp) );

        std::vector<TH1I*> vh_even;
        std::vector<TH1I*> vh_odd;
        for(int memoryCell=0; memoryCell < _nMemoryCells; memoryCell++) { //run through all memory cells, except the first one (its broken)
            // BXID even
            hist_name.str(std::string());
            hist_name << "h_TDCs_CID_" << chipID << "_" << memoryCell << "_even";

            hist_title.str(std::string());
            hist_title << "TDCs for hitbit == 1, Channel " << chipID << ", MemoryCell " << memoryCell << ", BXID even";

            h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 4000, 1, 4001);
            vh_even.push_back(h_tmp);

            // BXID odd
            hist_name.str(std::string());
            hist_name << "h_TDCs_CID_" << chipID << "_" << memoryCell << "_odd";

            hist_title.str(std::string());
            hist_title << "TDCs for hitbit == 1, Channel " << chipID << ", MemoryCell " << memoryCell << ", BXID odd";

            h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 4000, 1, 4001);
            vh_odd.push_back(h_tmp);
        }
        mh_TDCs_CIDs_even.insert(std::pair<int, std::vector<TH1I*> >(chipID, vh_even) );
        mh_TDCs_CIDs_odd.insert(std::pair<int, std::vector<TH1I*> >(chipID, vh_odd) );
    }

    //  T0s
    for(auto& p_T0 : v_T0) {
        std::stringstream hist_name;
        hist_name << "h_TDCs_T0s_" << p_T0.first << "_" << p_T0.second;

        std::stringstream hist_title;
        hist_title << "TDCs for hitbit == 1, T0 " << p_T0.first << " " << p_T0.second;

        TH1I* h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 500, 0, 4000);
        vh_TDCs_T0.push_back(h_tmp);

        hist_name.str(std::string());
        hist_name << "h_ADCs_T0s_" << p_T0.first << "_" << p_T0.second;

        hist_title.str(std::string());
        hist_title << "ADCs for hitbit == 1, T0 " << p_T0.first << " " << p_T0.second;

        h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 500, 0, 4000);
        vh_ADCs_T0.push_back(h_tmp);
    }

    streamlog_out(DEBUG) << "   init called  " << std::endl ;

    // usually a good idea to
    printParameters() ;

    if(not isCalibrationMode) {
        std::cout << "Reading Calibration" << std::endl;
        readCalibration();
    } else {
        std::cout << "Calibration mode!" << std::endl;
    }

    _nRun = 0 ;
    _nEvt = 0 ;

}

void TDC_analysis_15Aug::processRunHeader( LCRunHeader* run) {

    _nRun++ ;
}



void TDC_analysis_15Aug::processEvent( LCEvent * evt ) {

    LCCollection* col = evt->getCollection( _colName_HCAL ) ;

    // this will only be entered if the collection is available
    if( col != NULL ){

        int nHits = col->getNumberOfElements()  ;

        // stores all the hits in the event
        Event event;

        double mean_time_ns = 0;

        for(int i=0; i< nHits ; i++){

            // get the current hit
            LCGenericObject* p = dynamic_cast<LCGenericObject*>( col->getElementAt( i ) ) ;

            // load values into hit struct
            Hit hit;
            hit.bxID = p->getIntVal(1);
            hit.chipID = p->getIntVal(2);
            hit.memoryCell = p->getIntVal(3);
            hit.channel = p->getIntVal(4);
            hit.tdc = p->getIntVal(5);
            hit.adc = p->getIntVal(6);
            hit.hitbit = p->getIntVal(7);

            // throw away memoryCell 0, its broken
            if(hit.memoryCell == 0) continue;
            // just use hitbit == 1
            if(hit.hitbit == 0) continue;

            // convert tdc to ns
            if(not isCalibrationMode) {
                hit.ns = tdcToNs(hit.chipID, hit.memoryCell, hit.bxID, hit.tdc);
                //std::cout << tdc << std::endl;
            } else {
                hit.ns = (double)hit.tdc;
            }

            // count the number of triggered T0s
            hit.isT0 = false;
            if(std::find(v_T0.begin(), v_T0.end(), std::pair<int, int>(hit.chipID, hit.channel)) != v_T0.end()) {
                if(std::pair<int, int>(hit.chipID, hit.channel) == v_T0[0]) hit.T0ID = 0;
                else if(std::pair<int, int>(hit.chipID, hit.channel) == v_T0[1]) hit.T0ID = 1;
                else if(std::pair<int, int>(hit.chipID, hit.channel) == v_T0[2]) hit.T0ID = 2;
                // else if(std::pair<int, int>(hit.chipID, hit.channel) == v_T0[3]) hit.T0ID = 3;
                hit.isT0 = true;

                // cut out the second peak in the T0 adc spectrum
                if(hit.adc > 1000 && hit.ns > 0) {
                    event.T0s[hit.T0ID] = hit;
                    vh_TDCs_T0[hit.T0ID]->Fill(hit.tdc);
                    vh_ADCs_T0[hit.T0ID]->Fill(hit.adc);
                    //std::cout << event.T0s[hit.T0ID]->ns << "\t" << hit.T0ID << std::endl;
                    event.nT0s++;
                 }
            }

            mean_time_ns += hit.ns;

            // Fill the tdc spectra
            if(hit.chipID >= _first_chip && hit.chipID < _first_chip + _nChips) {
                mh_TDCs_CIDs[hit.chipID]->Fill(hit.ns); // ns instead of tdc here
                if(hit.bxID % 2 == 0) {
                    mh_TDCs_CIDs_even[hit.chipID][hit.memoryCell]->Fill(hit.tdc);
                } else {
                    mh_TDCs_CIDs_odd[hit.chipID][hit.memoryCell]->Fill(hit.tdc);
                }
            }

            // save hit in event
            event.v_hits.push_back(hit);
        }

        // for statistics
        tmp_count_T0s[event.nT0s]++;

        // in calibration mode: use all the data
        if(isCalibrationMode) {
            _v_events.push_back(event);
        } else {

            // only take events where all 4 T0 channels reported a hit
            if(event.nT0s == 3) {

                if(event.v_hits.size() > 0) {
                    mean_time_ns = mean_time_ns / (double)event.v_hits.size();
                }

                for(auto& hit : event.v_hits) {
                    h_delta_ns->Fill(hit.ns-mean_time_ns);
                }

                // Build vectors for scatter T0 Th2d
                double T0_0_ns = event.T0s[0].ns;
                double T0_1_ns = event.T0s[1].ns;
                double T0_2_ns = event.T0s[2].ns;

                double mean_T0_ns = (T0_0_ns + T0_1_ns + T0_2_ns) / 3.;
                h_T0_diff_0->Fill(T0_0_ns - mean_T0_ns);
                h_T0_diff_1->Fill(T0_1_ns - mean_T0_ns);
                h_T0_diff_2->Fill(T0_2_ns - mean_T0_ns);

                v_ns_T0_0.push_back(T0_0_ns);
                v_ns_T0_1.push_back(T0_1_ns);
                v_ns_T0_2.push_back(T0_2_ns);

                h2_T00_vs_T01_tdc->Fill(event.T0s[0].tdc, event.T0s[1].tdc);
                h2_T00_vs_T02_tdc->Fill(event.T0s[0].tdc, event.T0s[2].tdc);
                h2_T01_vs_T02_tdc->Fill(event.T0s[1].tdc, event.T0s[2].tdc);

                v_ns_T0_12.push_back((T0_1_ns + T0_2_ns) / 2.);
                v_ns_T0_02.push_back((T0_0_ns + T0_2_ns) / 2.);
                v_ns_T0_01.push_back((T0_0_ns + T0_1_ns) / 2.);

                // FIXME: how to handle this case? When the T0s differ too much
                // QuickFix: just throw away
                if(abs(event.T0s[0].ns - event.T0s[1].ns) > 60) {
                    // std::cout << event.T0s[0]->adc << " " << event.T0s[1]->adc << std::endl;
                    h2_debug_T0_adcs->Fill(event.T0s[0].adc, event.T0s[0].adc);
                }

                _v_events.push_back(event);


            } // end if nT0s == 3
        } // end isCalibrationMode
    } // end if col != 0


    //-- note: this will not be printed if compiled w/o MARLINDEBUG=1 !

    streamlog_out(DEBUG) << "   processing event: " << evt->getEventNumber()
        << "   in run:  " << evt->getRunNumber() << std::endl ;

    _nEvt ++ ;
}



void TDC_analysis_15Aug::check( LCEvent * evt ) {
    // nothing to check here - could be used to fill checkplots in reconstruction processor
}

/* Calculates the threshold for edge detection by the median of the entries */
double TDC_analysis_15Aug::calculateTDCMedian(TH1I* histo) {
    std::vector<int> v_entries;
    for(int i=histo->FindFirstBinAbove(1); i < histo->FindLastBinAbove(1); i++) {
        v_entries.push_back(histo->GetBinContent(i));
    }
    std::sort(v_entries.begin(), v_entries.end());

    double result = -1;
    if(v_entries.size() > 0) {
        result = v_entries[(int)(v_entries.size()/2)];
    }
    return result;

}

// TODO: May be improved. Check for special cases. Right now a lot of histograms are thrown away if
// something is wrong -> no calibration is better than a bad one
std::pair<double, double> TDC_analysis_15Aug::calculateEdges(TH1I* histo) {
    std::pair<double, double> result(-1, -1);

    int nBins = histo->GetNbinsX();
    int min_bin = histo->GetXaxis()->GetXmin();
    int max_bin = histo->GetXaxis()->GetXmax();
    double bin_length = (double)(max_bin - min_bin) / (double)nBins;
    //int nEntries = histo->GetEntries();

    // minimum of entries per histo: on average 3 entries per tdc
    //if(nEntries < 3 * (max_bin - min_bin)) { result.first = -1; result.second = -1; return result;}

    double median = calculateTDCMedian(histo);
    // threshold finding did not work, or histogram is empty
    if(median < 1) { result.first = -1; result.second = -1; return result;}
    // to few entries
    if(median < 3) { result.first = -1; result.second = -1; return result;}

    // get histo properties
    //std::cout << median << std::endl;

    if(median >= 3) {
        double thr = median / 2.;
        int first_bin = histo->FindFirstBinAbove(thr); // still needs to be mltiplied by bin_length
        int last_bin = histo->FindLastBinAbove(thr); // still needs to be mltiplied by bin_length

        //check if there is another bin before/after this one
        //first, check for noise
        int first_noise_bin = histo->FindFirstBinAbove(0);
        int last_noise_bin = histo->FindLastBinAbove(0);

        // if the first noise bin is close to the thr bin, there is probably very few noise and
        // the first noise bin is probably the correct thr_bin
        if(abs(first_noise_bin - first_bin) < 6. / (double)bin_length) {
            result.first = first_noise_bin*bin_length;
        } else { // else, the first noise hit is way before the thr_bin -> ignore it and take the thr_bin
            result.first = first_bin*bin_length;
        }

        if(abs(last_noise_bin - last_bin) < 6. / (double)bin_length) {
            result.second = last_noise_bin*bin_length;
        } else {
            result.second = last_bin*bin_length;
        }

        // reject early peaks
        first_bin = result.first/bin_length;
        int test_range = 100;
        double mean = 0.;
        for(int bin = first_bin; bin < first_bin + test_range; bin++) {
            mean += histo->GetBinContent(bin);
        }
        mean = mean / (double)test_range;


        //std::cout << mean << "\t" << median << "\t" << mean / (double)median << std::endl;

        // FIXME: This is not very robust nor clever...
        if(mean / (double)median < 0.4) {
            bool overjumped_peak = false;
            for(int bin = first_bin + 50; bin < last_bin; bin++) {
                if(histo->GetBinContent(bin) > thr) {
                    first_bin = bin;
                    result.first = bin*bin_length;
                    result.second = last_bin*bin_length;
                    overjumped_peak = true;
                    break;
                }
            }

            if(not overjumped_peak) {
                result.first = -1;
                result.second = -1;
                return result;
            }
        }

        last_bin = result.second/bin_length;
        test_range = 100;
        mean = 0.;
        for(int bin = last_bin; bin > last_bin - test_range; bin--) {
            mean += histo->GetBinContent(bin);
        }
        mean = mean / (double)test_range;
        if(mean / (double)median < 0.4) {
            result.first = -1;
            result.second = -1;
            return result;
        }

    }

    return result;

}

/* determines edges of tdc by using FindFirstBinAbove() and writes them to file */
void TDC_analysis_15Aug::writeCalibration() {
    ofstream out_file;
    out_file.open(fn_tdc_calibration);
    out_file << "ChipID\tMemoryCell\ttdc_min_even\ttdc_max_even\ttdc_min_odd\ttdc_max_odd\n";

    //int nBins = mh_TDCs_CIDs_even[160][1]->GetNbinsX();
    //int min_bin = mh_TDCs_CIDs_even[160][1]->GetXaxis()->GetXmin();
    //int max_bin = mh_TDCs_CIDs_even[160][1]->GetXaxis()->GetXmax();
    //float bin_length = (float)(max_bin - min_bin) / (float)nBins;

    for(auto& p_histos : mh_TDCs_CIDs_even) {
        int chipID = p_histos.first;
        std::vector<TH1I*> v_histos = p_histos.second;
        for(unsigned int memoryCell=0; memoryCell < v_histos.size(); memoryCell++) {
            //memoryCell = p_histo.first;
            out_file << chipID << "\t";
            out_file << memoryCell << "\t";

            TH1I* histo = v_histos[memoryCell];
            //double thr = 1.;
            if(chipID == 142) {
                std::cout << "142: "<< memoryCell << "\t" << histo->GetEntries() << std::endl;
            }
            // double thr = calculateThreshold(histo);

            // Even bunch crossings
            /*int first_bin_above_thr = histo->FindFirstBinAbove(thr)*bin_length;
            int last_bin_above_thr = histo->FindLastBinAbove(thr)*bin_length;
            if(histo->GetEntries() < 5000 || first_bin_above_thr < 0 || last_bin_above_thr < 0) { // to few statistics
                first_bin_above_thr = -1;
                last_bin_above_thr = -1;
            }
            */
            std::pair<double, double> p_edges = calculateEdges(histo);
            out_file << p_edges.first << "\t";
            out_file << p_edges.second << "\t";

            //odd bunchcrossings
            histo = mh_TDCs_CIDs_odd[chipID][memoryCell];
            p_edges = calculateEdges(histo);

            /*first_bin_above_thr = histo->FindFirstBinAbove(thr)*bin_length;
            last_bin_above_thr = histo->FindLastBinAbove(thr)*bin_length;
            if(histo->GetEntries() < 5000 || first_bin_above_thr < 0 || last_bin_above_thr < 0) { // to few statistics
                first_bin_above_thr = -1;
                last_bin_above_thr = -1;
            }
            */
            out_file << p_edges.first << "\t";
            out_file << p_edges.second << "\t";

            out_file << "\n";
        }
    }
    out_file.close();
}


void TDC_analysis_15Aug::readCalibration() {
    ifstream in_file;
    in_file.open(fn_tdc_calibration);

    if(in_file.is_open()) {
        std::string header;
        std::getline(in_file, header);
        std::cout << header << std::endl;

        int chipID, memoryCell, tdc_min_even, tdc_max_even, tdc_min_odd, tdc_max_odd;
        while(in_file >> chipID >> memoryCell >> tdc_min_even >> tdc_max_even >> tdc_min_odd >> tdc_max_odd) {
            m_tdcEdges[chipID][memoryCell].push_back(tdc_min_even);
            m_tdcEdges[chipID][memoryCell].push_back(tdc_max_even);
            m_tdcEdges[chipID][memoryCell].push_back(tdc_min_odd);
            m_tdcEdges[chipID][memoryCell].push_back(tdc_max_odd);

            std::pair<double, double> p_slopes;
            p_slopes.first = -1;
            p_slopes.second = -1;
            if(tdc_min_even > 0 && tdc_max_even > 0) {
                p_slopes.first = 3920. / (double)(tdc_max_even - tdc_min_even); // Bunchcrossing length minus deadtime (4000ns * 0.98)

            }
            if(tdc_min_odd > 0 && tdc_max_odd > 0) {
                p_slopes.second = 3920. / (double)(tdc_max_odd - tdc_min_odd);
            }
            m_tdcSlopes[chipID][memoryCell] = p_slopes;
            h_tdcSlopes->Fill(p_slopes.first);
            h_tdcSlopes->Fill(p_slopes.second);
            std::cout << p_slopes.first << "\t" << p_slopes.second << std::endl;
        }

        // For histograms that show the deviaiton in pedestal and slope between memory cells
        for(auto& edges : m_tdcEdges ) {

            double pedestal_1 = edges.second[1][0];
            double slope_1 = m_tdcSlopes[edges.first][1].first;
            if(pedestal_1 < 0 || slope_1 < 0) continue;

            for(int i=2; i < _nMemoryCells; i++) {
                double pedestal = edges.second[i][0];
                double slope = m_tdcSlopes[edges.first][i].first;
                if(pedestal > 0 && slope > 0) {
                    h_memCell_pedestal->Fill(pedestal-pedestal_1);
                    h_memCell_slope->Fill(slope-slope_1);
                }
            }
        }

        isCalibrationRead = true;
        in_file.close();
    } else {
        std::cout << "Could not open file " << fn_tdc_calibration;
        std::cout << " for reading calibraiton... does it exist?" << std::endl;
    }

}

int TDC_analysis_15Aug::tdcToNs(int chipID, int memoryCell, int BxID, int tdc) {

    double ns = 0;
    if(BxID % 2 == 0) {
        double pedestal = m_tdcEdges[chipID][memoryCell][0];
        double slope = m_tdcSlopes[chipID][memoryCell].first;
        if(pedestal < 0 || slope < 0) {
            ns = -1;
        } else {
            ns = (double)(tdc - pedestal) * slope;
        }
    } else {
        double pedestal = m_tdcEdges[chipID][memoryCell][2];
        double slope = m_tdcSlopes[chipID][memoryCell].second;
        if(pedestal < 0 || slope < 0) {
            ns = -1;
        } else {
            ns = (double)(tdc - pedestal) * slope;
        }
    }

    return ns;

}

void TDC_analysis_15Aug::end() {

    std::cout << "There are " << tmp_count_T0s[0] << " events with 0 T0s." << std::endl;
    std::cout << "There are " << tmp_count_T0s[1] << " events with 1 T0s." << std::endl;
    std::cout << "There are " << tmp_count_T0s[2] << " events with 2 T0s." << std::endl;
    std::cout << "There are " << tmp_count_T0s[3] << " events with 3 T0s." << std::endl;
    std::cout << "There are " << tmp_count_T0s[4] << " events with 4 T0s." << std::endl;

    if(isCalibrationMode) {
        writeCalibration();
    } else {

        for(unsigned int i=0; i < v_ns_T0_0.size(); i++) {
            h2_T0_vs_mT0s_0->Fill(v_ns_T0_0[i], v_ns_T0_12[i]);
            h2_T0_vs_mT0s_1->Fill(v_ns_T0_1[i], v_ns_T0_02[i]);
            h2_T0_vs_mT0s_2->Fill(v_ns_T0_2[i], v_ns_T0_01[i]);
            // h2_T0_vs_mT0s_3->Fill(v_ns_T0_3[i], v_ns_T0_012[i]);

            h2_T00_vs_T01->Fill(v_ns_T0_0[i], v_ns_T0_1[i]);
            h2_T00_vs_T02->Fill(v_ns_T0_0[i], v_ns_T0_2[i]);
            h2_T01_vs_T02->Fill(v_ns_T0_1[i], v_ns_T0_2[i]);

        }


        TFile* o_file = new TFile("out.root", "recreate");
        if (o_file->IsOpen()) {
            h_tdcSlopes->Write();
            h_delta_ns->Write();
            h_memCell_pedestal->Write();
            h_memCell_slope->Write();

            TCanvas* c0 = new TCanvas("c1", "c1");
            h_T0_diff_0->SetLineColor(kGreen);
            h_T0_diff_1->SetLineColor(kRed);
            h_T0_diff_0->Draw();
            h_T0_diff_1->Draw("SAME");
            h_T0_diff_2->Draw("SAME");

            c0->Write();

            h2_T0_vs_mT0s_0->Write();
            h2_T0_vs_mT0s_1->Write();
            h2_T0_vs_mT0s_2->Write();
            // h2_T0_vs_mT0s_3->Write();

            // h2_T00_vs_T01->Draw();
            h2_T00_vs_T01->Write();
            h2_T00_vs_T02->Write();
            h2_T01_vs_T02->Write();

            h2_T00_vs_T01_tdc->Write();
            h2_T00_vs_T02_tdc->Write();
            h2_T01_vs_T02_tdc->Write();
            // h2_T00_vs_T03->Write();

            h2_debug_T0_adcs->Write();

            for(auto& histo : mh_TDCs_CIDs_even[169]) {
                histo->Write();
            }
            for(auto& histo : mh_TDCs_CIDs_even[177]) {
                histo->Write();
            }
            for(auto& histo : mh_TDCs_CIDs_even[185]) {
                histo->Write();
            }
            for(auto& histo : mh_TDCs_CIDs_even[201]) {
                histo->Write();
            }
            for(auto& histo : mh_TDCs_CIDs_even[217]) {
                histo->Write();
            }

            for(auto& p_histo : mh_TDCs_CIDs) {
                p_histo.second->Write();
            }
            for(auto& p_histo : mh_TDCs_CIDs_even) {
                p_histo.second[1]->Write();
                p_histo.second[2]->Write();
            }
            for(auto& p_histo : mh_TDCs_CIDs_odd) {
                p_histo.second[1]->Write();
                p_histo.second[2]->Write();
            }
            for(auto& histo : vh_TDCs_T0) {
                histo->Write();
            }
            for(auto& histo : vh_ADCs_T0) {
                histo->Write();
            }

        } else {
            std::cout << "!!! Could not open root file. Histograms are not saved!" << std::endl;
        }

        o_file->Close();
        delete o_file;
    }

    mh_TDCs_CIDs.clear();
    mh_TDCs_CIDs_even.clear();
    mh_TDCs_CIDs_odd.clear();

    vh_TDCs_T0.clear();
    vh_ADCs_T0.clear();

    delete h_delta_ns;
    delete h_tdcSlopes;



    std::cout << "TDC_analysis_15Aug::end()  " << name()
 	      << " processed " << _nEvt << " events in " << _nRun << " runs "
     	      << std::endl ;

}
