#include "TimingManager.h"


#include <algorithm> // std::sort
#include <fstream>
#include <iostream>
#include <string>

#include <math.h>

#include "Ahc2HardwareConnection.hh"
#include "MappingProcessor.hh"

#include "TTree.h"
#include "TFile.h"


using namespace CALICE;
using namespace lcio;



/*  Adapted from Eldwans Ahc2CalibrateProcessor FillContainer
    Fills a container with a mapping of ModuleNumber and ChipNumber to ChipID*/
void TimingManager::fillChipIDMap(LCCollection* Ahc2HardwareConnectionCol) {

    // LCCollection *_Ahc2HardwareConnectionCol = evt->getCollection( _Ahc2HardwareConnectionName );

    std::cout << "Fill Hardware Connection Container" << std::endl;

    if (!Ahc2HardwareConnectionCol) {
        // streamlog_out(ERROR) << "Cannot fill container, collection is not valid." << std::endl;
        // throw StopProcessingException(this);
    }

    //Write Hardware Connection
    // ofstream out_file;
    // out_file.open("Hardware_Connection.txt");
    // out_file << "ChipID\tModuleNr\tChipNr" << std::endl;

    for (int i = 0; i < Ahc2HardwareConnectionCol->getNumberOfElements(); ++i) {
        Ahc2HardwareConnection *hardwareConnection = new Ahc2HardwareConnection(Ahc2HardwareConnectionCol->getElementAt(i)); // this will make a copy of the data (not link against the data like SimpleValue)

    	int ChipID = hardwareConnection->getChip();
    	int ModuleNumber = hardwareConnection->getModuleNumber();
    	int ChipNumber = hardwareConnection->getChipNumber();

    	_HardwareConnnectionContainer.insert(std::make_pair(std::make_pair(ModuleNumber, ChipNumber), ChipID));

        for(int iChannel=0; iChannel < _nChannels; iChannel++) {
            int I = _ahcMapper->getI(ModuleNumber, ChipNumber, iChannel);
            int J = _ahcMapper->getJ(ModuleNumber, ChipNumber, iChannel);
            int K = _ahcMapper->getK(ModuleNumber);
            _KIJContainer[K].push_back(std::make_pair(I,J));
            // std::cout << K << "\t" << I << "\t" << J << std::endl;
        }

        // out_file << ChipID << "\t" << ModuleNumber << "\t" << ChipNumber << std::endl;
    }

    // out_file.close();

    std::cout << "Container filled" << std::endl;
    std::cout << "Container contains " << _HardwareConnnectionContainer.size() << " elements" << std::endl;
}

void TimingManager::initializeGeometry(std::string mappingProcessorName, LCCollection* Ahc2HardwareConnectionCol) {
    // TODO: exception handling here
    _hasAhcMapper = true;

    try {
        _ahcMapper = dynamic_cast<const Ahc2Mapper*>(MappingProcessor::getMapper(mappingProcessorName));
    } catch(...) {
        _hasAhcMapper = false;
    }

    if(_hasAhcMapper) {
        _HardwareConnnectionContainer.clear();

        this->fillChipIDMap(Ahc2HardwareConnectionCol);
        for(auto& e : _HardwareConnnectionContainer) {
            // a list of all available chipIDs
            _v_chipIDs.push_back(e.second);
        }
        std::sort(_v_chipIDs.begin(), _v_chipIDs.end());

        _nChips = _v_chipIDs.size();
    } else {
        std::cout << "No ahcMapper! Using standard chip Set from 129 to 237!" << std::endl;

        for(int i=129; i < 237; i++) {
            _v_chipIDs.push_back(i);
        }
        _nChips = _v_chipIDs.size();
        // throw StopProcessingException(this);
    }

    _hasInitializedGeometry = true;
}

timing::Hit TimingManager::fillReconstructedHit(CalorimeterHit* p) {
    // std::cout << "Fill reconstructed hit" << std::endl;
    timing::Hit hit;

    int cellID = p->getCellID0();

    int module = _ahcMapper->getModuleFromCellID(cellID);
    int chip = _ahcMapper->getChipFromCellID(cellID);
    int channel = _ahcMapper->getChanFromCellID(cellID); std::map<std::pair<int, int>, int>::iterator found = _HardwareConnnectionContainer.find(std::make_pair(module, chip));
    if(found != _HardwareConnnectionContainer.end()) {
        hit.chipID = found->second;
    } else {
        hit.chipID = _v_chipIDs.front();
        std::cout << "Chip not found!!! Setting ChipID to " << hit.chipID << std::endl;
    }

    hit.memoryCell = p->getType() % 100; // from Ahc2CalibrateProcessor: newHit->setType(GainBit*100 + Mem)
    hit.channel = channel;
    hit.tdc = p->getTime();
    hit.adc = p->getEnergy();
    hit.mip = p->getEnergy();
    hit.hitbit = 1;

    hit.I = _ahcMapper->getI(module, chip, channel);
    hit.J = _ahcMapper->getJ(module, chip, channel);
    hit.K = _ahcMapper->getK(module);

    hit.module = _ahcMapper->getModule(hit.K);
    hit.chipNr = _ahcMapper->getChip(hit.I, hit.J, hit.K);

    return hit;
}

timing::Hit TimingManager::fillRawHit(LCGenericObject* p) {
    // std::cout << "Fill raw hit" << std::endl;
    timing::Hit hit;

    hit.bxID = p->getIntVal(1);
    hit.chipID = p->getIntVal(2);
    hit.memoryCell = p->getIntVal(3);
    hit.channel = p->getIntVal(4);
    hit.tdc = p->getIntVal(5);
    hit.adc = p->getIntVal(6);
    hit.hitbit = p->getIntVal(7);

    return hit;
}

// For Raw Hits
timing::Hit TimingManager::fillHit(LCGenericObject* p) {
    return this->fillRawHit(p);
}

//For reconstructed Hits
timing::Hit TimingManager::fillHit(CalorimeterHit* p) {
    return this->fillReconstructedHit(p);
}

void TimingManager::readTDCCalibration_extended(const char* fn_tdc_calibration) {
    std::ifstream in_file(fn_tdc_calibration);

    if(in_file.is_open()) {
        std::string header;
        std::getline(in_file, header);

        std::cout << header << std::endl;

        int chipID, channel, memoryCell;
        double  tdc_min_even, tdc_max_even, tdc_min_odd, tdc_max_odd, tdc_min_all, tdc_max_all;
        while(in_file >> chipID >> channel >> memoryCell >> tdc_min_even >> tdc_max_even >> tdc_min_odd >> tdc_max_odd >> tdc_min_all >> tdc_max_all) {

            // initilaize pair. First value is for even BxIDs, the second one is for odd BxIDs
            _m_tdcPedestal_extended[chipID][channel][memoryCell][true] = tdc_min_even;
            _m_tdcMaxEdge_extended[chipID][channel][memoryCell][true] = tdc_max_even;
            _m_tdcPedestal_extended[chipID][channel][memoryCell][false] = tdc_min_odd;
            _m_tdcMaxEdge_extended[chipID][channel][memoryCell][false] = tdc_max_odd;
            _m_tdcPedestal_extended_all[chipID][channel][memoryCell] = tdc_min_all;
            _m_tdcMaxEdge_extended_all[chipID][channel][memoryCell] = tdc_max_all;

            // calculates the slopes
            if(tdc_min_even > 0 && tdc_max_even > 0) {
                // Bunchcrossing length minus deadtime (4000ns * 0.98);
                _m_tdcSlope_extended[chipID][channel][memoryCell][true] = _bxPeriod * (1. - _rampDeadtime) / (double)(tdc_max_even - tdc_min_even);
            } else {
                _m_tdcSlope_extended[chipID][channel][memoryCell][true] = -1.;
            }

            if(tdc_min_odd > 0 && tdc_max_odd > 0) {
                _m_tdcSlope_extended[chipID][channel][memoryCell][false] = _bxPeriod * (1. - _rampDeadtime) / (double)(tdc_max_odd - tdc_min_odd);
            } else {
                _m_tdcSlope_extended[chipID][channel][memoryCell][false] = -1.;
            }

            if(tdc_min_all > 0 && tdc_max_all > 0) {
                _m_tdcSlope_extended_all[chipID][channel][memoryCell] = _bxPeriod * (1. - _rampDeadtime) / (double)(tdc_max_all - tdc_min_all);
            } else {
                _m_tdcSlope_extended_all[chipID][channel][memoryCell] = -1.;
            }
        }

        _hasReadTDCCalibration = true;
    } else {
        std::cout << "Could not open file " << fn_tdc_calibration;
        std::cout << " for reading calibraiton... does it exist?" << std::endl;
    }

}

/*
    Read nonLinearity Corrections from file.
    Format:
    T0 linearization is enabled	<1/0>
    <Header>
    <Header - T0 Nonlinearity: chipID, channel, nl_offset_even, nl_slope_even, nl_offset_odd, nl_slope_odd>
    ...
    <Header nonLinearity: a + b*t + c*t^2>
    <Header - Chip nonLinearity: chipID, a_even, b_even, c_even, a_odd, b_odd, c_odd>
    ...
*/
void TimingManager::readNonLinearityCorrection(const char* fn_nonLinearity) {
    std::ifstream in_file(fn_nonLinearity);

    if(in_file.is_open()) {
        std::string dump;

        in_file >> dump >> dump >> dump >> dump >> _isT0Linearization;
        std::cout << "is T0 linearization enabled?\t" << _isT0Linearization << std::endl;

        std::getline(in_file, dump); // Header line
        std::getline(in_file, dump); // Header line
        std::getline(in_file, dump); // Header line
        std::cout << dump << std::endl;

        if(_v_T0.size() == 0) std::cout << "TimingManager: T0s not set! Use TimingManager.setT0()" << std::endl;

        int chipID, channel;
        double nl_offset_even, nl_slope_even, nl_offset_odd, nl_slope_odd;

        for(unsigned int i=0; i<_v_T0.size(); i++) {
            in_file >> chipID >> channel >> nl_offset_even >> nl_slope_even >> nl_offset_odd >> nl_slope_odd;

            // chipID, channel, BxID%2==0: holds nonlinearity of the T0s
            if(_isT0Linearization) {
                _m_T0_nonLinearity[chipID][true] = std::make_pair(nl_offset_even, nl_slope_even);
                _m_T0_nonLinearity[chipID][false] = std::make_pair(nl_offset_odd, nl_slope_odd);
                std::cout << "Reading nonLinearity of T0: " << chipID << ", " << channel << std::endl;
            }
        }

        std::getline(in_file, dump); // Header line
        std::cout << dump << std::endl;
        std::getline(in_file, dump); // Header line
        std::cout << dump << std::endl;
        std::getline(in_file, dump); // Header line
        std::cout << dump << std::endl;

        // a + b*t + c*t^2
        double a_even, b_even, c_even, a_odd, b_odd, c_odd;
        while(in_file >> chipID >> a_even >> b_even >> c_even >> a_odd >> b_odd >> c_odd) {
            _m_nonLinearity[chipID][true].push_back(a_even);
            _m_nonLinearity[chipID][true].push_back(b_even);
            _m_nonLinearity[chipID][true].push_back(c_even);

            _m_nonLinearity[chipID][false].push_back(a_odd);
            _m_nonLinearity[chipID][false].push_back(b_odd);
            _m_nonLinearity[chipID][false].push_back(c_odd);
        }

        _hasReadNonLinearityCorrection = true;
    } else {
        std::cout << "Could not open file " << fn_nonLinearity;
        std::cout << " for reading calibration... does it exist?" << std::endl;
    }
}


void TimingManager::readTimingCalibration_extended(const char* fn_timing_calibration) {
    std::ifstream in_file(fn_timing_calibration);

    if(in_file.is_open()) {
        std::string header;
        in_file >> header >> _setOfCalibrationConstants; //read the set of calibration constants to use
        std::cout << header << "\t" << _setOfCalibrationConstants << std::endl;

        std::getline(in_file, header); // header of the offset values
        std::getline(in_file, header); // header of the offset values

        std::cout << header << std::endl;

        int chipID, channel, memCell;
        double offset_even, offset_odd, offset_all;

        while(in_file >> chipID >> channel >> memCell >> offset_even >> offset_odd >> offset_all) {
            _m_timing_offset_extended[chipID][channel][memCell][true] = offset_even;
            _m_timing_offset_extended[chipID][channel][memCell][false] = offset_odd;
            _m_timing_offset_extended_all[chipID][channel][memCell] = offset_all;
        }

        _hasReadTimingCalibration = true;
    } else {
        std::cout << "Could not open file " << fn_timing_calibration;
        std::cout << " for reading calibration... does it exist?" << std::endl;
    }

}



void TimingManager::readRootFile(const char * fn_root_hits, std::vector<timing::Event>& v_events) {
    // std::vector<timing::Event> v_events;

    timing::Event event;

    timing::Hit hit;

    TFile* f = new TFile(fn_root_hits, "read");
    TTree* T = (TTree*)f->Get("T");

    T->SetBranchAddress("eventID", &hit.eventID);
    T->SetBranchAddress("bxID", &hit.bxID);
    T->SetBranchAddress("channel", &hit.channel);
    T->SetBranchAddress("memoryCell", &hit.memoryCell);
    T->SetBranchAddress("chipID", &hit.chipID);
    T->SetBranchAddress("tdc", &hit.tdc);
    T->SetBranchAddress("adc", &hit.adc);
    T->SetBranchAddress("mip", &hit.mip);
    T->SetBranchAddress("I", &hit.I);
    T->SetBranchAddress("J", &hit.J);
    T->SetBranchAddress("K", &hit.K);
    T->SetBranchAddress("nTracks", &event.nTracks);

    int lastEventID = 0;
    for(int i=0; i < T->GetEntries(); i++) {
        hit = timing::Hit();

        T->GetEntry(i);

        if(hit.eventID != lastEventID) {
            v_events.push_back(event);
            event = timing::Event();
            lastEventID = hit.eventID;
        }

        event.v_hits.push_back(hit);
    }

    std::cout << "Reading from Root file: " << fn_root_hits << std::endl;
    std::cout << "I read in total " << T->GetEntries() << " hits, and "  << v_events.size() << " events." << std::endl;
}


double TimingManager::getTdcPedestal(int chipID, int channel, int memoryCell, int BxID, int setOfCalibrationConstants) {
    if(setOfCalibrationConstants == 0) {
        return _m_tdcPedestal_extended_all[chipID][channel][1];
    } else if(setOfCalibrationConstants == 1) {
        return _m_tdcPedestal_extended[chipID][channel][1][BxID%2==0];
    } else if(setOfCalibrationConstants == 9) {
        return _m_tdcPedestal_extended[chipID][channel][memoryCell][BxID%2==0];
    }

    return 0;
}

double TimingManager::getTdcMaxEdge(int chipID, int channel, int memoryCell, int BxID, int setOfCalibrationConstants) {
    if(setOfCalibrationConstants == 0) {
        return _m_tdcMaxEdge_extended_all[chipID][channel][1];
    } else if(setOfCalibrationConstants == 1) {
        return _m_tdcMaxEdge_extended[chipID][channel][1][BxID%2==0];
    } else if(setOfCalibrationConstants == 9) {
        return _m_tdcMaxEdge_extended[chipID][channel][memoryCell][BxID%2==0];
    }

    return 0;
}

double TimingManager::getTdcSlope(int chipID, int channel, int memoryCell, int BxID, int setOfCalibrationConstants) {
    if(setOfCalibrationConstants == 0 || setOfCalibrationConstants == 1) {
        // return 1.6;
        return _m_tdcSlope_extended[chipID][_nChannels][1][BxID%2==0];
    } else if(setOfCalibrationConstants == 9) {
        return _m_tdcSlope_extended[chipID][channel][memoryCell][BxID%2==0];
    }

    return 0;
}

/*
    convert TDC to ns using the pedestal and the slope from the TDC calibration
    No offset correction applied and no nonLinearity correction.
 */
// double TimingManager::tdcToNs(int chipID, int memoryCell, int BxID, int tdc) {
//
//     if(not _hasReadTDCCalibration) {
//         std::cout << "TDC Calibration is not read in. Please run ";
//         std::cout << "readTDCCalibration(const char* fn_tdc_calibration) first.";
//         std::cout << "returning 0.";
//         return 0;
//     }
//
//     double ns = 0.;
//
//     double pedestal = getTdcPedestal(chipID, channel, memoryCell, BxID, _setOfCalibrationConstants);
//     double slope = getTdcSlope(chipID, channel, memoryCell, BxID, _setOfCalibrationConstants);
//
//     if(pedestal < 0 || slope < 0) {
//         ns = -1;
//     } else {
//             ns = (double)(tdc - pedestal) * slope;
//     }
//
//     return ns;
// }

/*
    convert TDC to ns using the pedestal and the slope from the TDC calibration
    No offset correction applied and no nonLinearity correction.
 */
double TimingManager::tdcToNs(int chipID, int channel, int memoryCell, int BxID, int tdc) {

    if(not _hasReadTDCCalibration) {
        std::cout << "TDC Calibration is not read in. Please run ";
        std::cout << "readTDCCalibration(const char* fn_tdc_calibration) first.";
        std::cout << "returning 0.";
        return 0;
    }

    double ns = 0.;

    double pedestal = getTdcPedestal(chipID, channel, memoryCell, BxID, _setOfCalibrationConstants);
    double slope = getTdcSlope(chipID, channel, memoryCell, BxID, _setOfCalibrationConstants);

    if(pedestal < 0 || slope < 0) {
        ns = -1;
    } else {
            ns = (double)(tdc - pedestal) * slope;
    }

    return ns;
}

double TimingManager::tdcToNs_T0(int chipID, int channel, int memoryCell, int BxID, int tdc) {

    if(not _hasReadTDCCalibration) {
        std::cout << "TDC Calibration is not read in. Please run ";
        std::cout << "readTDCCalibration(const char* fn_tdc_calibration) first.";
        std::cout << "returning 0.";
        return 0;
    }

    double ns = 0.;

    double pedestal = getTdcPedestal(chipID, channel, memoryCell, BxID, 9);
    double slope = getTdcSlope(chipID, channel, memoryCell, BxID, 9);

    if(pedestal < 0 || slope < 0) {
        ns = -1;
    } else {
            ns = (double)(tdc - pedestal) * slope;
    }

    return ns;
}
/*
    converts from tdc to ns taking the nonLinearity of the histograms into account.
    Usually this is only done for the T0s. This method uses the external parameters
    nl_offset and nl_slope
*/
double TimingManager::tdcToNs_nonLin(int chipID, int channel, int memoryCell, int BxID, int tdc, double nl_offset, double nl_slope) {

    double ns = 0.;

    double pedestal = getTdcPedestal(chipID, channel,  memoryCell, BxID, _setOfCalibrationConstants);
    double maxEdge = getTdcMaxEdge(chipID, channel, memoryCell, BxID, _setOfCalibrationConstants);
    double slope = getTdcSlope(chipID, channel, memoryCell, BxID, _setOfCalibrationConstants);

    double a = (nl_offset + nl_slope * pedestal) / (double)(nl_offset + nl_slope * maxEdge);

    if(pedestal < 0 || slope < 0) {
        ns = -1;
    } else {
        ns = _bxPeriod*(1.-_rampDeadtime) / (double)(a-1.);
        ns = ns * (sqrt(1. + (a*a-1.)*(double)(tdc-pedestal)/(double)(maxEdge-pedestal)) - 1.);
    }

    return ns;
}

/*
    converts from tdc to ns taking the nonLinearity of the histograms into account.
    Usually this is only done for the T0s. Using the calibration data from file.
*/
double TimingManager::tdcToNs_nonLin(int chipID, int channel, int memoryCell, int BxID, int tdc, double T0_time) {

    if(not _hasReadNonLinearityCorrection) {
        std::cout << "nonLinearity Correction was not read. Using stdandard tdcToNs conversion" << std::endl;
        return tdcToNs(chipID, memoryCell, BxID, tdc);
    }

    double ns = 0.;

    double pedestal = getTdcPedestal(chipID, channel, memoryCell, BxID, _setOfCalibrationConstants);
    double maxEdge = getTdcMaxEdge(chipID, channel, memoryCell, BxID, _setOfCalibrationConstants);
    double slope = getTdcSlope(chipID, channel, memoryCell, BxID, _setOfCalibrationConstants);

    bool isEven = (BxID%2==0);

    //This is the linearization for the T0s
    if(_m_T0_nonLinearity.find(chipID) != _m_T0_nonLinearity.end()) {
        if(_isT0Linearization) {
            double nl_offset = _m_T0_nonLinearity[chipID][isEven].first;
            double nl_slope = _m_T0_nonLinearity[chipID][isEven].second;

            double a = (nl_offset + nl_slope * pedestal) / (double)(nl_offset + nl_slope * maxEdge);

            if(pedestal <= 0. || slope <= 0.) {
                return -1;
            } else {
                ns = _bxPeriod*(1.-_rampDeadtime) / (double)(a-1.);
                return ns * (sqrt(1. + (a*a-1.)*(double)(tdc-pedestal)/(double)(maxEdge-pedestal)) - 1.);
            }
        } else {
            return tdcToNs(chipID, channel, memoryCell, BxID, tdc);
        }
    } else {
        //This corrects for the quadratic fit for each chip compared to the T0
        double a = _m_nonLinearity[chipID][isEven][0];
        double b = _m_nonLinearity[chipID][isEven][1];
        double c = _m_nonLinearity[chipID][isEven][2];

        // if the fit was not performed, don't correct for it.
        if(abs(a) <= 1e-10) {
            // return tdcToNs(chipID, memoryCell, BxID, tdc);
            return -1;
        } else {
            ns = tdcToNs(chipID, channel, memoryCell, BxID, tdc);

            //in this case, the calibration had not worked
            if(ns < 0) return -1;

            //usually the T0 time should be used here as a parameter. If it isn't
            //given, use the ns. This should introduce only a small error.
            if(T0_time != 0) {
                ns -= a + b*T0_time + c*T0_time*T0_time;
            } else {
                ns -= a + b*ns + c*ns*ns;
            }
            return ns;
        }

    }

    return ns;
}

// double TimingManager::getOffset(int chipID, int channel) {
    // return _m_timing_offset[chipID][channel];
// }

double TimingManager::getOffset(int chipID, int channel, int memCell, int bxID) {
    return _m_timing_offset_extended[chipID][channel][memCell][bxID%2==0];
}
