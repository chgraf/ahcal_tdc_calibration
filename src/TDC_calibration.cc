#include "TDC_calibration.h"
#include <algorithm> //std::sort
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>
#include <math.h>

#include <EVENT/LCCollection.h>
#include <EVENT/LCGenericObject.h>
#include <EVENT/LCObject.h>
#include <EVENT/CalorimeterHit.h>

#include "MappingProcessor.hh"
#include "Ahc2HardwareConnection.hh"

// ----- include for verbosity dependend logging ---------
#include "marlin/VerbosityLevels.h"
#include "marlin/Exceptions.h"

#include "TCanvas.h"
#include "TFile.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TNtuple.h"
#include "TVectorD.h"

using namespace lcio;
using namespace marlin;
using namespace CALICE;


TDC_calibration aTDC_calibration;


TDC_calibration::TDC_calibration() : Processor("TDC_calibration") {

    // modify processor description
    _description = "TDC_calibration calibrates the tdc and the offset ..." ;

    // register steering parameters: name, description, class-variable, default value
    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCALRaw",
            "Name of the HCAL input collection for raw data",
            _colName_HCAL_Raw ,
            std::string("EUDAQDataHCAL")
    );

    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCALReco",
            "Name of the HCAL input collection for reco data",
            _colName_HCAL_Reco,
            std::string("Ahc2Calorimeter_Hits")
    );

    // registerInputCollection( LCIO::LCGENERICOBJECT,
            // "CollectionNameECAL" ,
            // "Name of the ECAL input collection"  ,
            // _colName_ECAL ,
            // std::string("EUDAQDataECAL")
    // );

    registerProcessorParameter(
        "TestbeamCampaign",
        "TestbeamCampaign: Aug15, Jul15",
        _TBCampaign,
        std::string("Aug15")
    );

    registerProcessorParameter(
        "CalibrationOutputFilename",
        "name of the calibration file",
        _fn_tdc_calibration,
        std::string("tdc_calibration.txt")
    );

    registerProcessorParameter(
        "useRootFile",
        "name of the root file that should be used for calibration. If slcio files should be used type: \"false\" ",
        _fn_RootInput,
        std::string("false")
    );

    registerProcessorParameter(
        "onlyT0Calibration",
        "If set to <true>, then only the calibration constants of the T0s are saved.",
        _fOnlyCalibrateT0s,
        bool(false)
    );

    registerProcessorParameter(
        "RootOutputFilename",
        "root calibratoin output file",
        _fn_root_spectra,
        std::string("tmp_calibration.root")
    );

    registerProcessorParameter(
        "MappingProcessorName" ,
        "Name of the MappingProcessor instance that provides the geometry of the detector." ,
        _mappingProcessorName,
        std::string("MyMappingProcessor")
    );

    registerProcessorParameter(
        "HardwareConnectionCollection" ,
        "Name of the Ahc2HardwareConnection Collection",
        _Ahc2HardwareConnectionName,
		std::string("Ahc2HardwareConnection")
    );

}

void TDC_calibration::init() {
    std::cout << "Init..." << std::endl;
    // ---------- config ----------

    _timingManager = new TimingManager();

    // ---------- config ----------
    _timingManager->setNChannels(36);
    _timingManager->setNMemoryCells(16);
    _timingManager->setBxPeriod(4000);
    _timingManager->setRampDeadtime(0.02);

    // this is of course bullshit right now, but maybe i come up with an idea oon how to
    // not hardcode the number of memory cells per chip, and then this could be done
    // by the timing manager
    _nMemoryCells = _timingManager->getNMemoryCells();
    _bxPeriod = _timingManager->getBxPeriod();
    _rampDeadtime = _timingManager->getRampDeadtime();
    _nChannels = _timingManager->getNChannels();

    // _fn_root_spectra = "calibration.root";
    _fn_root_analysis = "analysis_calibration.root";

    if(_fn_RootInput == std::string("false")) {
        _useRootFile = false;
    } else {
        _useRootFile = true;
        std::cout << "Reading from root file: " << _fn_RootInput << std::endl;
    }

    // T0s
    //v_T0.push_back(std::pair<int, int>(169,29));
    // v_T0.push_back(std::pair<int, int>(177,23));
    // v_T0.push_back(std::pair<int, int>(185,29));
    // v_T0.push_back(std::pair<int, int>(201,29)); // noisy
    // v_T0.push_back(std::pair<int, int>(211,6)); // broken
    // v_T0.push_back(std::pair<int, int>(217,23));
    if(_TBCampaign == "Aug15") {
        // v_T0.push_back(std::pair<int, int>(185,29));
        v_T0.push_back(std::pair<int, int>(169,29));
        v_T0.push_back(std::pair<int, int>(177,23));
        v_T0.push_back(std::pair<int, int>(185,29));
        v_T0.push_back(std::pair<int, int>(201,29)); // noisy
        v_T0.push_back(std::pair<int, int>(211,6)); // broken
        v_T0.push_back(std::pair<int, int>(217,23));
        // v_T0.push_back(std::pair<int, int>(201,29));
        // v_T0.push_back(std::pair<int, int>(217,23));
    } else if(_TBCampaign == "Jul15") {
        v_T0.push_back(std::pair<int, int>(169,29));
        v_T0.push_back(std::pair<int, int>(177,23));
        v_T0.push_back(std::pair<int, int>(185,29));
        v_T0.push_back(std::pair<int, int>(217,23));
    }

    _timingManager->setT0s(v_T0);

    // usually a good idea to
    printParameters() ;

    std::cout << "Calibration mode!" << std::endl;

    _nRun = 0 ;
    _nEvt = 0 ;
}

void TDC_calibration::processRunHeader( LCRunHeader* run) {
    _hasInitializedRun = false;
    _nRun++ ;
}

void TDC_calibration::init_histograms() {
    // ---------- Init Histos ----------
    h_channels = new TH1D("h_channels", "Channel distribution", 36, 0, 36);
    h_channels->GetXaxis()->SetTitle("Channel Number");
    h_channels->GetYaxis()->SetTitle("#");


    h_tdcSlopes = new TH1D("h_tdcSlopes", "TDC Slopes", 300, 0, 3);
    h_tdcSlopes->GetXaxis()->SetTitle("ramp slope [ns/bin]");
    h_tdcSlopes->GetYaxis()->SetTitle("#");

    h_pedestal = new TH1D("h_pedestal", "TDC pedestals", 2000, 0, 2000);
    h_pedestal->GetXaxis()->SetTitle("pedestal [tdc]");
    h_pedestal->GetYaxis()->SetTitle("#");

    h_maxEdge = new TH1D("h_maxEdge", "TDC Second Edge of TDC Spectrum", 2000, 2000, 4000);
    h_maxEdge->GetXaxis()->SetTitle("maxEdge [tdc]");
    h_maxEdge->GetYaxis()->SetTitle("#");

    h_memCell_pedestal = new TH1D("h_memCell_pedestal", "Differences in pedestal of all the memCells in one chip to the median", 200, -100, 100);
    h_memCell_pedestal->GetXaxis()->SetTitle("ramp pedestal [tdc]");
    h_memCell_pedestal->GetYaxis()->SetTitle("#");
    h_memCell_slope = new TH1D("h_memCell_slope", "Differences in slope of all the memCells in one chip to the median", 2000, -0.1, 0.1);
    h_memCell_slope->GetXaxis()->SetTitle("ramp slope [ns/bin]");
    h_memCell_slope->GetYaxis()->SetTitle("#");
    h_memCell_maxEdge = new TH1D("h_memCell_maxEdge", "Differences in maxEdge of all the memCells in one chip to the median", 200, -100, 100);
    h_memCell_maxEdge->GetXaxis()->SetTitle("ramp maxEdge [tdc]");
    h_memCell_maxEdge->GetYaxis()->SetTitle("#");

    h_channel_pedestal = new TH1D("h_channel_pedestal", "Differences in pedestal of all the channels in one chip to the mean of the channels", 200, -100, 100);
    h_channel_pedestal->GetXaxis()->SetTitle("ramp pedestal [tdc]");
    h_channel_pedestal->GetYaxis()->SetTitle("#");
    h_channel_slope = new TH1D("h_channel_slope", "Differences in slope of all the channels in one chip to the mean of the channels", 2000, -0.1, 0.1);
    h_channel_slope->GetXaxis()->SetTitle("ramp slope [ns/bin]");
    h_channel_slope->GetYaxis()->SetTitle("#");
    h_channel_maxEdge = new TH1D("h_channel_maxEdge", "Differences in maxEdge of all the channels in one chip to the mean of the channels", 200, -100, 100);
    h_channel_maxEdge->GetXaxis()->SetTitle("ramp maxEdge [tdc]");
    h_channel_maxEdge->GetYaxis()->SetTitle("#");

    // initialize histograms for TDC calibration
    if(not _useRootFile) {

        for(auto& chipID : _v_chipIDs) {
            std::vector< std::vector< TH1I* > > vvh_even;
            std::vector< std::vector< TH1I* > > vvh_odd;
            std::vector< std::vector< TH1I* > > vvh_all;
            // The extra channel is the sum of all channels
            for(int iChannel=0; iChannel < _nChannels+1; iChannel++) { //run through all memory cells, except the first one (its broken)

                if(_fOnlyCalibrateT0s && _timingManager->isT0(std::make_pair(chipID, iChannel))) continue;

                std::vector<TH1I*> vh_even;
                std::vector<TH1I*> vh_odd;
                std::vector<TH1I*> vh_all;
                for(int memoryCell=0; memoryCell < _nMemoryCells; memoryCell++) { //run through all memory cells, except the first one (its broken)
                    // BXID even
                    std::stringstream hist_name;
                    hist_name.str(std::string());
                    hist_name << "h_TDCs_CID_" << chipID << "_" << iChannel << "_" << memoryCell << "_even";

                    std::stringstream hist_title;
                    hist_title.str(std::string());
                    hist_title << "TDCs for hitbit == 1, Chip " << chipID << ", Channel, " << iChannel << ", MemCell " << memoryCell << ", BXID even";

                    TH1I* h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 4000, 1, 4001);
                    h_tmp->GetXaxis()->SetTitle("tdc");
                    h_tmp->GetYaxis()->SetTitle("#");
                    vh_even.push_back(h_tmp);

                    // BXID odd
                    hist_name.str(std::string());
                    hist_name << "h_TDCs_CID_" << chipID << "_" << iChannel << "_" << memoryCell << "_odd";

                    hist_title.str(std::string());
                    hist_title << "TDCs for hitbit == 1, Chip " << chipID << ", Channel, " << iChannel << ", MemCell " << memoryCell << ", BXID odd";

                    TH1I* h_tmp_2 = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 4000, 1, 4001);
                    h_tmp_2->GetXaxis()->SetTitle("tdc");
                    h_tmp_2->GetYaxis()->SetTitle("#");
                    vh_odd.push_back(h_tmp_2);

                    // both BXID
                    hist_name.str(std::string());
                    hist_name << "h_TDCs_CID_" << chipID << "_" << iChannel << "_" << memoryCell << "_all";

                    hist_title.str(std::string());
                    hist_title << "TDCs for hitbit == 1, Chip " << chipID << ", Channel, " << iChannel << ", MemCell " << memoryCell << ", BXID all";

                    TH1I* h_tmp_3 = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 4000, 1, 4001);
                    h_tmp_3->GetXaxis()->SetTitle("tdc");
                    h_tmp_3->GetYaxis()->SetTitle("#");
                    vh_all.push_back(h_tmp_3);
                }
                vvh_even.push_back(vh_even);
                vvh_odd.push_back(vh_odd);
                vvh_all.push_back(vh_all);

                // if(iChannel == 0) {
                    // mh_TDCs_ChiMem_even.insert(std::make_pair(chipID, vh_even));
                    // mh_TDCs_ChiMem_odd.insert(std::make_pair(chipID, vh_odd));
                // }
            }
            mh_TDCs_ChiChaMem_even.insert(std::make_pair(chipID, vvh_even));
            mh_TDCs_ChiChaMem_odd.insert(std::make_pair(chipID, vvh_odd));
            mh_TDCs_ChiChaMem_all.insert(std::make_pair(chipID, vvh_all));

            // mh_TDCs_Chips_even.insert(std::make_pair(chipID, vvh_even[0][0]));
            // mh_TDCs_Chips_odd.insert(std::make_pair(chipID, vvh_odd[0][0]));
        }
    }

    std::cout << "Initiated Histograms" << std::endl;
    // std::cout << "ChiChaMem: " << mh_TDCs_ChiChaMem_even.size() << " ";
    // std::cout << mh_TDCs_ChiChaMem_even[160].size() << " ";
    // std::cout << mh_TDCs_ChiChaMem_even[160][0].size() << std::endl;
    // std::cout << "ChiMem: " << mh_TDCs_ChiMem_even.size() << " ";
    // std::cout << mh_TDCs_ChiMem_even[160].size() << std::endl;
    // std::cout << "Chips: " << mh_TDCs_Chips_even.size() << std::endl;
}

void TDC_calibration::processEvent( LCEvent * evt ) {

    // Fills the hardwareConnection Container
    // and the histograms // This has only be done // Cannot be done in init() because we need one Event for the hardwareConnection
    // Or is there another way?
    if(not _hasInitializedRun) {
        const std::vector<std::string>* colNames =  evt->getCollectionNames();
        if(std::find(colNames->begin(), colNames->end(), _colName_HCAL_Reco) != colNames->end()) {
            std::cout << "Using reconstructed data from collection: " << _colName_HCAL_Reco << std::endl;
            std::cout << "Collection names stored in the event: " << std::endl;
            for(auto& n : *colNames) {
                std::cout << n << ", ";
            }
            std::cout << std::endl;
            _isReconstructedData = true;
        } else if(std::find(colNames->begin(), colNames->end(), _colName_HCAL_Raw) != colNames->end()) {
            std::cout << "Using raw data from collection: " << _colName_HCAL_Raw << std::endl;
            std::cout << "Collection names stored in the event: " << std::endl;
            for(auto& n : *colNames) {
                std::cout << n << ", ";
            }
            std::cout << std::endl;
            _isReconstructedData = false;
        } else {
            std::cout << "Could not find any of the specified collecitons..." << std::endl;
            std::cout << "Input raw data collection: " << _colName_HCAL_Raw << std::endl;
            std::cout << "Input reconstructed data collection: " << _colName_HCAL_Reco << std::endl;
            std::cout << "Collection names stored in the event: " << std::endl;
            for(auto& n : *colNames) {
                std::cout << n << ", ";
            }
            std::cout << std::endl;
        }
        _hasInitializedRun = true;
    }

    if(not _isInitialized) {

        _timingManager->initializeGeometry(_mappingProcessorName, evt->getCollection(_Ahc2HardwareConnectionName) );
        _v_chipIDs = _timingManager->getChipIDs();
        _nChips = _v_chipIDs.size();

        init_histograms();
        _isInitialized = true;
    }

    // if we use slcio data for the calibration: read it in
    // and store it in hit/event data structure
    if(not _useRootFile) {

        // Get the collection
        LCCollection* col;
        if(_isReconstructedData) {
             col = evt->getCollection(_colName_HCAL_Reco);
        } else {
             col = evt->getCollection(_colName_HCAL_Raw);
        }

        // this will only be entered if the collection is available
        if( col != NULL ){

            int nHits = col->getNumberOfElements();

            for(int i=0; i< nHits ; i++){

                // load values into hit struct
                timing::Hit  hit;

                // fill the hit. We have to distinguish between reconstructed and raw data
                if(_isReconstructedData) {
                    // get the current hit
                    CalorimeterHit* p = dynamic_cast<CalorimeterHit*>( col->getElementAt( i ));
                    hit = _timingManager->fillHit(p);
                } else {
                    LCGenericObject* p = dynamic_cast<LCGenericObject*>( col->getElementAt( i ));
                    hit = _timingManager->fillHit(p);
                }

                // BxID has to be set explicitly using the event
                hit.bxID = evt->getParameters().getIntVal("BXID");
                hit.isT0 = _timingManager->isT0(std::make_pair(hit.chipID, hit.channel));
                // if(_nEvt < 10000) {
                    // std::cout << "Module: " <<hit.module << "\tChipNr.: " << hit.chipNr << std::endl;
                // }

                if(_fOnlyCalibrateT0s && not hit.isT0) continue;

                // throw away memoryCell 0, its broken
                if(hit.memoryCell == 0) continue;
                // just use hitbit == 1
                if(hit.hitbit == 0) continue;


                // should not happen
                if(hit.bxID > 4096) {
                    std::cout << "Watch out! Corrupted readout cycle!";
                    std::cout << " ... just use data that is converted from the raw .txt files";
                    std::cout << " using the newest version of LabviewConverter2.cc";
                }

                // Fill the tdc spectra
                if(std::find(_v_chipIDs.begin(), _v_chipIDs.end(), hit.chipID) != _v_chipIDs.end()) {
                    // std::cout << hit.chipID << "\t" << hit.memoryCell << std::endl;
                    // std::cout << mh_TDCs_CIDs_even.size() << "\t" << mh_TDCs_CIDs_even[hit.chipID].size() << std::endl;
                    // std::cout << mh_TDCs_CIDs_odd.size() << "\t" << mh_TDCs_CIDs_odd[hit.chipID].size() << std::endl;

                    h_channels->Fill(hit.channel);
                    if(hit.chipID == 142 && hit.channel == 0 && hit.memoryCell == 0) {
                        _tmp_counter++;
                    }

                    if(hit.bxID % 2 == 0) {
                        mh_TDCs_ChiChaMem_even[hit.chipID][hit.channel][hit.memoryCell]->Fill(hit.tdc);
                        mh_TDCs_ChiChaMem_even[hit.chipID][hit.channel][0]->Fill(hit.tdc); // including all hits of the channel for all memorycells
                        mh_TDCs_ChiChaMem_even[hit.chipID][_nChannels][hit.memoryCell]->Fill(hit.tdc);
                        mh_TDCs_ChiChaMem_even[hit.chipID][_nChannels][0]->Fill(hit.tdc);

                        // mh_TDCs_ChiMem_even[hit.chipID][hit.memoryCell]->Fill(hit.tdc);
                        // mh_TDCs_Chips_even[hit.chipID]->Fill(hit.tdc);
                    } else {
                        mh_TDCs_ChiChaMem_odd[hit.chipID][hit.channel][hit.memoryCell]->Fill(hit.tdc);
                        mh_TDCs_ChiChaMem_odd[hit.chipID][hit.channel][0]->Fill(hit.tdc); // including all hits of the channel for all memorycells
                        mh_TDCs_ChiChaMem_odd[hit.chipID][_nChannels][hit.memoryCell]->Fill(hit.tdc);
                        mh_TDCs_ChiChaMem_odd[hit.chipID][_nChannels][0]->Fill(hit.tdc);

                        // mh_TDCs_ChiMem_odd[hit.chipID][hit.memoryCell]->Fill(hit.tdc);
                        // mh_TDCs_Chips_odd[hit.chipID]->Fill(hit.tdc);
                    }
                    mh_TDCs_ChiChaMem_all[hit.chipID][hit.channel][hit.memoryCell]->Fill(hit.tdc);
                    mh_TDCs_ChiChaMem_all[hit.chipID][hit.channel][0]->Fill(hit.tdc); // including all hits of the channel for all memorycells;
                    mh_TDCs_ChiChaMem_all[hit.chipID][_nChannels][hit.memoryCell]->Fill(hit.tdc);
                    mh_TDCs_ChiChaMem_all[hit.chipID][_nChannels][0]->Fill(hit.tdc);
                } else {
                    std::cout << "There is a problem with the chipIDs here, ";
                    std::cout << "could not fill TDC spectra" << std::endl;
                }
            }
        } // end if col != 0
    }
    _nEvt ++ ;
}

void TDC_calibration::check( LCEvent * evt ) {
    // nothing to check here - could be used to fill checkplots in reconstruction processor
}

/* Calculates the threshold for edge detection by the median of the entries */
double TDC_calibration::calculateTDCMedian(TH1I* histo) {
    std::vector<int> v_entries;
    for(int i=histo->FindFirstBinAbove(1); i < histo->FindLastBinAbove(1); i++) {
        v_entries.push_back(histo->GetBinContent(i));
    }
    std::sort(v_entries.begin(), v_entries.end());

    double result = -1;
    if(v_entries.size() > 0) {
        result = v_entries[(int)(v_entries.size()/2)];
    }
    return result;
}

// TODO: May be improved. Check for special cases. Right now a lot of histograms are thrown away if
// something is wrong -> no calibration is better than a bad one
std::pair<double, double> TDC_calibration::calculateEdges(TH1I* histo) {
    std::pair<double, double> result(-1, -1);

    int nBins = histo->GetNbinsX();
    int min_bin = histo->GetXaxis()->GetXmin();
    int max_bin = histo->GetXaxis()->GetXmax();
    double bin_length = (double)(max_bin - min_bin) / (double)nBins;
    //int nEntries = histo->GetEntries();

    // minimum of entries per histo: on average 3 entries per tdc
    //if(nEntries < 3 * (max_bin - min_bin)) { result.first = -1; result.second = -1; return result;}

    double median = calculateTDCMedian(histo);

    int median_thr = 4; // XXX: Hard coded Number!

    // threshold finding did not work, or histogram is empty
    if(median < 1) { result.first = -1; result.second = -1; return result;}
    // to few entries
    if(median < median_thr) { result.first = -1; result.second = -1; return result;}

    // get histo properties

    float thr = median / 2.;
    int first_bin = histo->FindFirstBinAbove(thr); // still needs to be mltiplied by bin_length
    int last_bin = histo->FindLastBinAbove(thr); // still needs to be mltiplied by bin_length
    if(median >= median_thr) {

        //check if there is another bin before/after this one
        //first, check for noise

        // if the first noise bin is close to the thr bin, there is probably very few noise and
        // the first noise bin is probably the correct thr_bin

        int thr_bin = first_bin*bin_length;

        // TF1 *f1 = new TF1("f1","[0] + [1]/(1+exp(-1*[2]*(x-[3])))",thr_bin-200,thr_bin+200);
        TF1 *f2 = new TF1("f2","[0]/2*(1-TMath::Erf((x-[2])/(sqrt(2)*[1])))",thr_bin-200,thr_bin+200);
        // f1->SetParameter(0,0);
        // f1->SetParameter(1,median);
        // f1->SetParameter(2,1./200.);
        // f1->SetParameter(3,thr_bin);

        f2->SetParameter(0,median);
        f2->SetParameter(1,-5.);
        f2->SetParameter(2,thr_bin);

        // histo->Fit("f1","","",thr_bin-50, thr_bin+50);
        histo->Fit("f2","QL","",thr_bin-50, thr_bin+50); // XXX: Fit Range Optimal?

        // result.first = first_bin*bin_length;
        // if(f1->GetParameter(2) < 0.01 || f1->GetParameter(2) > 1 || f1->GetParError(3) > 2) {
        if(f2->GetParError(2) > 2) { // XXX: Hard coded Number
            // result.first = first_bin*bin_length;
            result.first = first_bin*bin_length;
        } else {
            _count_working_fits++;
            // result.first = f1->GetParameter(3);
            result.first = f2->GetParameter(2);
        }
        _count_thr_edges++;

        // result.first = first_bin*bin_length;
        result.second = last_bin*bin_length;
    } else if(median >= 1) {
        if(median <= 2) {
            result.first = bin_length*(first_bin - exp(-median) - 2*exp(-2*median));
        } else {
            result.first = bin_length*(first_bin);
        }
        result.second = last_bin*bin_length;
    } else {
        result.first = -1;
        result.second = -1;
    }

    return result;
}

// std::pair<double,double> TDC_calibration::fitPlateau(TH1I* histo, double min_edge, double max_edge) {
//     TF1 *f_lin = new TF1("f_lin","[0] + [1]*x", min_edge+300, max_edge-500);
//     histo->Fit("f_lin","","",min_edge+300, max_edge-500);
//
//     return std::make_pair(f_lin->GetParameter(0), f_lin->GetParameter(1));
// }

/* write the tdc calibration to file */
void TDC_calibration::writeCalibration() {
    std::cout << "Writing calibration to " << _fn_tdc_calibration << std::endl;

    ofstream out_file;
    out_file.open(_fn_tdc_calibration.c_str());

    out_file << "ChipID\tChannel\tMemoryCell\ttdc_min_even\ttdc_max_even\ttdc_min_odd\ttdc_max_odd\ttdc_min_all\ttdc_max_all\n";

    _count_working_fits = 0; // for statistics
    _count_thr_edges = 0; // for statistics
    for(auto& p_chipHistos : mh_TDCs_ChiChaMem_even) {
        int chipID = p_chipHistos.first;
        auto vv_histos = p_chipHistos.second;

        std::map<bool, std::vector<double> > v_channel_pedestal;
        std::map<bool, std::vector<double> > v_channel_maxEdge;
        std::map<bool, std::vector<double> > v_channel_slope;

        std::map<bool, double> mean_channel_pedestal;
        std::map<bool, double> mean_channel_maxEdge;
        std::map<bool, double> mean_channel_slope;

        for(unsigned int iChannel=0; iChannel < vv_histos.size(); iChannel++) {
            std::vector<TH1I*> v_histos = vv_histos[iChannel];

            double sum_slope_even = 0;
            double sum_slope_odd = 0;
            double sum_pedestal_even = 0;
            double sum_pedestal_odd = 0;
            double sum_maxEdge_even = 0;
            double sum_maxEdge_odd = 0;
            std::vector<double> v_slope_even;
            std::vector<double> v_slope_odd;
            std::vector<double> v_pedestal_even;
            std::vector<double> v_pedestal_odd;
            std::vector<double> v_maxEdge_even;
            std::vector<double> v_maxEdge_odd;


            for(unsigned int memoryCell=0; memoryCell < v_histos.size(); memoryCell++) {
                TH1I* histo = v_histos[memoryCell];
                if(chipID == 142 && iChannel == 0) {
                    std::cout << "142: "<< memoryCell << "\t" << histo->GetEntries() << std::endl;
                }

                //even bunchcrossings
                std::pair<double, double> p_edges = calculateEdges(histo);
                double pedestal = p_edges.first;
                double maxEdge = p_edges.second;

                //calculate Plateau non-linearity
                // std::pair<double, double> p_nl = fitPlateau(histo, pedestal, maxEdge);
                int iChannel2 = iChannel;
                if(_fOnlyCalibrateT0s && (pedestal <= 0 || maxEdge <= 0)) continue;
                if(_fOnlyCalibrateT0s) {
                    for(auto& p: v_T0) {
                        if(p.first == chipID) iChannel2 = p.second;
                    }
                }

                out_file << chipID << "\t";
                out_file << iChannel2 << "\t";
                out_file << memoryCell << "\t";

                out_file << pedestal << "\t";
                out_file << maxEdge << "\t";

                // Bunchcrossing length minus deadtime (4000ns * 0.98);
                double slope = _bxPeriod * (1. - _rampDeadtime) / (double)(maxEdge - pedestal);
                if(pedestal > 0 && maxEdge > 0) {
                    sum_slope_even += slope;
                    sum_pedestal_even += pedestal;
                    sum_maxEdge_even += maxEdge;
                    v_slope_even.push_back(slope);
                    v_pedestal_even.push_back(pedestal);
                    v_maxEdge_even.push_back(maxEdge);

                    int id = memoryCell + iChannel*100 + chipID*100*100;
                    _m_id_pedestal[id] = pedestal;
                    _m_id_maxEdge[id] = maxEdge;
                    _m_id_slope[id] = slope;
                    if(memoryCell == 0) {
                        v_channel_pedestal[true].push_back(pedestal);
                        v_channel_maxEdge[true].push_back(maxEdge);
                        v_channel_slope[true].push_back(slope);
                        mean_channel_pedestal[true] += pedestal;
                        mean_channel_maxEdge[true] += maxEdge;
                        mean_channel_slope[true] += slope;
                    }
                }

                h_tdcSlopes->Fill(slope);
                h_pedestal->Fill(pedestal);
                h_maxEdge->Fill(maxEdge);

                //odd bunchcrossings
                histo = mh_TDCs_ChiChaMem_odd[chipID][iChannel][memoryCell];
                p_edges = calculateEdges(histo);

                pedestal = p_edges.first;
                maxEdge = p_edges.second;

                // p_nl = fitPlateau(histo, pedestal, maxEdge);

                out_file << pedestal << "\t";
                out_file << maxEdge << "\t";

                // Bunchcrossing length minus deadtime (4000ns * 0.98);
                slope = _bxPeriod * (1. - _rampDeadtime) / (double)(maxEdge - pedestal);
                if(pedestal > 0 && maxEdge > 0) {
                    sum_slope_odd += slope;
                    sum_pedestal_odd += pedestal;
                    sum_maxEdge_odd += maxEdge;
                    v_slope_odd.push_back(slope);
                    v_pedestal_odd.push_back(pedestal);
                    v_maxEdge_odd.push_back(maxEdge);

                    int id = memoryCell + iChannel*100 + chipID*100*100;
                    _m_id_pedestal[-1*id] = pedestal;
                    _m_id_maxEdge[-1*id] = maxEdge;
                    _m_id_slope[-1*id] = slope;
                    if(memoryCell == 0) {
                        v_channel_pedestal[false].push_back(pedestal);
                        v_channel_maxEdge[false].push_back(maxEdge);
                        v_channel_slope[false].push_back(slope);
                        mean_channel_pedestal[false] += pedestal;
                        mean_channel_maxEdge[false] += maxEdge;
                        mean_channel_slope[false] += slope;
                    }
                }

                h_tdcSlopes->Fill(slope);
                h_pedestal->Fill(pedestal);
                h_maxEdge->Fill(maxEdge);

                // std::cout << chipID << " " << "" << iChannel << " " << memoryCell <<std::endl;

                //all bunchcrossings
                histo = mh_TDCs_ChiChaMem_all[chipID][iChannel][memoryCell];
                p_edges = calculateEdges(histo);

                pedestal = p_edges.first;
                maxEdge = p_edges.second;

                // p_nl = fitPlateau(histo, pedestal, maxEdge);

                out_file << pedestal << "\t";
                out_file << maxEdge << "\t";

                out_file << "\n";
            }

            // std::cout << "I get into here" << std::endl;

            // histograms that show deviation for pedestal, maxEdge and slope with
            // within the memory cells
            if(v_slope_even.size() > 1) {
                std::sort(v_slope_even.begin(), v_slope_even.begin());
                std::sort(v_pedestal_even.begin(), v_pedestal_even.begin());
                std::sort(v_maxEdge_even.begin(), v_maxEdge_even.begin());
                double median_slope_even = v_slope_even[v_slope_even.size()/2];
                // double median_pedestal_even = v_pedestal_even[v_pedestal_even.size()/2];
                double median_maxEdge_even = v_maxEdge_even[v_maxEdge_even.size()/2];

                // double mean_slope_even = sum_slope_even/(double)v_slope_even.size();
                // double mean_pedestal_even = sum_pedestal_even/(double)v_pedestal_even.size();
                // double mean_maxEdge_even = sum_maxEdge_even/(double)v_maxEdge_even.size();
                for(unsigned int i=1; i < v_slope_even.size(); i++) {
                    if(i == v_slope_even.size()/2) continue; // skip the median, because the distance is trivially 0
                    h_memCell_slope->Fill(v_slope_even[i] - median_slope_even);
                    // h_memCell_pedestal->Fill(v_pedestal_even[i] - median_pedestal_even);
                    h_memCell_pedestal->Fill(v_pedestal_even[i] - v_pedestal_even[0]);
                    h_memCell_maxEdge->Fill(v_maxEdge_even[i] - median_maxEdge_even);
                }
            }
            if(v_slope_odd.size() > 1) {
                std::sort(v_slope_odd.begin(), v_slope_odd.begin());
                std::sort(v_pedestal_odd.begin(), v_pedestal_odd.begin());
                std::sort(v_maxEdge_odd.begin(), v_maxEdge_odd.begin());
                double median_slope_odd = v_slope_odd[v_slope_odd.size()/2];
                // double median_pedestal_odd = v_pedestal_odd[v_pedestal_odd.size()/2];
                double median_maxEdge_odd = v_maxEdge_odd[v_maxEdge_odd.size()/2];

                // double mean_slope_odd = sum_slope_odd/(double)v_slope_odd.size();
                // double mean_pedestal_odd = sum_pedestal_odd/(double)v_pedestal_odd.size();
                // double mean_maxEdge_odd = sum_maxEdge_odd/(double)v_maxEdge_odd.size();
                for(unsigned int i=0; i < v_slope_odd.size(); i++) {
                    if(i == v_slope_even.size()/2) continue; // skip the median, because the distance is trivially 0
                    h_memCell_slope->Fill(v_slope_odd[i] - median_slope_odd);
                    // h_memCell_pedestal->Fill(v_pedestal_odd[i] - median_pedestal_odd);
                    h_memCell_pedestal->Fill(v_pedestal_odd[i] - v_pedestal_odd[0]);
                    h_memCell_maxEdge->Fill(v_maxEdge_odd[i] - median_maxEdge_odd);
                }
            }
            // std::cout << "WTF" << std::endl;
        }
        // std::cout << "Nearly there" << std::endl;
        mean_channel_pedestal[true] = mean_channel_pedestal[true] / (double) v_channel_pedestal[true].size();
        mean_channel_maxEdge[true] = mean_channel_maxEdge[true] / (double) v_channel_maxEdge[true].size();
        mean_channel_slope[true] = mean_channel_slope[true] / (double) v_channel_slope[true].size();
        mean_channel_pedestal[false] = mean_channel_pedestal[false] / (double) v_channel_pedestal[false].size();
        mean_channel_maxEdge[false] = mean_channel_maxEdge[false] / (double) v_channel_maxEdge[false].size();
        mean_channel_slope[false] = mean_channel_slope[false] / (double) v_channel_slope[false].size();

        for(unsigned int i=0; i < v_channel_pedestal[true].size(); i++) {
            h_channel_pedestal->Fill(mean_channel_pedestal[true] - v_channel_pedestal[true][i]);
            h_channel_pedestal->Fill(mean_channel_pedestal[false] - v_channel_pedestal[false][i]);
            h_channel_maxEdge->Fill(mean_channel_maxEdge[true] - v_channel_maxEdge[true][i]);
            h_channel_maxEdge->Fill(mean_channel_maxEdge[false] - v_channel_maxEdge[false][i]);
            h_channel_slope->Fill(mean_channel_slope[true] - v_channel_slope[true][i]);
            h_channel_slope->Fill(mean_channel_slope[false] - v_channel_slope[false][i]);
        }
        // std::cout << "there" << std::endl;
    }
    out_file.close();
}

void TDC_calibration::readRootFile() {
    TFile* f = new TFile(_fn_RootInput.c_str());
    if (f->IsOpen()) {
        std::cout << "Reading root file" << std::endl;

        //read available ChipIDs and MemCells
        TVectorD* v_chipIDs = (TVectorD*)f->Get("v_chipIDs");
        TVectorD* v_channels = (TVectorD*)f->Get("v_channels");
        TVectorD* v_memCells = (TVectorD*)f->Get("v_memCells");

        // std::cout << std::endl;
        // std::cout << "\r" << std::setfill(' ') << std::setw(50) << "";
        for(int i=0; i < v_chipIDs->GetNoElements(); i++) {
            int chipID = (*v_chipIDs)[i];
            if(chipID <= 0) continue;


            std::vector< std::vector<TH1I*> > vvh_even;
            std::vector< std::vector<TH1I*> > vvh_odd;
            std::vector< std::vector<TH1I*> > vvh_all;
            for(int k=0; k < v_channels->GetNoElements(); k++) {
                int channel = (*v_channels)[k];

                if(_fOnlyCalibrateT0s && not _timingManager->isT0(std::make_pair(chipID, channel))) continue;

                std::vector<TH1I*> vh_even;
                std::vector<TH1I*> vh_odd;
                std::vector<TH1I*> vh_all;
                for(int j=0; j < v_memCells->GetNoElements(); j++) {
                    int memCell = (*v_memCells)[j];

                    std::stringstream hist_name;
                    hist_name << "h_TDCs_CID_" << chipID << "_" << channel << "_" << memCell << "_even;1";
                    TH1I* h = (TH1I*)f->Get(hist_name.str().c_str());
                    // std::cout << hist_name.str() << "\t" << h->GetEntries() << std::endl;
                    vh_even.push_back(h);

                    hist_name.str(std::string());
                    hist_name << "h_TDCs_CID_" << chipID << "_" << channel << "_" << memCell << "_odd;1";
                    h = (TH1I*)f->Get(hist_name.str().c_str());
                    vh_odd.push_back(h);

                    hist_name.str(std::string());
                    hist_name << "h_TDCs_CID_" << chipID << "_" << channel << "_" << memCell << "_all;1";
                    h = (TH1I*)f->Get(hist_name.str().c_str());
                    vh_all.push_back(h);
                    // std::cout << h->GetEntries() << std::endl;
                }
                vvh_even.push_back(vh_even);
                vvh_odd.push_back(vh_odd);
                vvh_all.push_back(vh_all);
            }
            mh_TDCs_ChiChaMem_even.insert(std::make_pair(chipID, vvh_even) );
            mh_TDCs_ChiChaMem_odd.insert(std::make_pair(chipID, vvh_odd) );
            mh_TDCs_ChiChaMem_all.insert(std::make_pair(chipID, vvh_all) );
            std::cout << "read chip: ";
            std::cout << std::setw(3) << std::setfill(' ') << chipID << " / ";
            std::cout << std::setw(3) << (*v_chipIDs)[v_chipIDs->GetNoElements()-1] << std::endl;
        }
    } else {
        std::cout << "!!! Could not open root file for reading!!! " << std::endl;
    }
}

void TDC_calibration::writeRootFile() {

    if(not _useRootFile) {
        std::cout << "Writing TDC spectra to " << _fn_root_spectra << std::endl;
        // the calibration TDC spectra
        TFile* cali_file = new TFile(_fn_root_spectra.c_str(), "recreate");
        if (cali_file->IsOpen()) {
            // saves all available chips
            TVectorD v_chipIDs(_v_chipIDs.size());
            for(unsigned int i=0; i < _v_chipIDs.size(); i++) {
                v_chipIDs[i] = _v_chipIDs[i];
            }
            TVectorD v_memCells(_nMemoryCells);
            for(int iMem=0; iMem < _nMemoryCells; iMem++) {
                v_memCells[iMem] = iMem;
            }
            TVectorD v_channels(_nChannels+1);
            for(int iChan=0; iChan < _nChannels+1; iChan++) {
                v_channels[iChan] = iChan;
            }
            v_chipIDs.Write("v_chipIDs");
            v_channels.Write("v_channels");
            v_memCells.Write("v_memCells");

            TNtuple* nt_t0s = new TNtuple("t0s", "chipID and channel of T0s", "chipID:channel");
            for(auto& t0: v_T0) {
                nt_t0s->Fill(t0.first, t0.second);
            }
            nt_t0s->Write();

            for(auto& chipID : _v_chipIDs) {
                for(int iChannel = 0; iChannel < _nChannels+1; iChannel++) {
                    if(_fOnlyCalibrateT0s && not _timingManager->isT0(std::make_pair(chipID, iChannel))) continue;
                    for(int iMemCell = 0; iMemCell < _nMemoryCells; iMemCell++) {
                        mh_TDCs_ChiChaMem_even[chipID][iChannel][iMemCell]->Write();
                        mh_TDCs_ChiChaMem_odd[chipID][iChannel][iMemCell]->Write();
                        mh_TDCs_ChiChaMem_all[chipID][iChannel][iMemCell]->Write();
                    }
                }
            }
        } else {
            std::cout << "!!! Could not open root file. Histograms are not saved!" << std::endl;
        }

        cali_file->Close();
        delete cali_file;
    }


    // histograms that analyse the calibration outcome
    TFile* ana_file = new TFile(_fn_root_analysis.c_str(), "recreate");
    if (ana_file->IsOpen()) {
        std::cout << "Writing analysis file to " << _fn_root_analysis << std::endl;
        TVectorD v_chipIDs(_v_chipIDs.size());
        for(unsigned int i=0; i < _v_chipIDs.size(); i++) {
            v_chipIDs[i] = _v_chipIDs[i];
        }
        TVectorD v_memCells(_nMemoryCells);
        for(int iMem=0; iMem < _nMemoryCells; iMem++) {
            v_memCells[iMem] = iMem;
        }
        TVectorD v_channels(_nChannels);
        for(int iChan=0; iChan < _nChannels; iChan++) {
            v_channels[iChan] = iChan;
        }
        v_chipIDs.Write("v_chipIDs");
        v_channels.Write("v_channels");
        v_memCells.Write("v_memCells");

        TNtuple* nt_t0s = new TNtuple("t0s", "chipID and channel of T0s", "chipID:channel");
        for(auto& t0: v_T0) {
            nt_t0s->Fill(t0.first, t0.second);
        }
        nt_t0s->Write();

        h_channels->Write();

        h_tdcSlopes->Write();
        h_pedestal->Write();
        h_maxEdge->Write();
        // h_delta_ns->Write();
        h_memCell_pedestal->Write();
        h_memCell_maxEdge->Write();
        h_memCell_slope->Write();

        h_channel_pedestal->Write();
        h_channel_maxEdge->Write();
        h_channel_slope->Write();

        std::vector<double> v_id;
        std::vector<double> v_pedestal;
        std::vector<double> v_maxEdge;
        std::vector<double> v_slope;

        std::vector<double> v_sumMemCell_id;
        std::vector<double> v_sumMemCell_pedestal;
        std::vector<double> v_sumMemCell_maxEdge;
        std::vector<double> v_sumMemCell_slope;

        for(auto& p_ped: _m_id_pedestal) {
            int id = p_ped.first;
            if(id %100 == 0) {
                v_sumMemCell_id.push_back(id/100);
                v_sumMemCell_pedestal.push_back(p_ped.second);
                v_sumMemCell_maxEdge.push_back(_m_id_maxEdge[id]);
                v_sumMemCell_slope.push_back(_m_id_slope[id]);
            }
            v_id.push_back(id);
            v_pedestal.push_back(p_ped.second);
            v_maxEdge.push_back(_m_id_maxEdge[id]);
            v_slope.push_back(_m_id_slope[id]);
        }

        // TGraph *gr_id_pedestal = new TGraph(v_id.size(), &v_id[0], &v_pedestal[0]);
        // gr_id_pedestal->SetTitle("Pedestals in tdc");
        // gr_id_pedestal->SetName("gr_id_pedestal");
        // gr_id_pedestal->GetXaxis()->SetTitle("ID");
        // gr_id_pedestal->GetYaxis()->SetTitle("pedestal [tdc]");
        // gr_id_pedestal->Write();
        //
        // TGraph *gr_id_maxEdge = new TGraph(v_id.size(), &v_id[0], &v_maxEdge[0]);
        // gr_id_maxEdge->SetTitle("maxEdge in tdc");
        // gr_id_maxEdge->SetName("gr_id_maxEdge");
        // gr_id_maxEdge->GetXaxis()->SetTitle("ID");
        // gr_id_maxEdge->GetYaxis()->SetTitle("maxEdge [tdc]");
        // gr_id_maxEdge->Write();
        //
        // TGraph *gr_id_slope = new TGraph(v_id.size(), &v_id[0], &v_slope[0]);
        // gr_id_slope->SetTitle("slope in tdc");
        // gr_id_slope->SetName("gr_id_slope");
        // gr_id_slope->GetXaxis()->SetTitle("ID");
        // gr_id_slope->GetYaxis()->SetTitle("slope [tdc]");
        // gr_id_slope->Write();
        //
        // TGraph *gr_id_sumMemCell_pedestal = new TGraph(v_sumMemCell_id.size(), &v_sumMemCell_id[0], &v_sumMemCell_pedestal[0]);
        // gr_id_sumMemCell_pedestal->SetTitle("Pedestals in tdc");
        // gr_id_sumMemCell_pedestal->SetName("gr_id_sumMemCell_pedestal");
        // gr_id_sumMemCell_pedestal->GetXaxis()->SetTitle("ID");
        // gr_id_sumMemCell_pedestal->GetYaxis()->SetTitle("pedestal [tdc]");
        // gr_id_sumMemCell_pedestal->Write();
        //
        // TGraph *gr_id_sumMemCell_maxEdge = new TGraph(v_sumMemCell_id.size(), &v_sumMemCell_id[0], &v_sumMemCell_maxEdge[0]);
        // gr_id_sumMemCell_maxEdge->SetTitle("maxEdge in tdc");
        // gr_id_sumMemCell_maxEdge->SetName("gr_id_sumMemCell_maxEdge");
        // gr_id_sumMemCell_maxEdge->GetXaxis()->SetTitle("ID");
        // gr_id_sumMemCell_maxEdge->GetYaxis()->SetTitle("maxEdge [tdc]");
        // gr_id_sumMemCell_maxEdge->Write();
        //
        // TGraph *gr_id_sumMemCell_slope = new TGraph(v_sumMemCell_id.size(), &v_sumMemCell_id[0], &v_sumMemCell_slope[0]);
        // gr_id_sumMemCell_slope->SetTitle("solpe in tdc");
        // gr_id_sumMemCell_slope->SetName("gr_id_sumMemCell_solpe");
        // gr_id_sumMemCell_slope->GetXaxis()->SetTitle("ID");
        // gr_id_sumMemCell_slope->GetYaxis()->SetTitle("solpe [tdc]");
        // gr_id_sumMemCell_slope->Write();

        //write the chips that include the T0s
        for(auto& t0 : v_T0) {
            if(_fOnlyCalibrateT0s) {
                for(auto& histo : mh_TDCs_ChiChaMem_even[t0.first][0]) {
                    histo->Write();
                    // Calculate Autocorrelation
                    int last_value = 0;
                    std::vector<double> v_autocorr;
                    std::vector<double> v_x;

                    for(int b=10; b < histo->GetNbinsX()-10; b++) {
                        std::vector<int> ys;
                        double mean = 0.;
                        for(int bb = b-10; bb < b+10; bb++) {
                            mean += histo->GetBinContent(bb);
                            ys.push_back(histo->GetBinContent(bb));
                        }
                        std::sort(ys.begin(), ys.end());
                        mean = mean / (double)ys.size();
                        int median = ys[ys.size() / 2];
                        double sigma = 0.;
                        for(auto& y: ys) {
                            sigma += (y-mean) * (y-mean);
                        }
                        sigma = sigma / (double)(ys.size() - 1);
                        int this_value = histo->GetBinContent(b);
                        // std::cout << median << " " << this_value << " " << last_value <<  std::endl;

                        double ac = 0;
                        if(last_value == 0) {
                            ac = 0;
                        } else {
                            ac = (this_value - mean) * (last_value - mean) / (sigma*sigma);
                        }
                        v_x.push_back(b);
                        v_autocorr.push_back(ac);
                        last_value = this_value;
                        // std::cout << last_value << " " << this_value  << " " << b << " " << histo->GetBinContent(b) << std::endl;
                    }

                    TGraph* gr_ac = new TGraph(v_x.size(), &(v_x[0]), &(v_autocorr[0]));
                    std::stringstream ss_title;
                    ss_title << "Autocorrelation " << histo->GetTitle();
                    std::stringstream ss_name;
                    ss_name << "gr_ac_" << histo->GetName();
                    gr_ac->SetTitle(ss_title.str().c_str());
                    gr_ac->SetName(ss_name.str().c_str());
                    gr_ac->GetXaxis()->SetTitle("TDCs");
                    gr_ac->GetYaxis()->SetTitle("Autocorrelation");
                    gr_ac->Write();
                    delete gr_ac;
                }
                for(auto& histo : mh_TDCs_ChiChaMem_odd[t0.first][0]) {
                    histo->Write();
                }

            }
        }

        // for(auto& chipID : _v_chipIDs) {
        //     for(int iChannel = 0; iChannel <= _nChannels; iChannel++) {
        //         if(_fOnlyCalibrateT0s && not _timingManager->isT0(std::make_pair(chipID, iChannel))) continue;
        //         for(int iMemCell=1; iMemCell <= 4; iMemCell++) {
        //             // if(iChannel == 5) iChannel = _nChannels;
        //             if(mh_TDCs_ChiChaMem_all[chipID][iChannel][iMemCell]->GetEntries() > 50000) {
        //                 mh_TDCs_ChiChaMem_even[chipID][iChannel][iMemCell]->Write();
        //                 mh_TDCs_ChiChaMem_odd[chipID][iChannel][iMemCell]->Write();
        //                 mh_TDCs_ChiChaMem_all[chipID][iChannel][iMemCell]->Write();
        //             }
        //         }
        //     }
        // }

    } else {
        std::cout << "!!! Could not open root file. Histograms are not saved!" << std::endl;
    }

    ana_file->Close();
    delete ana_file;

}

void TDC_calibration::end() {

    if(_useRootFile) {
        readRootFile();
    }

    writeCalibration();
    writeRootFile();


    std::cout << _tmp_counter << std::endl;

    std::cout << "Working fits: " << _count_working_fits << " / " << (_nChips-12) * _nMemoryCells * _nChannels * 2 << std::endl;
    std::cout << "With thr method this would be: " << _count_thr_edges << " / " << (_nChips-12) * _nMemoryCells * _nChannels * 2 << std::endl;

    std::cout << "TDC_calibration::end()  " << name()
 	      << " processed " << _nEvt << " events in " << _nRun << " runs "
     	      << std::endl ;
}
