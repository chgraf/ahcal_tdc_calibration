#include "TDC_analysis.h"

#include <algorithm> //std::sort
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <tuple>
#include <math.h>

#include <EVENT/LCCollection.h>
#include <EVENT/LCGenericObject.h>
#include <EVENT/CalorimeterHit.h>

#include "MappingProcessor.hh"
#include "Ahc2HardwareConnection.hh" // ----- include for verbosity dependend logging ---------
#include "marlin/VerbosityLevels.h"
#include "marlin/Exceptions.h"

#include "TCanvas.h"
#include "TF1.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMath.h"
#include "TProfile.h"


using namespace lcio;
using namespace marlin;
using namespace CALICE;


TDC_analysis aTDC_analysis;


TDC_analysis::TDC_analysis() : Processor("TDC_analysis") {

    // modify processor description
    _description = "TDC_analysis has a first look on the TDC data ..." ;


    // register steering parameters: name, description, class-variable, default value

    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCALRaw",
            "Name of the HCAL input collection for raw data",
            _colName_HCAL_Raw ,
            std::string("EUDAQDataHCAL")
    );

    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCALReco",
            "Name of the HCAL input collection for reco data",
            _colName_HCAL_Reco,
            std::string("Ahc2Calorimeter_Hits")
    );

    registerInputCollection( LCIO::LCGENERICOBJECT,
            "CollectionNameHCALTracks",
            "Name of the HCAL input collection for tracked data",
            _colName_HCAL_Tracks,
            std::string("Ahc2Calorimeter_Tracks")
    );

    registerProcessorParameter(
        "isTrackData",
        "Use the track collection",
        _isTrackData,
        false
    );

    registerProcessorParameter(
        "TestbeamCampaign",
        "TestbeamCampaign: Aug15, Jul15",
        _TBCampaign,
        std::string("Aug15")
    );

    registerProcessorParameter(
        "TDCCalibrationFilename",
        "name of the TDC calibration file",
        _fn_tdc_calibration,
        std::string("tdc_calibration.txt")
    );

    registerProcessorParameter(
        "TimingCalibrationFilename",
        "name of the Timing calibration file",
        _fn_timing_calibration,
        std::string("timing_calibration.txt")
    );

    registerProcessorParameter(
        "NonLinearityCorrectionFilename",
        "name of the nonLinearity correction file",
        _fn_nonLinearity_correction,
        std::string("tdc_non_linearity.txt")
    );

    registerProcessorParameter(
        "OutputRootFilename",
        "name of the Root input file",
        _fn_root_out,
        std::string("analyse.root")
    );

    registerProcessorParameter(
        "InputRootFilename",
        "name of the Root output file",
        _fn_root_in,
        std::string("hits.root")
    );

    registerProcessorParameter(
        "UseRootInputFile",
        "Should the input be from a root file?",
        _isRootInput,
        bool(false)
    );

    registerProcessorParameter(
        "MappingProcessorName" ,
        "Name of the MappingProcessor instance that provides the geometry of the detector." ,
        _mappingProcessorName,
        std::string("MyMappingProcessor")
    );

    registerProcessorParameter(
        "HardwareConnectionCollection" ,
        "Name of the Ahc2HardwareConnection Collection",
        _Ahc2HardwareConnectionName,
		std::string("Ahc2HardwareConnection")
    );

    registerProcessorParameter(
        "DataSet" ,
        "Which DataSet? MUONS=0, PION70_TRACKS=1, PIONS70=2",
        p_DataSet,
		0
    );

}

void TDC_analysis::init() {
    std::cout << "Init..." << std::endl;

    _dataSet = DataSet(p_DataSet);

    _timingManager = new TimingManager();

    // ---------- config ----------
    _timingManager->setNChannels(36);
    _timingManager->setNMemoryCells(16);
    _timingManager->setBxPeriod(4000);
    _timingManager->setRampDeadtime(0.02);

    _nChannels = _timingManager->getNChannels();
    _nMemoryCells = _timingManager->getNMemoryCells();

    if(_TBCampaign == "Aug15") {
        // v_T0.push_back(std::pair<int, int>(185,29));
        // v_T0.push_back(std::pair<int, int>(177,29));
        v_T0.push_back(std::pair<int, int>(201,29));
        v_T0.push_back(std::pair<int, int>(217,23));
    } else if(_TBCampaign == "Jul15") {
        v_T0.push_back(std::pair<int, int>(169,29));
        v_T0.push_back(std::pair<int, int>(177,23));
        v_T0.push_back(std::pair<int, int>(185,29));
        v_T0.push_back(std::pair<int, int>(217,23));
    }

    _nT0s = v_T0.size();

    _broken_channels.push_back(std::make_pair(144, 29));
    _broken_channels.push_back(std::make_pair(154, 6));

    _broken_chips.push_back(141);
    _broken_chips.push_back(144);

    for(int i=0; i <= _nT0s; i++) {
        tmp_count_T0s.push_back(0);
    }

    streamlog_out(DEBUG) << "   init called  " << std::endl ;

    // usually a good idea to
    printParameters() ;

    _timingManager->setT0s(v_T0);


    std::cout << "Reading TDC Calibration... " << std::endl;
    // _timingManager->readTDCCalibration_extended(_fn_tdc_calibration.c_str());
    _timingManager->readTDCCalibration_extended(_fn_tdc_calibration.c_str());

    std::cout << "Reading Timing Calibration... " << std::endl;
    _timingManager->readTimingCalibration_extended(_fn_timing_calibration.c_str());

    std::cout << "Reading NonLinearity Correction... " << std::endl;
    _timingManager->readNonLinearityCorrection(_fn_nonLinearity_correction.c_str());

    _isInitialized = false;

    _nRun = 0 ;
    _nEvt = 0 ;

}

void TDC_analysis::init_histograms() {

    // ---------- Init Histos ----------
    h_tdcSlopes = new TH1D("h_tdcSlopes", "TDC Slopes", 300, 0, 3);
    h_tdcSlopes->GetXaxis()->SetTitle("ramp slope [ns/bin]");
    h_tdcSlopes->GetYaxis()->SetTitle("#");

    h_pedestal = new TH1D("h_pedestal", "TDC pedestals", 2000, 0, 2000);
    h_pedestal->GetXaxis()->SetTitle("pedestal [tdc]");
    h_pedestal->GetYaxis()->SetTitle("#");

    h_maxEdge = new TH1D("h_maxEdge", "TDC Second Edge of TDC Spectrum", 2000, 2000, 4000);
    h_maxEdge->GetXaxis()->SetTitle("maxEdge [tdc]");
    h_maxEdge->GetYaxis()->SetTitle("#");

    h_memCell_pedestal = new TH1D("h_memCell_pedestal", "Differences in pedestal of all the memCells in one chip to the median", 200, -100, 100);
    h_memCell_pedestal->GetXaxis()->SetTitle("ramp pedestal [tdc]");
    h_memCell_pedestal->GetYaxis()->SetTitle("#");
    h_memCell_slope = new TH1D("h_memCell_slope", "Differences in slope of all the memCells in one chip to the median", 2000, -0.1, 0.1);
    h_memCell_slope->GetXaxis()->SetTitle("ramp slope [ns/bin]");
    h_memCell_slope->GetYaxis()->SetTitle("#");
    h_memCell_maxEdge = new TH1D("h_memCell_maxEdge", "Differences in maxEdge of all the memCells in one chip to the median", 200, -100, 100);
    h_memCell_maxEdge->GetXaxis()->SetTitle("ramp maxEdge [tdc]");
    h_memCell_maxEdge->GetYaxis()->SetTitle("#");

    h_T0_memCell = new TH1D("h_T0_memCell", "Investigating the T0 deviations", 17, 0, 16);
    h_T0_memCell->GetXaxis()->SetTitle("MemCell");
    h_T0_memCell->GetYaxis()->SetTitle("#");

    //  T0s
    unsigned int iT0 = 0;
    for(auto& p_T0 : v_T0) {

        std::stringstream hist_name;
        hist_name << "h_TDCs_T0s_" << p_T0.first << "_" << p_T0.second;
        std::stringstream hist_title;
        hist_title << "TDCs for hitbit == 1, T0 " << p_T0.first << " " << p_T0.second;
        TH1I* h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 500, 0, 4000);
        vh_TDCs_T0.push_back(h_tmp);

        hist_name.str(std::string());
        hist_name << "h_TDCs_T0s_" << p_T0.first << "_" << p_T0.second << "odd";
        hist_title.str(std::string());
        hist_title << "TDCs for hitbit == 1, odd, T0 " << p_T0.first << " " << p_T0.second;
        h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 500, 0, 4000);
        vh_TDCs_T0_odd.push_back(h_tmp);

        hist_name.str(std::string());
        hist_name << "h_ADCs_T0s_" << p_T0.first << "_" << p_T0.second;
        hist_title.str(std::string());
        hist_title << "ADCs for hitbit == 1, T0 " << p_T0.first << " " << p_T0.second;
        h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 500, 0, 4000);
        vh_ADCs_T0.push_back(h_tmp);

        hist_name.str(std::string());
        hist_name << "h_T0_diff_" << iT0;
        hist_title.str(std::string());
        hist_title << "Differences of T0 " << iT0 << "(" << p_T0.first << ", " << p_T0.second << ")";
        hist_title << " to the mean of this event";
        h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 2000, -1000, 1000);
        h_tmp->GetXaxis()->SetTitle("T0 - <T0> [ns]");
        h_tmp->GetYaxis()->SetTitle("#");
        vh_T0_diff.push_back(h_tmp);

        hist_name.str(std::string());
        hist_name << "h2_T0_vs_mT0s_" << iT0;
        hist_title.str(std::string());
        hist_title << "T0 " << iT0 << "(" << p_T0.first << ", " << p_T0.second << ")";
        hist_title << " vs the mean of all other T0s";
        TH2D* h2_tmp = new TH2D(hist_name.str().c_str(), hist_title.str().c_str(), 1500, -1000, 5000, 1500, -1000, 5000);
        h2_tmp->GetXaxis()->SetTitle("T0_0 [ns]");
        h2_tmp->GetYaxis()->SetTitle("<T0> [ns]");
        h2_tmp->SetOption("COLZ");
        vh2_T0_vs_mT0s.push_back(h2_tmp);

        if(iT0 < v_T0.size() - 1) {
            for(unsigned int jT0 = iT0+1; jT0 < v_T0.size(); jT0++) {
                hist_name.str(std::string());
                hist_name << "h2_T0" << iT0 << "_vs_T0" << jT0;
                hist_title.str(std::string());
                hist_title << "T0 " << iT0 << " vs T0 " << jT0 << " in ns";
                h2_tmp = new TH2D(hist_name.str().c_str(), hist_title.str().c_str(), 1500, -1000, 5000, 1500, -1000, 5000);
                h2_tmp->GetXaxis()->SetTitle("T0 [ns]");
                h2_tmp->GetYaxis()->SetTitle("T0 [ns]");
                h2_tmp->SetOption("COLZ");
                mh2_T0_vs_each_other[iT0][jT0] = h2_tmp;

                hist_name.str(std::string());
                hist_name << "h2_T0" << iT0 << "_vs_T0" << jT0 << "_tdc";
                hist_title.str(std::string());
                hist_title << "T0 " << iT0 << " vs T0 " << jT0 << " in tdc";
                h2_tmp = new TH2D(hist_name.str().c_str(), hist_title.str().c_str(), 1500, -1000, 5000, 1500, -1000, 5000);
                h2_tmp->GetXaxis()->SetTitle("T0 [tdc]");
                h2_tmp->GetYaxis()->SetTitle("T0 [tdc]");
                h2_tmp->SetOption("COLZ");
                mh2_T0_vs_each_other_tdc[iT0][jT0] = h2_tmp;
            }
        }
        iT0++;
    }

    for(auto& chipID : _v_chipIDs) {
        TH1I* h_even;
        TH1I* h_odd;
        TH1I* h_corrected_even;
        TH1I* h_corrected_odd;
        // for(int memoryCell=0; memoryCell < 16; memoryCell++) { //run through all memory cells, except the first one (its broken)
            // BXID even
        std::stringstream hist_name;
        hist_name.str(std::string());
        hist_name << "h_TDCs_CID_" << chipID << "_even";

        std::stringstream hist_title;
        hist_title.str(std::string());
        hist_title << "TDCs for hitbit == 1, Channel " << chipID << ", BXID even";

        TH1I* h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 250, 1, 5001);
        h_tmp->GetXaxis()->SetTitle("tdc");
        h_tmp->GetYaxis()->SetTitle("#");
        h_even = h_tmp;

        // BXID odd
        hist_name.str(std::string());
        hist_name << "h_TDCs_CID_" << chipID << "_odd";

        hist_title.str(std::string());
        hist_title << "TDCs for hitbit == 1, Channel " << chipID << ", BXID odd";

        h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 250, 1, 5001);
        h_tmp->GetXaxis()->SetTitle("tdc");
        h_tmp->GetYaxis()->SetTitle("#");
        h_odd = h_tmp;

        mh_TDCs_CIDs_even.insert(std::pair<int, TH1I* >(chipID, h_even) );
        mh_TDCs_CIDs_odd.insert(std::pair<int, TH1I* >(chipID, h_odd) );

        // ----- Corected Histograms -----

        hist_name.str(std::string());
        hist_name << "h_TDCs_CID_" << chipID << "_corrected_even";

        hist_title.str(std::string());
        hist_title << "TDCs for hitbit == 1, Channel " << chipID << ", BXID even, Corrected";

        h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 250, 1, 5001);
        h_tmp->GetXaxis()->SetTitle("tdc");
        h_tmp->GetYaxis()->SetTitle("#");
        h_corrected_even = h_tmp;

        // BXID odd
        hist_name.str(std::string());
        hist_name << "h_TDCs_CID_" << chipID << "_corrected_odd";

        hist_title.str(std::string());
        hist_title << "TDCs for hitbit == 1, Channel " << chipID << ", BXID odd, Corrected";

        h_tmp = new TH1I(hist_name.str().c_str(), hist_title.str().c_str(), 250, 1, 5001);
        h_tmp->GetXaxis()->SetTitle("tdc");
        h_tmp->GetYaxis()->SetTitle("#");
        h_corrected_odd = h_tmp;

        mh_TDCs_CIDs_corrected_even.insert(std::pair<int, TH1I* >(chipID, h_corrected_even) );
        mh_TDCs_CIDs_corrected_odd.insert(std::pair<int, TH1I* >(chipID, h_corrected_odd) );

    }

    //TODO: each chip needs his own nChannelsX, nChannelsY
    _hitMap_calibrated = new HitMap(_timingManager->getKIJContainer(), std::string("Hits with Calibration"));
    _hitMap_timeResolution = new HitMap(_timingManager->getKIJContainer(), std::string("Time resolution"));
    _hitMap_timeDelay = new HitMap(_timingManager->getKIJContainer(), std::string("Time Delay"));
    _hitMap = new HitMap(_timingManager->getKIJContainer(), std::string("HitMap"));

}


void TDC_analysis::processRunHeader( LCRunHeader* run) {

    _hasInitializedRun = false;
    _nRun++ ;
}

void TDC_analysis::processHit(timing::Hit& hit) {
    // convert tdc to ns
    hit.ns = _timingManager->tdcToNs(hit.chipID, hit.channel, hit.memoryCell, hit.bxID, hit.tdc);
    _hitMap->fill(hit.I, hit.J, hit.K);

    // if(not _isReconstructedData) {
        // count the number of triggered T0s
    hit.isT0 = false;
    if(std::find(v_T0.begin(), v_T0.end(), std::pair<int, int>(hit.chipID, hit.channel)) != v_T0.end()) {
        // std::cout << "is T0" << std::endl;
        //std::cout << hit.chipID << " " << hit.I << " " << hit.J << " " << hit.K << std::endl;
        int adc_T0_thr = 0;
        if(_TBCampaign == "Jul15") {
            adc_T0_thr = 500;
        } else if(_TBCampaign == "Aug15") {
            adc_T0_thr = 1100;
        }

        //Proper T0 calibration
        hit.ns = _timingManager->tdcToNs_T0(hit.chipID, hit.channel, hit.memoryCell, hit.bxID, hit.tdc);
        // if(hit.eventID < 10000) {
        //     std::cout << hit.ns << std::endl;
        // }

        //cutting the second peak in the T0 adc spectrum
        if(hit.ns > 0 && (hit.adc > adc_T0_thr || _isReconstructedData) && hit.ns < 3500) {

            for(int k=0; k < _nT0s; k++) {
                if(std::pair<int, int>(hit.chipID, hit.channel) == v_T0[k]) {
                    hit.T0ID = k;
                    break;
                }
            }

            if(hit.bxID % 2 == 0) {
                vh_TDCs_T0[hit.T0ID]->Fill(hit.tdc);
            } else {
                vh_TDCs_T0[hit.T0ID]->Fill(hit.tdc);
            }

            vh_ADCs_T0[hit.T0ID]->Fill(hit.adc);

            hit.isT0 = true;
        }
    }
}

void TDC_analysis::reProcessEvent(timing::Event event) {
    // cut on 15 hits for the Muon data set in order to throw away events with
    // two muons
    // if(event.v_hits.size() > 15 && _dataSet == MUON) {
        // return;
    // }

    if(event.nTracks > 1) return;

    // only take events where all T0 channels reported a hit
    if(event.nT0s == _nT0s) {
        // Build vectors for scatter T0 Th2d
        double mean_T0_ns = 0.;
        std::vector<double>  vT0_ns;
        std::vector<double>  vT0_tdc;
        for(auto& t0 : event.T0s) {
            vT0_ns.push_back(t0.second.ns);
            vT0_tdc.push_back(t0.second.tdc);
            mean_T0_ns += t0.second.ns;
        }

        mean_T0_ns = mean_T0_ns / _nT0s;

        for(unsigned int i=0; i < vh_T0_diff.size(); i++) {
            vh_T0_diff[i]->Fill(vT0_ns[i] - mean_T0_ns);
            vh2_T0_vs_mT0s[i]->Fill(vT0_ns[i], mean_T0_ns);
        }

        for(unsigned int i=0; i < v_T0.size() - 1; i++) {
            for(unsigned int j=i+1; j < v_T0.size(); j++) {
                mh2_T0_vs_each_other[i][j]->Fill(vT0_ns[i], vT0_ns[j]);
                mh2_T0_vs_each_other_tdc[i][j]->Fill(vT0_tdc[i], vT0_tdc[j]);
            }
        }

        // FIXME: how to handle this case? When the T0s differ too much
        // QuickFix: just throw away. Cut was at 15ns
        if(fabs(event.T0s[0].ns - event.T0s[1].ns) < 10) {
            _v_events.push_back(event);
        } else {
            h_T0_memCell->Fill(event.T0s[0].memoryCell);
            h_T0_memCell->Fill(event.T0s[1].memoryCell);
        }

    } // end if nT0s == 2
}

void TDC_analysis::processEvent(LCEvent * evt) {

        // std::cout << evt->getCollectionNames()->size() << std::endl;
        // std::cout << _colName_HCAL << "\t" << evt->getCollectionNames()->at(4) << std::endl;
    LCCollection* col = NULL;

    if(not _hasInitializedRun) {
        const std::vector<std::string>* colNames =  evt->getCollectionNames();
        if(std::find(colNames->begin(), colNames->end(), _colName_HCAL_Reco) != colNames->end()) {
            std::cout << "Using reconstructed data from collection: " << _colName_HCAL_Reco << std::endl;
            std::cout << "Collection names stored in the event: " << std::endl;
            for(auto& n : *colNames) {
                std::cout << n << ", ";
            }
            std::cout << std::endl;
            _isReconstructedData = true;
        } else if(std::find(colNames->begin(), colNames->end(), _colName_HCAL_Raw) != colNames->end()) {
            std::cout << "Using raw data from collection: " << _colName_HCAL_Raw << std::endl;
            std::cout << "Collection names stored in the event: " << std::endl;
            for(auto& n : *colNames) {
                std::cout << n << ", ";
            }
            std::cout << std::endl;
            _isReconstructedData = false;
        } else {
            std::cout << "Could not find any of the specified collecitons..." << std::endl;
            std::cout << "Input raw data collection: " << _colName_HCAL_Raw << std::endl;
            std::cout << "Input reconstructed data collection: " << _colName_HCAL_Reco << std::endl;
            std::cout << "Collection names stored in the event: " << std::endl;
            for(auto& n : *colNames) {
                std::cout << n << ", ";
            }
            std::cout << std::endl;
        }
        _hasInitializedRun = true;
    }


    if(not _isInitialized) {
        _timingManager->initializeGeometry(_mappingProcessorName, evt->getCollection(_Ahc2HardwareConnectionName) );
        _v_chipIDs = _timingManager->getChipIDs();
        // _nChips = _v_chipIDs.size();

        init_histograms();

        if(_isRootInput) {
            std::vector<timing::Event> tmp_events;
            _timingManager->readRootFile(_fn_root_in.c_str(), tmp_events);

            for(auto& event: tmp_events) {
                timing::Event myEvent;

                for(auto& hit: event.v_hits) {
                    timing::Hit myHit = hit;
                    processHit(myHit);
                    if(myHit.isT0) {
                        myEvent.T0s[myHit.T0ID] = myHit;
                        myEvent.nT0s++;
                    }

                    // if(myHit.mip < 10 && _dataSet == DataSet::MUON) {
                        myEvent.v_hits.push_back(myHit);
                    // }

                }

                // std::cout << "myEvent size: " << myEvent.v_hits.size() << std::endl;
                reProcessEvent(myEvent);
            }

            std::cout << "Number of events with both T0s: " << _v_events.size() << std::endl;
        }

        _isInitialized = true;
    }

    try {
        if(_isTrackData) {
            col = evt->getCollection( _colName_HCAL_Tracks) ;
        } else if(_isReconstructedData) {
            col = evt->getCollection( _colName_HCAL_Reco) ;
        } else {
            col = evt->getCollection( _colName_HCAL_Raw) ;
        }
    } catch (const std::exception& e) { }

    // this will only be entered if the collection is available
    if( col != NULL && not _isRootInput) {

        int nHits = col->getNumberOfElements();

        // stores all the hits in the event
        timing::Event event;

        for(int i=0; i< nHits ; i++){
            // std::cout << ".";

            // load values into hit struct
            timing::Hit  hit;

            if(_isReconstructedData) {
                // get the current hit
                // std::cout << "\tbefore hit assignment" << std::endl;
                // std::cout << "Encoding: " << _ahcMapper->getDecoder()->getCellIDEncoding() << std::endl;
                CalorimeterHit* p = dynamic_cast<CalorimeterHit*>( col->getElementAt( i ) ) ;

                // hit = _timingManager->FillReconstructedHit(p);
                hit = _timingManager->fillHit(p);

                // BxID has to be set explicitly using the event
                hit.bxID = evt->getParameters().getIntVal("BXID");
                // _hitMap->fill(hit.I, hit.J, hit.K);


            } else {
                // get the current hit
                LCGenericObject* p = dynamic_cast<LCGenericObject*>( col->getElementAt( i ) ) ;

                hit = _timingManager->fillHit(p);

                // throw away memoryCell 0, its broken
                if(hit.memoryCell == 0) continue;
                // just use hitbit == 1
                if(hit.hitbit == 0) continue;
            }

            if(hit.bxID > 4096) {
                std::cout << "Watch out! Corrupted readout cycle!";
                std::cout << " ... just use data that is converted from the raw .txt files";
                std::cout << " using the newest version of LabviewConverter2.cc";
            }

            processHit(hit);
            if(hit.isT0) {
                event.T0s[hit.T0ID] = hit;
                event.nT0s++;
            }

            // save hit in event
            event.v_hits.push_back(hit);
        }

        reProcessEvent(event);

    } // end if col != 0


    //-- note: this will not be printed if compiled w/o MARLINDEBUG=1 !

    streamlog_out(DEBUG) << "   processing event: " << evt->getEventNumber()
        << "   in run:  " << evt->getRunNumber() << std::endl ;

    _nEvt++;
}



void TDC_analysis::check( LCEvent * evt ) {
    // nothing to check here - could be used to fill checkplots in reconstruction processor
}

void TDC_analysis::writeEventMaps(std::vector< std::vector< std::tuple<timing::Hit, double> > > v_eventsForHitMap) {
    // Writing Hitmap for single Events
    std::cout << "Number of events for Hitmaps: " <<  v_eventsForHitMap.size() << std::endl;
    TFile* hitMap_file = new TFile("map_events.root", "recreate");
    if(hitMap_file->IsOpen()) {
        for(auto& hits: v_eventsForHitMap) {
            HitMap* hitmap = new HitMap(_timingManager->getKIJContainer(), std::string("Delay for single Events"));
            for(auto& tup: hits) {
                timing::Hit hit = std::get<0>(tup);
                double delay = std::get<1>(tup);

                hitmap->set(hit.I, hit.J, hit.K, delay);
            }
            if(_dataSet == MUON) {
                hitmap->commonZScale(-20,30);
            } else if(_dataSet == PION70) {
                hitmap->commonZScale(-80,150);
            }
            hitmap->write(hitMap_file);

            delete hitmap;
        }
    }
    hitMap_file->Close();
    delete hitMap_file;
}


Double_t median1(TH1 *h1) {
   //compute the median for 1-d histogram h1
   // stop if histogram is empty
   if(h1->GetEntries() == 0) {
       return -1;
   }

   Int_t nbins = h1->GetXaxis()->GetNbins();
   Double_t *x = new Double_t[nbins];
   Double_t *y = new Double_t[nbins];
   for (Int_t i=0;i<nbins;i++) {
      x[i] = h1->GetXaxis()->GetBinCenter(i+1);
      y[i] = h1->GetBinContent(i+1);
   }
   Double_t median = TMath::Median(nbins,x,y);
   delete [] x;
   delete [] y;
   return median;
}

void median2(TH2 *h2, std::vector<double>& medians, std::vector<double>& err, std::vector<double>& x) {
   //compute and print the median for each slice along X of h2
   Int_t nbins = h2->GetYaxis()->GetNbins();
   for (Int_t i=1;i<=nbins;i++) {
      TH1 *h1 = h2->ProjectionY("",i,i);
      Double_t median = median1(h1);

      //    only do this, if the median computation was succesfull
      if(median > 0) {
          medians.push_back(median);
          x.push_back(h2->GetXaxis()->GetBinCenter(i));
      }
    //   x.push_back((i-1)*h2->GetXaxis()->GetBinWidth(1) + 0.5*(float)h2->GetXaxis()->GetBinWidth(1));
      delete h1;
   }
}


void TDC_analysis::fit_calibration() {
    // chipID, channel(0..36), memCell(0..15), bxID(0,1,2), v_T0times, v_tdcs
    // while channel 36 is the sum of all channels, memCell 0 is the sum of all memCells
    // and bxID 2 is the sum of both bxIDs
    std::map<int, std::map<int, std::map<int, std::map<int, std::vector<std::pair<double, int> > > > > > m_tdcs;

    for(auto& evt: _v_events) {

        double T0_time = 0.;
        for(auto& T0_hit: evt.T0s) {
            T0_time += T0_hit.second.ns;
        }
        T0_time = T0_time / (double)evt.nT0s;

        evt.T0_time = T0_time;

        if(T0_time < 500 || T0_time > 3500) continue;

        for(auto& hit: evt.v_hits) {
            m_tdcs[hit.chipID][hit.channel][hit.memoryCell][(hit.bxID+1)%2].push_back(std::make_pair(T0_time,hit.tdc));
            m_tdcs[hit.chipID][hit.channel][hit.memoryCell][2].push_back(std::make_pair(T0_time,hit.tdc));
m_tdcs[hit.chipID][36][hit.memoryCell][(hit.bxID+1)%2].push_back(std::make_pair(T0_time, hit.tdc));
            m_tdcs[hit.chipID][36][hit.memoryCell][2].push_back(std::make_pair(T0_time, hit.tdc));

            m_tdcs[hit.chipID][hit.channel][0][(hit.bxID+1)%2].push_back(std::make_pair(T0_time, hit.tdc));
            m_tdcs[hit.chipID][hit.channel][0][2].push_back(std::make_pair(T0_time, hit.tdc));

            m_tdcs[hit.chipID][36][0][(hit.bxID+1)%2].push_back(std::make_pair(T0_time, hit.tdc));
            m_tdcs[hit.chipID][36][0][2].push_back(std::make_pair(T0_time, hit.tdc));
        }
    }

    TFile* o_file = new TFile("fits.root", "recreate");

    if(o_file->IsOpen()) {

        TH1D* h_compare_slope = new TH1D("h_compare_slope", "Compare fit slope with the usual one", 100, -0.1, 0.1);
        TH1D* h_compare_offset = new TH1D("h_compare_offset", "Compare fit offset with the usual one", 400, -200, 200);
        // TH1D* h_compare_quad = new TH1D("h_compare_quad", "Compare fit quad with the usual one", 100, -0.01, 0.01);


        // #pragma omp parallel num_threads(5)
        // #pragma omp for
        // for(unsigned int i=0; i < _v_chipIDs.size(); i++) {
            // int chipID = _v_chipIDs[i];
        for(auto& chipID: _v_chipIDs) {

            if(chipID > 150) continue;
            std::cout << "filling h2 for " << chipID << std::endl;
            double chipSlope_even = -1;
            double chipSlope_odd = -1;
            bool fCalculatedChipSlope = false;

            for(int iMem = 1; iMem < 2; iMem++) {

                // Calculate slope for the chip, after all the memCell 1 were done
                if(iMem > 2 && not fCalculatedChipSlope) {
                    std::vector<double> v_chnSlopes_even;
                    std::vector<double> v_chnSlopes_odd;
                    for(int iChn = 0; iChn < _nChannels + 1; iChn++) {
                        double chnSlope_even = std::get<1>(_m_calibration_values[chipID][iChn][1][1]);
                        double chnSlope_odd = std::get<1>(_m_calibration_values[chipID][iChn][1][0]);
                        if(chnSlope_even > 0) v_chnSlopes_even.push_back(chnSlope_even);
                        if(chnSlope_odd > 0) v_chnSlopes_odd.push_back(chnSlope_odd);
                        // std::cout << chnSlope_even << std::endl;
                    }
                    // get the median of all slopes
                    if(v_chnSlopes_even.size() > 0) {
                        std::sort(v_chnSlopes_even.begin(), v_chnSlopes_even.end());
                        chipSlope_even = v_chnSlopes_even[floor(v_chnSlopes_even.size()/2)];
                    }

                    if(v_chnSlopes_odd.size() > 0) {
                        std::sort(v_chnSlopes_odd.begin(), v_chnSlopes_odd.end());
                        chipSlope_odd = v_chnSlopes_odd[floor(v_chnSlopes_odd.size()/2)];
                    }

                    // std::cout << std::endl;
                    // std::cout << chipSlope_even << std::endl;
                    fCalculatedChipSlope = true;
                }

                for(int iChn = 0; iChn < _nChannels + 1; iChn++) {
                    for(int iBx=0; iBx < 2; iBx++) {
                        int nHits = m_tdcs[chipID][iChn][iMem][iBx].size();
                        if(nHits > 250) {
                            std::stringstream ss_title;
                            ss_title << chipID << " " << iChn << " " << iMem << " " << iBx;

                            int nBins_T0ns = std::min(1000, std::max(100, (int)(nHits/5)));
                            // if(nHits > 3000) nBins_T0ns = 300;
                            // else if(nHits > 2000) nBins_T0ns = 300;
                            // else if(nHits > 1000) nBins_T0ns = 200;
                            // else if(nHits > 500) nBins_T0ns = 150;
                            // else if (nHits > 250) nBins_T0ns = 100;

                            // A value around 3000 seems to be fine here
                            int nBins_HitTDC = 3000;

                            // TH2D: hit tdc over T0 ns
                            TH2D* h2_hits = new TH2D("h2_hits", ss_title.str().c_str(), nBins_T0ns, 500, 3500, nBins_HitTDC, 500, 3500);
                            h2_hits->GetXaxis()->SetTitle("T0 [ns]");
                            h2_hits->GetYaxis()->SetTitle("Hit [tdc]");

                            for(auto& p: m_tdcs[chipID][iChn][iMem][iBx]) {
                                h2_hits->Fill(p.first, p.second);
                            }

                            std::vector<double> median;
                            std::vector<double> err;
                            std::vector<double> x;
                            // Calculates an array of medians for a TH2.
                            // For each bin in the x axis, the median along the y axis is calculated
                            median2(h2_hits, median, err, x);

                            if(median.size() < 10) continue;

                            TGraph* gr_ramp = new TGraph(median.size(), &(median[0]), &(x[0]));
                            gr_ramp->GetXaxis()->SetTitle("Hit [tdc]");
                            gr_ramp->GetYaxis()->SetTitle("T0 [ns]");
                            gr_ramp->SetMinimum(500);
                            gr_ramp->SetMaximum(3000);

                            // Using a linear fitter
                            TF1* f_lin = new TF1("f_lin", "1++x", 0, 4000);
                            // TF1* f_lin = new TF1("f_lin", "pol1", 0, 4000);
                            f_lin->SetParameter(0, -1000.);

                            // fix slope for memCells != 1
                            if(iMem > 1 && iBx==0 && chipSlope_odd > 0) f_lin->FixParameter(1,chipSlope_odd);
                            else if(iMem > 1 && iBx==1 && chipSlope_even > 0) f_lin->FixParameter(1,chipSlope_even);
                            else f_lin->SetParameter(1,1.6);
                            // f_lin->SetParameter(1,1.6);

                            // f_quad->SetParameter(2,0.);
                            // adjust the percentage of data points that are taken into
                            // account by the robust fit
                            // if(nHits > 1000) {

                            // Robust fit, the B option may be ommitted
                            gr_ramp->Fit("f_lin","Qrob=0.9", "", 500, 3000);
                            // } else {
                                // gr_ramp->Fit("f_lin","Qrob=0.8", "", 500, 3000);
                            // }
                            gr_ramp->SetTitle(ss_title.str().c_str());


                            double offset = f_lin->GetParameter(0);
                            double slope = f_lin->GetParameter(1);
                            // double quad = f_quad->GetParameter(2);
                            double quad = 0.;

                            // std::cout << fabs(slope - _timingManager->getTdcSlope(chipID, iChn, iMem, iBx+1, 9)) << std::endl;
                            // if(fabs(slope - _timingManager->getTdcSlope(chipID, iChn, iMem, iBx+1, 9)) > 0.1) {
                            if(iMem == 1) {
                                h2_hits->Write();
                                gr_ramp->Write();
                            }
                            // }

                            auto tupel = std::make_tuple(offset, slope, quad);
                            _m_calibration_values[chipID][iChn][iMem][iBx] = tupel;

                            h_compare_slope->Fill(slope - _timingManager->getTdcSlope(chipID, iChn, iMem, iBx+1, 9));
                            h_compare_offset->Fill(offset - (_timingManager->getOffset(chipID, iChn, iMem, iBx+1) - _timingManager->getTdcPedestal(chipID, iChn, iMem, iBx+1, 9)));
                            // h_compare_slope->Fill(quad - _timingManager->getTdcSlope(chipID, iChn, iMem, iBx, 9));

                            delete h2_hits;
                            delete gr_ramp;
                            delete f_lin;
                        }
                        else {
                            _m_calibration_values[chipID][iChn][iMem][iBx] = std::make_tuple(0,0,0);
                        }
                    }
                }
            }
        }
        h_compare_slope->Write();
        h_compare_offset->Write();

    }


    o_file->Close();

}


void TDC_analysis::end() {

    fit_calibration();
    // for(int i=0; i <= _nT0s; i++) {
        // std::cout << "There are " << tmp_count_T0s[i] << " events with " << i << " T0s." << std::endl; }

    // Events, Hits, (Hit, delay)
    std::vector< std::vector< std::tuple<timing::Hit, double> > > v_eventsForHitMap;

    std::cout << std::endl;
    std::cout << "There are: " << _v_events.size() << " Events." << std::endl;
    std::cout << std::endl;

    TFile* o_file = new TFile("analysis.root", "recreate");

    if(o_file->IsOpen()) {

        // TH1I* h_T0_residual = new TH1I("h_T0_residual", "T0 residuals", 100, -50, 50);
        // for(unsigned int i=0; i < v_ns_T0_0.size(); i++) {
            // h_T0_residual->Fill(v_ns_T0_0[i] - v_ns_T0_1[i]);
        // }
        // h_T0_residual->Write();

        HitMap* _hitMap_Offset_even = new HitMap(_timingManager->getKIJContainer(), std::string("Offset for even BxIDs"));
        HitMap* _hitMap_Offset_odd = new HitMap(_timingManager->getKIJContainer(), std::string("Offset for odd BxIDs"));

        // for(auto& chipID: _v_chipIDs) {
            // for(int ch=0; ch < _nChannels; ch++) {
                // _hitMap_timeResolution->set(i.first, j.first, k.first, f1->GetParameter(2));
            // }
        // }

        h_tdcSlopes->Write();

        h_memCell_pedestal->Write();
        h_memCell_slope->Write();

        TCanvas* c0 = new TCanvas("c0", "c0");
        vh_T0_diff[0]->SetLineColor(kRed);
        vh_T0_diff[0]->Draw();
        for(unsigned int i=1; i < vh_T0_diff.size(); i++) {
            if(i==1) vh_T0_diff[i]->SetLineColor(kGreen);
            if(i==2) vh_T0_diff[i]->SetLineColor(kOrange);
            if(i==3) vh_T0_diff[i]->SetLineColor(kBlack);
            vh_T0_diff[i]->Draw("SAME");
        }
        c0->Write();

        for(auto& histo : vh2_T0_vs_mT0s) {
            histo->Write();
        }

        for(auto& pp_histo : mh2_T0_vs_each_other) {
            for(auto& p_histo : pp_histo.second) {
                p_histo.second->Write();
            }
        }

        for(auto& pp_histo : mh2_T0_vs_each_other_tdc) {
            for(auto& p_histo : pp_histo.second) {
                p_histo.second->Write();
            }
        }


        for(auto& histo : vh_TDCs_T0) {
            histo->Write();
        }
        for(auto& histo : vh_TDCs_T0_odd) {
            histo->Write();
        }
        for(auto& histo : vh_ADCs_T0) {
            histo->Write();
        }

        h_T0_memCell->Write();

        std::vector<double> v_chip;
        std::vector<double> v_delay_mean;
        std::vector<double> v_delay_stdev;
        std::vector<double> v_error_x;
        std::vector<double> v_error_y;

        TH1D* h_chip_resolutions = new TH1D("h_chip_resolutions", "Time Resolution per chip", 100, 0, 50);

        TH1D* h_delta_ns = new TH1D("h_delta_ns", "difference of t to T0 mean of event, offset corrected", 3000, -500, 1000);
        TH1D* h_delta_ns_corrected = new TH1D("h_delta_ns_corrected", "difference of t to T0 mean of event, nonLinearity corrected", 3000, -500, 1000);
        TH1D* h_T0_diff = new TH1D("h_T0_diff", "Difference between T0s; T0s are non-Linearity corrected", 3000, -500, 1000);

        TH2D* h2_timeWalk = new TH2D("h2_timeWalk", "dT over mip", 100, 0, 50, 500, -25,25);
        TProfile* prfl_timeWalk = new TProfile("prfl_timeWalk", "Timewalk", 400, 0, 100);

        std::vector<double> v_dT;
        std::vector<double> v_mip;

        std::map<int, std::vector<double> > mv_nHits_dt;

        std::map<int, std::vector<double> > mv_timewalk;
        double binSize = 0.1; // Mip

        std::map<int, std::map<int, std::map<int, std::vector<double> > > > mv_ijk_resolution;
        std::map<int, std::map<int, std::map<int, std::pair<int, int> > > > m_ijk_cahnnel;


        for(auto& event: _v_events) {
            //XXX: REMEMBER THAT I AM DOING THIS!!!
            // if(event.v_hits.size() > 15) continue;

            double T0_time = 0.;
            for(auto& T0_hit: event.T0s) {
                T0_time += T0_hit.second.ns;
            }
            T0_time = T0_time / (double)event.nT0s;

            std::vector< std::tuple<timing::Hit, double> > v_hitsForHitMap;



            for(auto& hit: event.v_hits) {
                if(hit.isT0) continue;
                if(hit.chipID == 185 || hit.chipID == 182 || hit.chipID == 183) {
                    continue;
                }
                // double offset = _timingManager->getOffset(hit.chipID, hit.channel, hit.memoryCell, hit.bxID);
                double offset = 0;

                if(offset != 0 || true) {
                    // if(event.v_hits.size() < 20) { //TODO:Remove this again
                        if(hit.ns > 0) {
                            h_delta_ns->Fill(hit.ns - T0_time - offset);
                        }

                        // auto tupel = _m_calibration_values[hit.chipID][hit.channel][hit.memoryCell][(hit.bxID+1)%2];
                        // auto tupel_slope = _m_calibration_values[hit.chipID][36][1][(hit.bxID+1)%2];
                        // double delay = std::get<0>(tupel);
                        // double slope = std::get<1>(tupel_slope);
                        // if(delay != 0 && slope != 0 ) {
                        //     hit.ns = (delay + slope*hit.tdc + std::get<2>(tupel_slope)*hit.tdc*hit.tdc);
                        // } else {
                        //     continue;
                        // }
                        hit.ns = _timingManager->tdcToNs_nonLin(hit.chipID, hit.channel, hit.memoryCell, hit.bxID, hit.tdc, T0_time);

                        if(hit.ns > 0 && hit.ns > 500 && hit.ns < 3500) {
                            double tw_correction = 0;
                            // if(hit.mip > 20.) tw_correction = 0.;
                            // if(hit.mip <= 2.) tw_correction = 3.4 - 3.*hit.mip;
                            // if(hit.mip <= 20.) {
                                // tw_correction = -3.2 + 4.5/(hit.mip) - 0.64/(hit.mip*hit.mip) + 0.035*hit.mip*hit.mip - 0.17*hit.mip;
                                // tw_correction = -3.55 - 0.77/(hit.mip*hit.mip) + 4.82/(hit.mip)- 0.073*hit.mip + 0.027*hit.mip*hit.mip;
                                // tw_correction = -3.55 - 0.77/(hit.mip*hit.mip) + 4.82/(hit.mip)- 0.073*hit.mip + 0.027*hit.mip*hit.mip;
                                // tw_correction = -3. + 2.37*exp(-0.69*hit.mip + 0.75) + 0.711/(hit.mip*hit.mip);
                                // tw_correction = -2.9 + 5.68*exp(-0.80*hit.mip);
                            tw_correction = -3.573 + 5.495*exp(-0.6055*hit.mip);
                                // tw_correction = -4. + 5.48/(hit.mip) -1./(hit.mip*hit.mip) + 0.021*hit.mip*hit.mip;
                            // }
                            if(hit.chipID != 172 && hit.chipID != 175 && hit.chipID != 181 && hit.chipID != 178 && hit.chipID != 144 && hit.chipID != 146
                                    && hit.chipID != 154 && hit.chipID != 155 && hit.chipID != 156 && hit.chipID != 141 && hit.chipID != 169
                                    && hit.chipID != 177 && hit.chipID != 207)  {

                                // h_delta_ns_corrected->Fill(hit.ns - tw_correction - T0_time - offset);
                                h_delta_ns_corrected->Fill(hit.ns - T0_time - tw_correction);

                                h2_timeWalk->Fill(hit.mip, hit.ns - T0_time - offset);
                                mv_timewalk[floor(hit.mip/binSize)].push_back(hit.ns - tw_correction - T0_time - offset);
                                if(fabs(hit.ns - T0_time - offset) < 20.) {
                                    prfl_timeWalk->Fill(hit.mip, hit.ns - T0_time - offset);
                                }

                            }

                            if(event.v_hits.size() > 20 && v_eventsForHitMap.size() < 200) {
                                v_hitsForHitMap.push_back(std::make_tuple(hit, hit.ns - tw_correction - T0_time - offset));
                            }

                            mv_ijk_resolution[hit.I][hit.J][hit.K].push_back(hit.ns - tw_correction - T0_time - offset);
                            m_ijk_cahnnel[hit.I][hit.J][hit.K] = std::make_pair(hit.chipID, hit.channel);

                            if(hit.mip > 15) {
                                v_dT.push_back(hit.ns - T0_time - offset);
                                v_mip.push_back(hit.mip);
                            }

                        }
                    // }
                    mv_nHits_dt[event.v_hits.size()].push_back(hit.ns - T0_time - offset);
                    _hitMap_calibrated->fill(hit.I, hit.J, hit.K);
                }
            }

            if(event.v_hits.size() > 20 && v_eventsForHitMap.size() < 200) {
                v_eventsForHitMap.push_back(v_hitsForHitMap);
            }
        }


        for(auto& i: mv_ijk_resolution) {
            for(auto& j: i.second) {
                for(auto& k: j.second) {
                    if(k.second.size() > 500) {
                        std::stringstream hist_name;
                        hist_name << "h_kij_dT_" << k.first << "-" << i.first << "-" << j.first;
                        std::stringstream hist_title;
                        hist_title << "dT for k ij: " << k.first << "-" << i.first << "-" << j.first << " ";
                        hist_title << m_ijk_cahnnel[i.first][j.first][k.first].first << " " << m_ijk_cahnnel[i.first][j.first][k.first].second;

                        TH1D* h_dT = new TH1D(hist_name.str().c_str(), hist_title.str().c_str(), 200, -100, 100);
                        for(auto& dT: k.second) {
                            h_dT->Fill(dT);
                        }

                        int maxBin = h_dT->GetMaximumBin();
                        int fit_x_min = maxBin - 12;
                        int fit_x_max = maxBin + 12;

                        TF1 *f1 = new TF1("f1","gaus",-100,100);
                        h_dT->Fit(f1,"QL","",h_dT->GetBinCenter(fit_x_min), h_dT->GetBinCenter(fit_x_max));
                        h_dT->Write();
                        _hitMap_timeResolution->set(i.first, j.first, k.first, f1->GetParameter(2));
                        _hitMap_timeDelay->set(i.first, j.first, k.first, f1->GetParameter(1));

                        if(f1->GetParameter(2) > 5) {
                            std::cout << m_ijk_cahnnel[i.first][j.first][k.first].first << "\t";
                            std::cout << m_ijk_cahnnel[i.first][j.first][k.first].second << "\t";
                            std::cout << f1->GetParameter(2) << std::endl;
                        }
                    }
                }
            }
        }

        std::vector<double> v_timewalk_bins;
        std::vector<double> v_timewalk_dT_fits;
        std::vector<double> v_timewalk_dT_error;
        std::vector<double> v_timewalk_bin_error;

        //Timewalk correction
        for(auto& e: mv_timewalk) {
            std::vector<double> dTs = e.second;
            if(dTs.size() > 500) {
                double bin = e.first;

                std::stringstream hist_name;
                hist_name << "h_tw_slice_" << bin;
                std::stringstream hist_title;
                hist_title << "dT for Mip-values: " << bin*binSize << " - " << (bin+1)*binSize;

                TH1D* h_slice = new TH1D(hist_name.str().c_str(), hist_title.str().c_str(), 200, -100, 100);

                for(auto& dT: dTs) {
                    h_slice->Fill(dT);
                }

                int maxBin = h_slice->GetMaximumBin();
                int fit_x_min = maxBin - 12;
                int fit_x_max = maxBin + 12;

                TF1 *f1 = new TF1("f1","gaus",-100,100);
                h_slice->Fit(f1,"QL","",h_slice->GetBinCenter(fit_x_min), h_slice->GetBinCenter(fit_x_max));

                v_timewalk_bins.push_back((bin+0.5)*binSize);
                v_timewalk_bin_error.push_back(binSize/sqrt(12.));
                v_timewalk_dT_fits.push_back(f1->GetParameter(1));
                v_timewalk_dT_error.push_back(f1->GetParError(1));

                h_slice->Write();
            }
        }

        TGraphErrors* gr_timewalk_bin = new TGraphErrors(v_timewalk_bins.size(),
                &(v_timewalk_bins[0]), &(v_timewalk_dT_fits[0]), &(v_timewalk_bin_error[0]), &(v_timewalk_dT_error[0]));
        gr_timewalk_bin->SetTitle("dT over Mips");
        // TF1 *f_tw = new TF1("f_tw","[0]+[1]/(x*x)+[2]/x+[3]*x+[4]*x*x",-0.5,12.);
        // f_tw->SetParameter(0,-3.5);
        // f_tw->SetParameter(0,-0.8);
        // f_tw->SetParameter(0, 4.8);
        // f_tw->SetParameter(0,-0.07);
        // f_tw->SetParameter(0,-0.02);
        TF1 *f_expo = new TF1("f_expo","[0]+[1]*TMath::Exp([2]*x+[3])+[4]/(x*x)",0,15.);
        f_expo->SetParameter(0,-3);
        f_expo->SetParameter(1,3);
        f_expo->SetParameter(2,-1);
        f_expo->SetParameter(3,1);
        gr_timewalk_bin->Fit(f_expo,"","",-0.5, 10.);
        gr_timewalk_bin->Write();

        prfl_timeWalk->Write();
        // Double_t q [1];
        // Double_t median [1];
        // q[0] = 0.5;
        // prfl_timeWalk->GetQuantiles(1, q, median);
        // std::cout << "Median: " << median[0] << std::endl;


        // h2_timeWalk->QuantilesX(0.5, "h_test");
        // TH1D* h_test;
        // h2_timeWalk->QuantilesX(0.5, "h_test");
        std::vector<double> median;
        std::vector<double> err;
        std::vector<double> x;
        median2(h2_timeWalk, median, err, x);
        // h_test->Write();

        h2_timeWalk->Write();
        TGraph* gr_timewalk = new TGraph(v_dT.size(), &(v_mip[0]), &(v_dT[0]));
        TGraph* gr_timewalk2 = new TGraph(median.size(), &(x[0]), &(median[0]));
        gr_timewalk2->Write();

        gr_timewalk->GetHistogram()->SetMaximum(10.);
        gr_timewalk->GetHistogram()->SetMinimum(-10.);
        gr_timewalk->SetTitle("Timewalk");
        gr_timewalk->Write();

        TGraphErrors* gr = new TGraphErrors(v_delay_mean.size(), &(v_chip[0]), &(v_delay_mean[0]), &(v_error_x[0]), &(v_error_y[0]) );
        gr->Write();


        TH1D* h_nHits = new TH1D("h_nHits", "number of hits per event", 200, 0, 200);

        std::vector<double> v_nHits;
        std::vector<double> v_dt;
        std::vector<double> v_errY;
        for(auto& p : mv_nHits_dt) {
            std::vector<double> v = p.second;
            v_nHits.push_back(p.first);
            std::sort(v.begin(), v.end());
            v_dt.push_back(v[v.size()/2]);
            v_errY.push_back(0);

            h_nHits->SetBinContent(p.first, v.size());
        }

        h_nHits->Write();

        h_delta_ns->Write();

        TGraphErrors* gr_nHits_dt = new TGraphErrors(v_nHits.size(), &(v_nHits[0]), &(v_dt[0]), &(v_errY[0]), &(v_errY[0]));
        gr_nHits_dt->SetTitle("Median dt vs number of hits");
        gr_nHits_dt->Write();

        h_delta_ns_corrected->Write();

        h_chip_resolutions->Write();


        if(_dataSet == DataSet::MUON) {
            _hitMap_timeResolution->commonZScale(3,8);
            _hitMap_timeDelay->commonZScale(-2,2);
        } else if(_dataSet == DataSet::PION70) {
            _hitMap_timeResolution->commonZScale(6,16);
            _hitMap_timeDelay->commonZScale(-10,2);
        }

        _hitMap_calibrated->setLogZ();
        _hitMap->setLogZ();

        _hitMap_timeResolution->write(o_file);
        _hitMap_timeDelay->write(o_file);
        _hitMap_calibrated->write(o_file);
        _hitMap->write(o_file);

        h_T0_diff->Write();

    } else {
        std::cout << "!!! Could not open root file. Histograms are not saved!" << std::endl;
    }

    o_file->Close();
    delete o_file;

    writeEventMaps(v_eventsForHitMap);

    std::cout << "TDC_analysis::end()  " << name()
 	      << " processed " << _nEvt << " events in " << _nRun << " runs "
     	      << std::endl ;

}
